package racer.racer_app_finish;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;

import androidx.annotation.NonNull;
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {
  @Override
  public void configureFlutterEngine(@NonNull FlutterEngine flutterEngine) {
//    PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
//    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
//    {
//      if (!pm.isIgnoringBatteryOptimizations(getPackageName()))
//      {
//        //  Prompt the user to disable battery optimization
//        Intent intent = new Intent();
//        intent.setAction(Settings.ACTION_IGNORE_BATTERY_OPTIMIZATION_SETTINGS);
//        startActivity(intent);
//      }
//    }

    GeneratedPluginRegistrant.registerWith(flutterEngine);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

  }
}
