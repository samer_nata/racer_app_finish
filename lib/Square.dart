import 'dart:async';

import 'Models/Course.dart';
import 'Models/Details_Tracking.dart';
import 'Models/Timings.dart';

class Square {
  Function fun_refresh_state;
  String txt,tracking;
  bool send_server = false;
  int bib;
  double km;
  bool want_to_send = false;
  int count_gray = 0;
  DetailsTracking detailsTracking;
  bool is_start_from_before = false;
  int time_in_mill;


//  Timer timer_square=new Timer(const Duration(seconds: 1), (){});

  bool is_start = false;
  bool is_start_from_page = false;
  int timerMaxSeconds = 10;
  Course course;
  bool is_finish = false;
  List<Timing> lst_timings = new List();
  int currentSeconds = 0;
  bool is_stop = false;
  bool is_hide = false;
  Map<String, dynamic> map_gray = new Map();
  bool start_animation = false;
  bool is_delete = false;
  bool is_finish_page = false;
  Function fun_delete,fun_delete_from_list_undo,fun_want_refresh;
  //properties to manage timer
 bool loopActive=false;
 bool buttonPressed=false;
 int counter=0;


  Square(
      {this.txt,
        this.timerMaxSeconds,
        this.detailsTracking,
        this.course,
        this.lst_timings,
        this.bib,
        this.is_start_from_page,
        this.time_in_mill,
        this.count_gray,
        this.fun_refresh_state,
        this.fun_delete,this.fun_delete_from_list_undo,this.tracking,this.fun_want_refresh});

  Duration interval = Duration(seconds: 1);
  Duration increase = Duration(seconds: 0);

//bool is_delete=false;
//  final int timerMaxSeconds = 10;

  Future<void> startTimeout() {
    print("req>>>>>>>>>>>>>>>>>>>>");

    this.is_start_from_before = true;
//   this. function.call();

//    var duration = this.interval;
//    if (this.currentSeconds != 0 && !this.is_finish) {
//      this.currentSeconds = this.timerMaxSeconds - this.increase.inSeconds;
//    }

    if (this.is_start_from_page) {
      Timer.periodic(this.interval, (timer) async {
        print("Cure>>>>>>>>>>>>>>>> ${this.currentSeconds} bib ${this.bib}");
        print("start_anima>>>>>>>>>>>>>>>> ${this.start_animation}");
        print(
            "${this.bib} this.increase.inSeconds>>>>>>>>>>>>>> ${this.increase.inSeconds}");
        if (this.increase.inSeconds >= this.timerMaxSeconds) {
          this.is_finish = true;
//          this.start_animation=false;

          cancel();
          timer.cancel();
          start_animation_delete();
          return;
        }
        if (this.is_stop) {
          this.currentSeconds = this.timerMaxSeconds - this.increase.inSeconds;
//        interval = Duration(seconds: 1);
//          this. function.call();
          timer.cancel();
          return;
        }
        if (this.is_start == true) {
          this.currentSeconds = this.timerMaxSeconds - timer.tick;

//          function.call();
          if (this.increase.inSeconds >= this.timerMaxSeconds) {
            timer.cancel();

//          this.currentSeconds=0;
//        this.is_start_from_page=false;
            this.is_finish = true;

            start_animation_delete();
            return;
          }

          this.currentSeconds = this.timerMaxSeconds - this.increase.inSeconds;
          this.increase = Duration(seconds: this.increase.inSeconds + 1);
        }
        if (!this.send_server && !is_finish_page) {
          this.fun_refresh_state.call();
        }
      });
    }
  }

  start_animation_delete() {
    print('invoked>>>> start_animation_delete');
    if(this.want_to_send)return;
    this.start_animation = true;
    if (!is_finish_page&&!send_server) {
      this.fun_refresh_state.call();
    }
    Future.delayed(Duration(milliseconds: 900), () {
      print("fun_refresh_state1>>>>");

      if (!this.is_finish_page&&!this.want_to_send) {
        print("fun_refresh_state2>>>>");
        this.fun_delete_from_list_undo.call();
        this.fun_delete.call();
        this.fun_refresh_state.call();
        this.fun_want_refresh.call();
      }
    });
  }

  @override
  String toString() {
    return 'Square{send_server: $send_server, bib: $bib, is_finish: $is_finish, start_animation: $start_animation ,tracking: $tracking}';
  }

  Future<void> cancel() {
    this.is_start = false;
    this.is_stop = true;

//   }
  }

  Future<void> start() {
    this.is_start = true;
    this.is_stop = false;

//function.call();
    if (this.is_start_from_page&&!send_server) {
      this.startTimeout();
    }
  }
}
