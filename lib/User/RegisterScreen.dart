//import 'package:flutter/material.dart';
//import 'package:flutter_spinkit/flutter_spinkit.dart';
//import 'package:racer_app_finish/Loop/LapsTimes.dart';
//import 'package:racer_app_finish/Loop/Loop.dart';
//import 'package:racer_app_finish/Models/Course.dart';
//import 'package:racer_app_finish/Models/Event.dart';
//import 'package:racer_app_finish/Models/Race.dart';
//import 'package:racer_app_finish/Models/Racer.dart';
//import 'package:racer_app_finish/Race/Race.dart';
//
//import 'package:toast/toast.dart';
//import '../Checking/CheckMeInScreen.dart';
//import '../Database.dart';
//import 'User.dart';
//
//class RegisterScreen extends StatefulWidget {
//  Event_l event_l;
//  Race race;
//
//
//  RegisterScreen(this.event_l,this.race);
//
//  @override
//  _RegisterScreenState createState() => _RegisterScreenState(event_l,race);
//}
//
//class _RegisterScreenState extends State<RegisterScreen> {
//  Event_l event_l;
//  Race race;
//
//  _RegisterScreenState(this.event_l,this.race);
//
//  TextEditingController firstNameController = new TextEditingController();
//  TextEditingController lastNameController = new TextEditingController();
//  TextEditingController bibNumberController = new TextEditingController();
//  TextEditingController clubController = new TextEditingController();
//  TextEditingController divisionController = new TextEditingController();
//  List categories;
//  String divisionValue = "Klass (valfri)";
//  String firstName, lastName, bibNumber, club, division;
//  Course course;
//  bool donecourses = false;
//  bool doneRaces=false;
//
//  Future<void> refresh_race() async {
//    setState(() {
//      doneRaces = false;
//    });
//    await DBProvider.db.Race_list_rafres(race, event_l).then((value) {
//      if (value.length > 0) {
//        setState(() {
//          race = value[0];
//
//
//        });
//      }
//      setState(() {
//        doneRaces = true;
//      });
//    });
//    get_course();
//  }
//  @override
//  void initState() {
//    super.initState();
//    DBProvider.db.get_information_race(race).then((value) {
//      refresh_race();
//    });
//
//    categories = ["Klass (valfri)", "First", "Last", "Second"];
////    get_laps();
//  }
//
//  void get_course() {
//    DBProvider.db.Course_from_race(race).then((value) {
//      setState(() {
//        course=value;
//        donecourses = true;
//      });
//    });
//  }
//
////  List laps_list = new List();
////
////  get_laps() {
////    DBProvider.db.LapsTimes_list(loop.id).then((data) {
////      setState(() {
////        laps_list = data;
////      });
////    });
////  }
//
//  @override
//  Widget build(BuildContext context) {
//    return Scaffold(
//      appBar: PreferredSize(
//        child: Container(
//          height: 175,
//          child: Center(
//            child: Padding(
//              padding: const EdgeInsets.only(top: 24.0),
//              child: Padding(
//                padding: const EdgeInsets.only(top: 0.0),
//                child: SingleChildScrollView(
//                  scrollDirection: Axis.vertical,
//                  child: Stack(
//                    children: <Widget>[
//                      Column(
//                        crossAxisAlignment: CrossAxisAlignment.start,
//                        mainAxisAlignment: MainAxisAlignment.center,
//                        children: <Widget>[
//                          SingleChildScrollView(
//                            scrollDirection: Axis.horizontal,
//                            child: Row(
//                              children: <Widget>[
//                                Padding(
//                                  padding: const EdgeInsets.all(8.0),
//                                  child: InkWell(
//                                    onTap: () {
//                                      Navigator.of(context)
//                                          .pushNamedAndRemoveUntil(
//                                              '/RacesScreen',
//                                              (Route<dynamic> route) => false);
//                                    },
//                                    child: Container(
//                                      width: 70,
//                                      height: 50,
//                                      decoration: BoxDecoration(
//                                          image: DecorationImage(
//                                              image: AssetImage(
//                                                  'Images/LaLigaLogo.png'),
//                                              fit: BoxFit.fill)),
//                                    ),
//                                  ),
//                                ),
//                                Padding(
//                                  padding: const EdgeInsets.all(8.0),
//                                  child: SingleChildScrollView(
//                                    scrollDirection: Axis.vertical,
//                                    child: Column(
//                                      mainAxisAlignment:
//                                          MainAxisAlignment.center,
//                                      children: <Widget>[
//                                        Container(
//                                          //color: Colors.white,
//                                          width: 200,
//                                          child: Center(
//                                            child: Text(
//                                              race.name,
//                                              style: TextStyle(
//                                                  color: Colors.white,
//                                                  fontSize: 15,
//                                                  fontWeight: FontWeight.w100),
//                                            ),
//                                          ),
//                                        ),
//                                        Padding(
//                                          padding:
//                                              const EdgeInsets.only(top: 8.0),
//                                          child: Container(
//                                            //color: Colors.white,
//                                            width: 200,
//                                            child: Center(
//                                              child: Text(
//                                                race.name,
//                                                style: TextStyle(
//                                                    color: Colors.white,
//                                                    fontSize: 15,
//                                                    fontWeight:
//                                                        FontWeight.w100),
//                                              ),
//                                            ),
//                                          ),
//                                        ),
//                                        Padding(
//                                          padding:
//                                              const EdgeInsets.only(top: 16.0),
//                                          child: Container(
//                                            width: 200,
//                                            child: Center(
//                                              child: Text(
//                                                "Registrering",
//                                                style: TextStyle(
//                                                    color: Colors.white54,
//                                                    fontSize: 15,
//                                                    fontWeight:
//                                                        FontWeight.w100),
//                                              ),
//                                            ),
//                                          ),
//                                        )
//                                      ],
//                                    ),
//                                  ),
//                                ),
//                              ],
//                            ),
//                          ),
//                          Padding(
//                            padding: const EdgeInsets.only(top: 8.0),
//                            child: Container(
//                              width: MediaQuery.of(context).size.width,
//                              color: Colors.blue,
//                              height: 2,
//                            ),
//                          )
//                        ],
//                      ),
//                      Align(
//                        alignment: Alignment.centerRight,
//                        child: Padding(
//                          padding: const EdgeInsets.only(right: 12.0, top: 8.0),
//                          child: Column(
//                            children: <Widget>[
//                              Padding(
//                                padding: const EdgeInsets.only(bottom: 16.0),
//                                child: Container(
//                                  width: 20,
//                                  height: 25,
//                                  decoration: BoxDecoration(
//                                      image: DecorationImage(
//                                          image:
//                                              AssetImage('Images/profile.png'),
//                                          fit: BoxFit.fill)),
//                                ),
//                              ),
//                              Container(
//                                width: 25,
//                                height: 25,
//                                decoration: BoxDecoration(
//                                    image: DecorationImage(
//                                        image:
//                                            AssetImage('Images/redChecked.png'),
//                                        fit: BoxFit.fill)),
//                              ),
//                            ],
//                          ),
//                        ),
//                      )
//                    ],
//                  ),
//                ),
//              ),
//            ),
//          ),
//        ),
//        preferredSize: Size.fromHeight(120),
//      ),
//      backgroundColor: Colors.black,
//      body: SingleChildScrollView(
//        scrollDirection: Axis.vertical,
//        child: Column(
//          children: <Widget>[
//            Padding(
//              padding: const EdgeInsets.all(4.0),
//              child: Container(
//                color: Color(0xff121214),
//                width: MediaQuery.of(context).size.width,
//                height: 100,
//                child: SingleChildScrollView(
//                  scrollDirection: Axis.vertical,
//                  child: Column(
//                    crossAxisAlignment: CrossAxisAlignment.start,
//                    mainAxisAlignment: MainAxisAlignment.center,
//                    children: <Widget>[
//                      Padding(
//                        padding: const EdgeInsets.only(
//                            left: 8.0, bottom: 3.0, top: 8.0),
//                        child: Text(
//                          "Starttid: ${race.startTimeMs.toString().substring(10)}",
//                          style: TextStyle(
//                              color: Colors.white,
//                              fontSize: 17,
//                              fontWeight: FontWeight.w100),
//                        ),
//                      ),
//                      Padding(
//                        padding: const EdgeInsets.only(left: 8.0, bottom: 3.0),
//                        child:donecourses&&doneRaces? Text(
//                          "Distans: ${course != null ? course.distance != null ? course.distance : '...' : '...'} km",
//                          style: TextStyle(
//                              color: Colors.white,
//                              fontSize: 17,
//                              fontWeight: FontWeight.w100),
//                        ): SpinKitThreeBounce(
//                          size: 17,
//                          color: Colors.blue,
//                        ),
//                      ),
//                      Padding(
//                        padding: const EdgeInsets.only(left: 8.0, bottom: 3.0),
//                        child: Row(
//                          children: <Widget>[
//                            Text(
//                              "Mellantider: ",
//                              style: TextStyle(
//                                  color: Colors.white,
//                                  fontSize: 17,
//                                  fontWeight: FontWeight.w100),
//                            ),
//                            course != null && donecourses
//                                ? Row(
//                              children: List.generate(
//                                  course.coursepoints.length, (index) {
//                                return Padding(
//                                  padding:
//                                  const EdgeInsets.only(left: 4),
//                                  child: Text(
//                                    '${course.coursepoints[index].distance.toString()} km',
//                                    style: TextStyle(
//                                        color: Colors.white,
//                                        fontSize: 17,
//                                        fontWeight: FontWeight.w100),
//                                  ),
//                                );
//                              }),
//                            )
//                                : Container(),
//                          ],
//                        ),
//                      )
//                    ],
//                  ),
//                ),
//              ),
//            ),
//            Padding(
//              padding: const EdgeInsets.all(4.0),
//              child: Container(
//                color: Color(0xff121214),
//                width: MediaQuery.of(context).size.width,
//                height: 300,
//                child: SingleChildScrollView(
//                  scrollDirection: Axis.vertical,
//                  child: Column(
//                    children: <Widget>[
//                      Padding(
//                        padding: const EdgeInsets.only(
//                            left: 16.0, right: 16.0, bottom: 8.0),
//                        child: TextField(
//                          onChanged: (value) {
//                            setState(() {
//                              firstName = value;
//                            });
//                          },
//                          style: TextStyle(
//                              color: Colors.white,
//                              fontSize: 17,
//                              fontWeight: FontWeight.w100),
//                          textAlign: TextAlign.left,
//                          controller: firstNameController,
//                          decoration: new InputDecoration(
//                            contentPadding: EdgeInsets.only(
//                                left: 8, right: 8.0, bottom: 8.0, top: 10),
//                            //border: InputBorder.none,
//                            hintStyle: TextStyle(
//                                color: Colors.white,
//                                fontSize: 17,
//                                fontWeight: FontWeight.w100),
//                            hintText: 'Ditt förnamn',
//                          ),
//                        ),
//                      ),
//                      Padding(
//                        padding: const EdgeInsets.only(
//                            left: 16.0, right: 16.0, bottom: 8.0),
//                        child: TextField(
//                          onChanged: (value) {
//                            setState(() {
//                              lastName = value;
//                            });
//                          },
//                          style: TextStyle(
//                              color: Colors.white,
//                              fontSize: 17,
//                              fontWeight: FontWeight.w100),
//                          textAlign: TextAlign.left,
//                          controller: lastNameController,
//                          decoration: new InputDecoration(
//                            contentPadding: EdgeInsets.only(
//                                left: 8, right: 8.0, bottom: 8.0, top: 10),
//                            //border: InputBorder.none,
//                            hintStyle: TextStyle(
//                                color: Colors.white,
//                                fontSize: 17,
//                                fontWeight: FontWeight.w100),
//                            hintText: 'Ditt efternamn',
//                          ),
//                        ),
//                      ),
//                      Padding(
//                        padding: const EdgeInsets.only(
//                            left: 16.0, right: 16.0, bottom: 8.0),
//                        child: TextField(
//                          keyboardType: TextInputType.number,
//                          onChanged: (value) {
//                            setState(() {
//                              bibNumber = value;
//                            });
//                          },
//                          style: TextStyle(
//                              color: Colors.white,
//                              fontSize: 17,
//                              fontWeight: FontWeight.w100),
//                          textAlign: TextAlign.left,
//                          controller: bibNumberController,
//                          decoration: new InputDecoration(
//                            contentPadding: EdgeInsets.only(
//                                left: 8, right: 8.0, bottom: 8.0, top: 10),
//                            //border: InputBorder.none,
//                            hintStyle: TextStyle(
//                                color: Colors.white,
//                                fontSize: 17,
//                                fontWeight: FontWeight.w100),
//                            hintText: 'Ditt startnummer',
//                          ),
//                        ),
//                      ),
//                      Padding(
//                        padding: const EdgeInsets.only(
//                            left: 16.0, right: 16.0, bottom: 8.0),
//                        child: TextField(
//                          onChanged: (value) {
//                            setState(() {
//                              club = value;
//                            });
//                          },
//                          style: TextStyle(
//                              color: Colors.white,
//                              fontSize: 17,
//                              fontWeight: FontWeight.w100),
//                          textAlign: TextAlign.left,
//                          controller: clubController,
//                          decoration: new InputDecoration(
//                            contentPadding: EdgeInsets.only(
//                                left: 8, right: 8.0, bottom: 8.0, top: 10),
//                            //border: InputBorder.none,
//                            hintStyle: TextStyle(
//                                color: Colors.white,
//                                fontSize: 17,
//                                fontWeight: FontWeight.w100),
//                            hintText: 'Din klubb (valfri)',
//                          ),
//                        ),
//                      ),
//                      ListTile(
//                        leading: Padding(
//
//
//
//                          padding: const EdgeInsets.only(left: 10.0),
//                          child: Theme(
//                            data: Theme.of(context)
//                                .copyWith(canvasColor: Colors.black),
//                            child: DropdownButton(
//                              hint: Text(
//                                divisionValue,
//                                style: TextStyle(
//                                    color: Colors.white,
//                                    fontSize: 17,
//                                    fontWeight: FontWeight.w100),
//                              ),
//                              underline: Container(
//                                color: Color(0x483e3700),
//                              ),
//                              icon: Icon(
//                                Icons.keyboard_arrow_down,
//                                color: Colors.blue,
//                              ),
//                              value: divisionValue,
//                              items: List.generate(categories.length, (index) {
//                                return DropdownMenuItem(
//                                  child: Text(
//                                    categories[index].toString(),
//                                    style: TextStyle(
//                                        color:
//                                            categories[index] == divisionValue
//                                                ? Colors.white54
//                                                : Colors.white,
//                                        fontSize: 17,
//                                        fontWeight: FontWeight.w100),
//                                  ),
//                                  value: categories[index],
//                                );
//                              }),
//                              onChanged: (value) {
//                                setState(() {
//                                  divisionValue = value;
//                                });
//                              },
//                            ),
//                          ),
//                        ),
//                      )
//                    ],
//                  ),
//                ),
//              ),
//            ),
//            Padding(
//              padding: const EdgeInsets.only(top: 8.0),
//              child: Container(
//                decoration: BoxDecoration(
//                  borderRadius: BorderRadius.all(Radius.circular(10)),
//                  color: Colors.blue,
//                ),
//                width: 280,
//                height: 40,
//                child: InkWell(
//                  onTap: () => doSubmitted(),
//                  child: Center(
//                    child: Text(
//                      "Registrera mig",
//                      style: TextStyle(
//                          color: Colors.white,
//                          fontSize: 15,
//                          fontWeight: FontWeight.w100),
//                    ),
//                  ),
//                ),
//              ),
//            ),
//            Padding(
//              padding: const EdgeInsets.only(top: 16.0, left: 8.0),
//              child: Align(
//                alignment: Alignment.bottomLeft,
//                child: InkWell(
//                  onTap: () => Navigator.pop(context),
//                  child: Text(
//                    "Tillbaka",
//                    style: TextStyle(
//                        color: Colors.blue,
//                        fontSize: 15,
//                        fontWeight: FontWeight.w300),
//                  ),
//                ),
//              ),
//            )
//          ],
//        ),
//      ),
//    );
//  }
//
//  void doSubmitted() async {
//    setState(() {
//      firstName = firstNameController.text.trim();
//      lastName = lastNameController.text.trim();
//      bibNumber = bibNumberController.text.trim();
//      club = clubController.text.trim();
//      division = divisionValue.toString().trim();
//    });
//
//    if (firstName.isEmpty && lastName.isEmpty && bibNumber.isEmpty) {
//      Toast.show(
//          "Vänligen skriv ditt förnamn , ditt efternamn , ditt startnummer",
//          context,
//          backgroundColor: Colors.black,
//          duration: Toast.LENGTH_LONG);
//    } else if (firstName.isEmpty || lastName.isEmpty || bibNumber.isEmpty) {
//      if (firstName.isEmpty) {
//        Toast.show("Vänligen skriv ditt förnamn", context,
//            backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
//      } else if (lastName.isEmpty) {
//        Toast.show("Vänligen skriv ditt efternamn", context,
//            backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
//      } else if (bibNumber.isEmpty) {
//        Toast.show("Vänligen skriv ditt startnummer", context,
//            backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
//      }
//    } else {
//      Racer racer = new Racer(
//       bib: int.parse(bibNumber),surname: lastName,firstname: firstName,club: club,id: 5);
//
//      Navigator.push(
//          context,
//          new MaterialPageRoute(
//              builder: (context) => CheckMeInScreen(event_l, race, racer)));
//    }
//  }
//}
