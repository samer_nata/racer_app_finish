import 'dart:convert';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geolocator/geolocator.dart';
import 'package:imei_plugin/imei_plugin.dart';
import 'package:racer_app_finish/Appearance.dart';
import 'package:racer_app_finish/Checking/CheckMeInScreen.dart';
import 'package:racer_app_finish/Checking/CheckedScreen.dart';
import 'package:racer_app_finish/Enum.dart';
import 'package:racer_app_finish/ManagerFunction/OurTabBarView/ScreenManager/ControlPage.dart';
import 'package:racer_app_finish/ManagerFunction/OurTabBarView/ScreenManager/EventsScreenManager.dart';
import 'package:racer_app_finish/ManagerFunction/OurTabBarView/ScreenManager/Login_Manager_From_Racer.dart';
import 'package:racer_app_finish/ManagerFunction/OurTabBarView/ScreenManager/RacesScreenManager.dart';
import 'package:racer_app_finish/Models/Checked.dart';
import 'package:racer_app_finish/Models/Course.dart';
import 'package:racer_app_finish/Models/Details_Tracking.dart';
import 'package:racer_app_finish/Models/Event.dart';
import 'package:racer_app_finish/Models/Manager.dart';
import 'package:racer_app_finish/Models/Race.dart';
import 'package:racer_app_finish/Models/Racer.dart';
import 'package:racer_app_finish/Models/Trackers.dart';
import 'package:racer_app_finish/Models/TrackingWithInformation.dart';
import 'package:racer_app_finish/Race/Race.dart';
import 'package:racer_app_finish/Screen/EventsScreenRacer.dart';
import 'package:racer_app_finish/Screen/RacesScreenRacer.dart';
import 'package:racer_app_finish/ColorsF.dart' as col;
import 'package:racer_app_finish/SplashScreen.dart';

import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;
import '../Database.dart';

class Login extends StatefulWidget {
  Event_l event_l;
  Race race;
  Racer racer;

  Login(this.event_l, this.race, this.racer);

  @override
  _LoginState createState() => _LoginState(event_l, race, racer);
}

class _LoginState extends State<Login> {
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic> _deviceData = <String, dynamic>{};
  Event_l event_l;
  Racer racer;
  Race race;
  Course course;
  bool donecourses = false, doneRaces = false;

  _LoginState(this.event_l, this.race, this.racer);

  TextEditingController firstNameController = new TextEditingController();
  TextEditingController lastNameController = new TextEditingController();
  TextEditingController bibController = new TextEditingController();
  TextEditingController clubController = new TextEditingController();

  List<Racer> racer_list = new List();
  List<Manager> manager_list = new List();
  List<TrackingInformation> lst_tracking = new List();

  Future<void> initPlatformState() async {
    Map<String, dynamic> deviceData = <String, dynamic>{};

    try {
      if (Platform.isAndroid) {
        deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
      } else if (Platform.isIOS) {
        deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }

    if (!mounted) return;

    _deviceData = deviceData;
    get_service();
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'androidId': build.androidId,
      'systemFeatures': build.systemFeatures,
    };
  }

  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'identifierForVendor': data.identifierForVendor,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }

  bool isLoadingLogin = false;
  var _key_scaffold = GlobalKey<ScaffoldState>();
  List<Checked> checked_list = new List();
  String imei;
  bool read_only_bib = false;
  DetailsTracking detailsTracking;
  bool check_permission_gps_granted = false;

//  FocusNode myFocusNode;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key_scaffold,
//      floatingActionButton: FloatingActionButton(
//        onPressed: () async {
//          Geolocator.openLocationSettings();
//        },
//      ),
      endDrawer: get_drawer(),
      appBar: PreferredSize(
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.only(top: 8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 0),
                      child: Container(
                        child: Text(
                          "${event_l.name}",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4),
                      child: Container(
                        child: Text(
                          "${race.name}",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4),
                      child: Container(
                        child: Text(
                          "Registration",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.only(top: 15, right: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      child: InkWell(
                        onTap: () {
                          if (manager_list.length == 0) {
                            Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => LoginManagerFromRacer(
                                            event_l, race)))
                                .then((value) => get_manager());
                          } else if (manager_list.length > 0) {
                            Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) =>
                                            ControlPage(event_l, race)))
                                .then((value) => get_manager());
                          }
                        },
                        child: Container(
                          width: 20,
                          height: 25,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: manager_list.length == 0
                                      ? AssetImage('Images/profile.png')
                                      : AssetImage('Images/greenProfile.png'),
                                  fit: BoxFit.fill)),
                        ),
                      ),
                    ),
                    Container(
                      child: racer_list.length != 0 && checked_list.length != 0
                          ? InkWell(
                              onTap: () {
                                _key_scaffold.currentState.openEndDrawer();
//                                        _showMyDialog();
                              },
                              child: Stack(
                                alignment: Alignment.topRight,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(
                                        right: get_length_checked() > 0 ? 8 : 0,
                                        top: get_length_checked() > 0 ? 5 : 0),
                                    child: Container(
                                      width: 25,
                                      height: 25,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  'Images/redChecked.png'),
                                              fit: BoxFit.fill)),
                                    ),
                                  ),
                                  get_length_checked() > 0
                                      ? Container(
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Colors.orangeAccent,
                                              border: Border.all(
                                                  color: Colors.white,
                                                  width: 1)),
                                          child: Padding(
                                            padding: const EdgeInsets.all(2.0),
                                            child: Text(
                                              get_length_checked().toString(),
                                              style: TextStyle(
                                                  color: Colors.white),
                                              textAlign: TextAlign.center,
                                            ),
                                          ))
                                      : Container()
                                ],
                              ),
                            )
                          : Container(
                              width: 25,
                              height: 25,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image:
                                          AssetImage('Images/greyChecked.png'),
                                      fit: BoxFit.fill)),
                            ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: InkWell(
                child: Padding(
                  padding: EdgeInsets.only(top: 60),
                  child: Container(
                    width: 80,
                    height: 50,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('Images/LaLigaLogo.png'),
                            fit: BoxFit.fill)),
                  ),
                ),
                onTap: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (_) => EventsScreenRacer(true)),
                      (route) => false);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 120),
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.blue,
                height: 2,
              ),
            ),
          ],
        ),
        preferredSize: Size.fromHeight(100),
      ),
      backgroundColor: Colors.black,
      body: !check_permission_gps_granted
          ? Center(
              child: SpinKitThreeBounce(
                color: Colors.blue,
                size: 15,
              ),
            )
          : Stack(
              children: <Widget>[
                SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 15, right: 10, left: 10, bottom: 40),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
//              height: MediaQuery.of(context).size.height / 7,
                          decoration: BoxDecoration(
                              color: col.col_from_argb,
                              border: Border(
                                  top: BorderSide(
                                      color: Colors.blueGrey[50], width: 0.3))),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 5, right: 5),
                                  child: Container(
                                    child: Text(
                                        'Starting time : ${showdate(race.startTimeMs).toString().substring(10, 16)}',
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: font_size_body,
                                            fontWeight: FontWeight.w400)),
                                  ),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(left: 5, right: 5),
                                  child: Container(
                                    child: donecourses && doneRaces
                                        ? Text(
                                            "Distance: ${course != null ? course.distance != null ? removeDecimalZeroFormat(double.parse((course.distance.toInt() / 1000).toStringAsFixed(1))) : '...' : '...'} km",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: font_size_body,
                                                fontWeight: FontWeight.w400),
                                          )
                                        : SpinKitThreeBounce(
                                            size: font_size_body,
                                            color: Colors.blue,
                                          ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                      left: 5, right: 5, bottom: 10),
                                  child: SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                      children: <Widget>[
                                        Text(
                                          "Lap times: ",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: font_size_body,
                                              fontWeight: FontWeight.w400),
                                        ),
                                        course != null && donecourses
                                            ? Row(
                                                mainAxisSize: MainAxisSize.min,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: course.coursepoints
                                                    .where((element) =>
                                                        element.reference !=
                                                            'start' &&
                                                        element.reference !=
                                                            'finish')
                                                    .map((e) => Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 4),
                                                          child: Text(
                                                            '${removeDecimalZeroFormat(double.parse((e.distance / 1000).toStringAsFixed(1)))} km${course.coursepoints.lastWhere((element) => element.reference != 'start' && element.reference != 'finish') != e ? ',' : ''}',
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize:
                                                                    font_size_body,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w400),
                                                          ),
                                                        ))
                                                    .toList(),
                                              )
                                            : Container(),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 10.0, right: 10.0, top: 1.0),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: col.col_from_argb,
                                    border: Border(
                                        bottom: BorderSide(
                                            color: Colors.blueGrey[50],
                                            width: 0.3),
                                        top: BorderSide(
                                            color: Colors.blueGrey[50],
                                            width: 0.3))),
                                child: TextField(
//                            readOnly: racer != null ? true : false,
                                  onChanged: (value) {
                                    setState(() {
                                      firstNameController.text = value;
                                      firstNameController.selection =
                                          TextSelection.fromPosition(
                                              TextPosition(
                                                  offset: firstNameController
                                                      .text.length,
                                                  affinity:
                                                      TextAffinity.upstream));
                                    });
                                  },
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: font_size_body,
                                      fontWeight: FontWeight.w400),
                                  textAlign: TextAlign.left,
                                  controller: firstNameController,
                                  decoration: new InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                        left: 8,
                                        right: 8.0,
                                        bottom: 8.0,
                                        top: 10),
                                    //border: InputBorder.none,
                                    hintStyle: TextStyle(
                                        color: Colors.white,
                                        fontSize: font_size_body,
                                        fontStyle: FontStyle.italic,
                                        fontWeight: FontWeight.w400),
                                    hintText: 'Add your first name',
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 10.0, right: 10.0, bottom: 0.0),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: col.col_from_argb,
                                    border: Border(
                                        bottom: BorderSide(
                                            color: Colors.blueGrey[50],
                                            width: 0.3),
                                        top: BorderSide(
                                            color: Colors.blueGrey[50],
                                            width: 0.3))),
                                child: TextField(
//                            readOnly: racer != null ? true : false,
                                  onChanged: (value) {
                                    setState(() {
                                      lastNameController.text = value;
                                      lastNameController.selection =
                                          TextSelection.fromPosition(
                                              TextPosition(
                                                  offset: lastNameController
                                                      .text.length,
                                                  affinity:
                                                      TextAffinity.upstream));
                                    });
                                  },
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: font_size_body,
                                      fontWeight: FontWeight.w400),
                                  textAlign: TextAlign.left,
                                  controller: lastNameController,
                                  decoration: new InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                        left: 8,
                                        right: 8.0,
                                        bottom: 8.0,
                                        top: 10),
                                    //border: InputBorder.none,
                                    hintStyle: TextStyle(
                                        color: Colors.white,
                                        fontSize: font_size_body,
                                        fontStyle: FontStyle.italic,
                                        fontWeight: FontWeight.w400),
                                    hintText: 'Add your last name',
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 10.0, right: 10.0, bottom: 0.0),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: col.col_from_argb,
                                    border: Border(
                                        bottom: BorderSide(
                                            color: Colors.blueGrey[50],
                                            width: 0.3),
                                        top: BorderSide(
                                            color: Colors.blueGrey[50],
                                            width: 0.3))),
                                child: TextField(
                                  readOnly: read_only_bib,
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    WhitelistingTextInputFormatter.digitsOnly
                                  ],
                                  onChanged: (value) {
                                    setState(() {
                                      bibController.text = value;
                                      bibController.selection = TextSelection
                                          .fromPosition(TextPosition(
                                              offset: bibController.text.length,
                                              affinity: TextAffinity.upstream));
                                    });
                                  },
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: font_size_body,
                                      fontWeight: FontWeight.w400),
                                  textAlign: TextAlign.left,
                                  controller: bibController,
//                            readOnly:read_only_bib ,
                                  decoration: new InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                        left: 8,
                                        right: 8.0,
                                        bottom: 8.0,
                                        top: 10),
                                    //border: InputBorder.none,
                                    hintStyle: TextStyle(
                                        color: Colors.white,
                                        fontSize: font_size_body,
                                        fontStyle: FontStyle.italic,
                                        fontWeight: FontWeight.w400),
                                    hintText: 'Add your bib',
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 10.0, right: 10.0, bottom: 8.0),
                              child: Container(
                                decoration: BoxDecoration(
                                    color: col.col_from_argb,
                                    border: Border(
//                                  bottom: BorderSide(
//                                      color: Colors.blueGrey[50], width: 1),
                                        top: BorderSide(
                                            color: Colors.blueGrey[50],
                                            width: 0.3))),
                                child: TextField(
//                            readOnly: racer != null ? true : false,
                                  onChanged: (value) {
                                    setState(() {
                                      clubController.text = value;
                                      clubController.selection = TextSelection
                                          .fromPosition(TextPosition(
                                              offset:
                                                  clubController.text.length,
                                              affinity: TextAffinity.upstream));
                                    });
                                  },
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: font_size_body,
                                      fontWeight: FontWeight.w400),
                                  textAlign: TextAlign.left,
                                  controller: clubController,
                                  textDirection: TextDirection.ltr,
                                  decoration: new InputDecoration(
                                    contentPadding: EdgeInsets.only(
                                        left: 8,
                                        right: 8.0,
                                        bottom: 8.0,
                                        top: 10),
                                    //border: InputBorder.none,
                                    hintStyle: TextStyle(
                                        color: Colors.white,
                                        fontSize: font_size_body,
                                        fontStyle: FontStyle.italic,
                                        fontWeight: FontWeight.w400),
                                    hintText: 'Add your club (opt.)',
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      /***********d******************/
                      !isLoadingLogin
                          ? Padding(
                              padding: const EdgeInsets.only(
                                bottom: 40,
                                left: 10,
                                right: 10,
                              ),
                              child: Align(
                                alignment: Alignment.bottomCenter,
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 50),
                                  child: InkWell(
                                    onTap: () async {
                                      await login();
                                    },
                                    child: Container(
                                      decoration: BoxDecoration(
                                          color: Colors.blue,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10))),
                                      width: MediaQuery.of(context).size.width,
                                      height: 40,
                                      child: Center(
                                        child: Text(
                                          "SIGN ME UP",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: font_size_body,
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : SpinKitThreeBounce(
                              size: 17,
                              color: Colors.blue,
                            ),
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: InkWell(
                          onTap: () async {
//                        TrackingInformation trackingInformation =
//                            lst_tracking.firstWhere(
//                                (element) =>
//                                    element.reference == race.reference,
//                                orElse: () => null);
//                        DBProvider.db.TrackingInformation_list().then((value) {
//                          lst_tracking = value;
//                        });
                            Navigator.of(context).pop();
                          },
                          child: Container(
                            child: Text(
                              "Back",
                              style: TextStyle(
                                  color: Colors.blue,
                                  fontSize: font_size_body,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
    );
  }
  bool serviceEnabled=false;
  Future <void>  move (Function function){
    Future.delayed(Duration(seconds: 2),() async {
      if(!is_dispose){
        await function.call();
      }
    });
  }

  get_service() async {


    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      print("here>>>1");
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      toast("Please enable location service");
     move((){
       Geolocator.openLocationSettings();
     });


      delay_enable_location();


    } else {
      print("here>>>4");
      await check_and_request();
    }
    print("here>>>5");

  }
  Future<void> delay_enable_location(){
    Future.delayed(Duration(seconds: 2),() async {

      serviceEnabled = await Geolocator.isLocationServiceEnabled();

      if(!check_permission_gps_granted&&!is_dispose&&!call_check_and_request&&!serviceEnabled){
        delay_enable_location();
      }
      else if(serviceEnabled&&!call_check_and_request&&!is_dispose){
        check_and_request();

      }
    });
  }
  bool call_check_and_request=false;
  bool observe_permission=true;
  Future<void> delay_permission(){
    Future.delayed(Duration(seconds: 2),() async {
      LocationPermission permission;
      serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if(serviceEnabled&&!check_permission_gps_granted&&!is_dispose&&observe_permission){
        observe_permission=false;
        permission = await Geolocator.checkPermission();
        if (permission == LocationPermission.always ||
            permission == LocationPermission.whileInUse) {
          setState(() {
            check_permission_gps_granted = true;
          });
        }
      }
      else {
        delay_permission();
      }

    });
  }


  check_and_request() async {
    call_check_and_request=true;
    LocationPermission permission;
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.always ||
        permission == LocationPermission.whileInUse) {
      setState(() {
        check_permission_gps_granted = true;
      });
    } else if (permission == LocationPermission.denied &&
        Platform.isAndroid &&
        _deviceData.containsKey("version.sdkInt") &&
        int.tryParse(_deviceData['version.sdkInt'].toString()) < 30) {
      permission = await Geolocator.requestPermission();
    } else if (Platform.isAndroid &&
        _deviceData.containsKey("version.sdkInt") &&
        int.tryParse(_deviceData['version.sdkInt'].toString()) >= 30) {
//      _deviceData["version.sdkInt"]="30";
//              DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
//              AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;

      toast("Please grant GPS access permission");
      move((){
         Geolocator.openAppSettings();
      });

    }
    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    } else if (permission == LocationPermission.denied) {
      // Permissions are denied, next time you could try
      // requesting permissions again (this is also where
      // Android's shouldShowRequestPermissionRationale
      // returned true. According to Android guidelines
      // your App should show an explanatory UI now.
      return Future.error('Location permissions are denied');
    } else if (permission == LocationPermission.always ||
        permission == LocationPermission.whileInUse) {
      setState(() {
        check_permission_gps_granted = true;
      });
    }
    delay_permission();
  }

  Future<String> get_imei() async {
    try {
      imei = await ImeiPlugin.getImei();
      return imei;
    } catch (ex) {
      get_imei();
    }
  }

  Future<void> toast(String msg) async {
    if (msg != null) {
      Toast.show('${msg} ', context,
          backgroundColor: Colors.blueGrey, duration: Toast.LENGTH_LONG);
    }

    return;
  }

  load_detailsTracking() async {
    await DBProvider.db
        .DetailsTracking_list_from_race(race, event_l)
        .then((value) {
      if (value.length > 0) {
        detailsTracking = value[0];
      }
    });
  }

  Future RegisterNewTracker(Racer racer_new, String imei) async {
    print(">>>>>>>>>>>>>>.addRacer");
    try {
//      final result = await InternetAddress.lookup('google.com');
      if (2 > 1) {
        {
          var bodys = jsonEncode({
            "bib": racer_new.bib,
            "imei": imei,
          });
          await http
              .put(
                  'http://54.77.120.67:8080/rest/_App/trackings/${detailsTracking.reference_race}/trackers',
                  headers: <String, String>{
                    'Content-Type': 'application/json;charset=UTF-8',
                  },
                  body: bodys)
              .then((val) async {
            if (val.statusCode != 200) {
              setState(() {
                isLoadingLogin = false;
              });
              throw Exception('Failed to load data');
            } else if (val.body.isEmpty) {
              print("Empty");
            } else if (val.statusCode == 200) {
              Map<String, dynamic> data = jsonDecode(val.body);
//              Racer racer = Racer.fromJSONserver(data);
              if (data.containsKey("errors")) {
                print('here_erroe>>>>>');

                throw Exception('${data['errors']}');
              }
              List list_old = detailsTracking.trackers['trackers'];
              print('list_old>>>');
              list_old.add(data);
              await detailsTracking.trackers
                  .update('trackers', (value) => list_old);
              await DBProvider.db.update_detailstracking(detailsTracking);
              print("data>>>>> ${data}");
            }

//            Navigator.pop(context);
          });
        }
      }
      await http.post(
        'http://54.77.120.67:8080/rest/_App/trackings/${detailsTracking.reference_race}/trackers/${racer_new.bib}/checkin',
        headers: <String, String>{
          'Content-Type': 'application/json;charset=UTF-8',
        },
      ).then((value) async {
        if (value.statusCode != 200) {
          setState(() {
            isLoadingLogin = false;
          });
          throw Exception('Failed to load data');
        } else if (value.body.isEmpty) {
          throw Exception('Empty');
        } else if (value.statusCode == 200) {
          Map<String, dynamic> data = jsonDecode(value.body);
          if (data.containsKey("errors")) {
            print('here_erroe>>>>>');

            throw Exception('${data['errors']}');
          }
//              Racer racer = Racer.fromJSONserver(data);
          Tracker tracker = Tracker.fromJSON(data);
          if (tracker.status == 'checkedin' && tracker.devices.contains(imei)) {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (_) =>
                        CheckedScreen(event_l, race, racer_new, imei)),
                (route) => false);
          }
        }

//            N
      });

//      Navigator.push(
//          context,
//         );
//      Navigator.pushAndRemoveUntil(
//          context,
//          MaterialPageRoute(
//              builder: (_) =>
//                  CheckedScreen(event_l, race, racer, detailsTracking, imei)),
//          (route) => false);
    } on SocketException catch (_) {
      toast('${_.message}');
      setState(() {
        isLoadingLogin = false;
      });
    }
  }

  Future login() async {
    setState(() {
      isLoadingLogin = true;
    });
//old>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    /* await DBProvider.db.get_trackings_race(race, event_l, is_update: true);

    */
    await DBProvider.db.TrackingInformation_list().then((value) {
      lst_tracking = value;
    });
    TrackingInformation trackingInformation = lst_tracking.firstWhere(
        (element) =>
            element.reference == race.reference &&
            element.ref_event == event_l.reference,
        orElse: () => null);

    if (trackingInformation != null &&
            trackingInformation.status == Status.finished.asString() ||
        trackingInformation.status == Status.guned.asString()) {
      toast('sorry the race is finish or guned');

      setState(() {
        isLoadingLogin = false;
      });
      Navigator.pop(context);
      return;
    }

    if (firstNameController.text.isEmpty &&
        lastNameController.text.isEmpty &&
        bibController.text.isEmpty) {
      toast('Vänligen skriv ditt förnamn , ditt efternamn , ditt startnummer');
    } else if (firstNameController.text.isEmpty ||
        lastNameController.text.isEmpty ||
        bibController.text.isEmpty) {
      if (firstNameController.text.isEmpty) {
        toast('Vänligen skriv ditt förnamn');

        setState(() {
          isLoadingLogin = false;
        });
      } else if (lastNameController.text.isEmpty) {
        toast('Vänligen skriv ditt efternamn');

        setState(() {
          isLoadingLogin = false;
        });
      } else if (bibController.text.isEmpty) {
        toast('Vänligen skriv ditt startnummer');

        setState(() {
          isLoadingLogin = false;
        });
      }
    } else {
      setState(() {
        isLoadingLogin = true;
      });
      if (!read_only_bib) {
        //old>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

        /*  if (race.list_racers
                .where(
                    (element) => element.bib.toString() == bibController.text)
                .length >
            0) {
          toast('please change the bib');
          setState(() {
            isLoadingLogin = false;
          });
          return;
        }

       */
        await addRacer_d().then((value) async {
//          print("valrrrracer> ${value}");
//          if (value == null) return;
          if (value) {
            await push_with_check_open(trackingInformation);
          }
        });
      } else if (read_only_bib && !is_equal_racer()) {
        await doUpdate().then((value) async {
          if (value) {
            await push_with_check_open(trackingInformation);
          }
        });
      } else if (read_only_bib) {
        await push_with_check_open(trackingInformation);
      }
    }
  }

  bool is_equal_racer() {
    if (read_only_bib && racer.firstname != firstNameController.text ||
        racer.surname != lastNameController.text ||
        racer.club != clubController.text) return false;
    return true;
  }

  Future<void> push_with_check_open(
      TrackingInformation trackingInformation) async {
    print('racerpush_with_check_open>> ${racer}');

//    if (check_can_check_in(trackingInformation)) {
//      get_imei().then((imei) async {
//        await RegisterNewTracker(racer, imei);
//      });
//    } else
    {
      Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (_) => CheckMeInScreen(event_l, race, true)))
          .then((value) => get_racer());
    }
    setState(() {
      isLoadingLogin = false;
    });
    get_racer();
  }

  Future<bool> addRacer_d() async {
    bool state = false;
    try {
      if (2 > 1) {
        print(">>>>>>>>>>>>>>.addRacer  ${racer.toString()}");
        var bodys = jsonEncode({
          "bib": int.parse(bibController.text.trim()),
          "firstname": firstNameController.text.trim(),
          "surname": lastNameController.text.trim(),
          "club": clubController.text.trim(),
        });
        await http
            .post(
                'http://54.77.120.67:8080/rest/_App/races/${race.reference}/startlist/participants?format=LaLiga',
                headers: <String, String>{
                  'Content-Type': 'application/json;charset=UTF-8',
                },
                body: bodys)
            .catchError((erroe) {
          print('exxxxxca ${erroe}');
        }).then((val) async {
          if (val.statusCode != 200) {
            setState(() {
              isLoadingLogin = false;
            });
            throw Exception('please try again or chang the bib');
          } else {
            Map<String, dynamic> data = jsonDecode(val.body);
//              Racer racer = Racer.fromJSONserver(data);
            print('data>>>>>>>>>>>> ${data}');
            if (data.containsKey("errors")) {
              print('here_erroe>>>>>');

              throw Exception('${data['errors']}');
            }
            List list_old = race.racers['startlist'];
            print('list_old>>>');
            list_old.add(data);
            await race.racers.update('startlist', (value) => list_old);

            Racer racer_new = new Racer(
                id: int.parse(data['id']),
                bib: data['bib'],
                surname: data['surname'],
                firstname: data['firstname'],
                club: data['club'] == null ? '' : data['club']);

            racer_new.ref_race = race.reference;
            racer_new.ref_event = race.ref_event;
            racer = racer_new;
            await DBProvider.db.insert_race(race);
            await DBProvider.db.insert_racer(racer);

            print("racer>>>>> ${racer}");
            read_only_bib = true;
            read_only_bib = true;
            state = true;
            print("racernew>>>>> ${racer}");
            return state;
          }

//            Navigator.pop(context);
        });
        return state;
      }
    } on SocketException catch (_) {
      toast('Check internet connection !!');

      setState(() {
        isLoadingLogin = false;
      });
      state = false;
      return state;
    } on Exception catch (e) {
      toast('${e}');

      setState(() {
        isLoadingLogin = false;
      });
      state = false;
      return state;
    }
  }


  Future<bool> doUpdate() async {
    bool state = false;
    try {
//      final result = await InternetAddress.lookup('google.com');
      if (1 < 2) {
        if (firstNameController.text.isEmpty &&
            lastNameController.text.isEmpty) {
          toast(
              'Vänligen skriv ditt förnamn , ditt efternamn , ditt startnummer , club ');
        } else if (firstNameController.text.isEmpty ||
            lastNameController.text.isEmpty) {
          if (firstNameController.text.isEmpty) {
            toast('Vänligen skriv ditt förnamn');
          } else if (lastNameController.text.isEmpty) {
            toast('Vänligen skriv ditt efternamn');
          }
        } else {
          setState(() {
            isLoadingLogin = true;
          });
          var bodys = jsonEncode({
            "id": '${racer.id}',
            "bib": racer.bib,
            "firstname": firstNameController.text,
            "surname": lastNameController.text,
            "club": clubController.text
          });

          await http
              .put(
                  'http://54.77.120.67:8080/rest/_App/races/${race.reference}/startlist/participants/${racer.bib}?format=LaLiga',
                  body: bodys)
              .then((val) async {
            print('eeee>>>>> ${val.body}');
            if (val.statusCode != 200) {
              setState(() {
                isLoadingLogin = false;
              });
              throw Exception('Failed to load data');
            } else {
              Map<String, dynamic> data = jsonDecode(val.body);
              if (data.containsKey("errors")) {
                print('here_erroe>>>>>');

                throw Exception('${data['errors']}');
              }
              List list_old = race.racers['startlist'];
              print('data>>>>> ${data}');
              print('list_old>>> ${list_old}');
              list_old.forEach((element) {
                if (element['bib'].toString() == data['bib'].toString()) {
                  element = data;
                }
              });

              Racer racer_new = new Racer(
                  id: int.parse(data['id']),
                  bib: data['bib'],
                  surname: data['surname'],
                  firstname: data['firstname'],
                  club: data['club'] == null ? '' : data['club']);

              racer_new.ref_race = race.reference;
              racer_new.ref_event = race.ref_event;
              racer = racer_new;

              await DBProvider.db.insert_racer(racer);
              await race.racers.update('startlist', (value) => list_old);
              print('after>>>> ${race.racers}');
              await DBProvider.db.insert_race(race).then((value) async {
                await DBProvider.db
                    .Race_list_refresh(race, event_l)
                    .then((value) {
                  if (value.length > 0) {
                    setState(() {
                      race = value[0];
                    });
                  }
                });
              });
              state = true;
              await DBProvider.db.insert_racer(racer);
              return state;
            }
          });
          return state;
        }
      }
    } on SocketException catch (_) {
      state = false;
      setState(() {
        isLoadingLogin = false;
      });
      return state;
      toast('Check internet connection !!');
    } on Exception catch (_) {
      state = false;
      setState(() {
        isLoadingLogin = false;
      });
      return state;
    }
  }

  Future<void> get_course() {
    DBProvider.db.Course_from_race(race).then((value) {
      setState(() {
        course = value;
        donecourses = true;
      });
    });
    get_racer();
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('vill du logga ut?'),
          actions: <Widget>[
            FlatButton(
              child: Text('Annullera'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: () async {
                await DBProvider.db.Racer_list().then((value) {
                  if (value != null && value.length > 0) {
                    DBProvider.db.deleteAllRacer().then((value) {
                      setState(() {
                        racer_list = [];
                      });
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          '/EventsScreen', (Route<dynamic> route) => false);
                      return;
                    });
                  }
                });
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/EventsScreen', (Route<dynamic> route) => false);
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> get_racer() async {
//    await DBProvider.db.Racer_list_from(event_l, race).then((value) {
//      if(value!=null){
//        setState(() {
//          bibController.text=value.bib.toString();
//        });
//      }
//
//    });
    await DBProvider.db.Racer_list().then((value) {
      print('value>>> ${value}');
      if (value != null && value.length > 0) {
        setState(() {
          racer_list = value;
        });
      }
      if (value == null) {
        racer_list = [];
      }
    });
  }

  Future<void> refresh_race() async {
    setState(() {
      doneRaces = false;
    });
    await DBProvider.db.Race_list_refresh(race, event_l).then((value) {
      if (value != null && value.length > 0) {
        setState(() {
          race = value[0];
        });
      }
      setState(() {
        doneRaces = true;
      });
    });
    await get_course();
    await get_checked_list();
  }

  Future<void> load_controller() async {
    if (racer != null) {
      firstNameController.text = racer.firstname;
      lastNameController.text = racer.surname;

      clubController.text = racer.club;
    }
    await DBProvider.db.Racer_list_from(event_l, race).then((value) {
      if (value != null) {
        setState(() {
          bibController.text = value.bib.toString();
        });
      }
    });
  }

//  Future<void> get_information() async {
////    await DBProvider.db.get_information_race(race).then((value) async {
////      refresh_race();
////    });
//  }
  Future<void> get_manager() {
    DBProvider.db.Manager_list().then((value) {
      if (value.length > 0) {
        setState(() {
          manager_list = value;
        });
      } else {
        setState(() {
          manager_list = [];
        });
      }
    });
  }

  Drawer get_drawer() {
    child:
    return Drawer(
      child: Container(
        color: col.col_from_argb,
        child: ListView(
          scrollDirection: Axis.vertical,
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.only(top: 40),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white24),
                child: CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Container(
                    width: 80,
                    height: 50,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('Images/LaLigaLogo.png'),
                            fit: BoxFit.fill)),
                  ),
                ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: checked_list.map((e) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    child: InkWell(
                      onTap: () async {
                        Event_l event;
                        Race race;
                        await DBProvider.db.Events_list().then((value) {
                          event = value.firstWhere(
                              (element) => element.reference == e.ref_event,
                              orElse: () => null);
//                          print('event>>>> ${event}');
                        });
                        if (event != null) {
                          await DBProvider.db
                              .Race_list_from_event(event)
                              .then((value) {
                            race = value.firstWhere(
                                (element) => element.reference == e.reference,
                                orElse: () => null);
                          });
                          print('race>>>> ${race}');
                        }
                        print('here>>>>');
                        if (event != null &&
                            race != null &&
                            racer_list.firstWhere((element) =>
                                    element.ref_event == event.reference &&
                                    element.ref_race == race.reference) !=
                                null) {
                          print('here>>>ddddd>');
                          e.view = true;

                          await DBProvider.db.update_checked(e);
                          Navigator.of(context).pop();
                          Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) =>
                                          CheckMeInScreen(event, race, false)))
                              .then((value) {
                            get_checked_list();
                          });
                        }
                      },
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: e.view ? Colors.white54 : Colors.blueGrey,
                        ),
                        child: Center(
                          child: Text(
                            '${e.name_event} - ${e.name}',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: font_size_body,
                                fontWeight: FontWeight.w100),
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              }).toList(),
            )
          ],
        ),
      ),
    );
  }

  int get_length_checked() {
    return checked_list
        .where((element) => !element.view && element.is_open)
        .length;
  }

  Future<void> get_checked_list() async {
    await DBProvider.db.Checked_list_open_races().then((value) {
      if (value.length > 0) {
        setState(() {
          checked_list = value;
        });
      } else {
        setState(() {
          checked_list = [];
        });
      }
    });
  }

  String removeDecimalZeroFormat(double n) {
    return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 1);
  }

  print_c() {
    course.coursepoints.forEach((element) {
      print('element>>>>>>>>>>>>>>>> ${element.toString()}');
    });
  }

  @override
  void initState() {
    initPlatformState();

    get_manager();
    refresh_race();
    load_controller();
    load_detailsTracking();
//    myFocusNode = FocusNode();
    super.initState();
  }
  bool is_dispose=false;

  @override
  void dispose() {
//    myFocusNode = FocusNode();
    clubController.dispose();
    bibController.dispose();
    firstNameController.dispose();
    lastNameController.dispose();
    is_dispose=true;

    super.dispose();
  }
}
