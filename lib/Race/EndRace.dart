import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:http/http.dart' as http;
import 'package:racer_app_finish/Appearance.dart';
import 'package:racer_app_finish/Models/Event.dart';
import 'package:racer_app_finish/Models/Race.dart';
import 'package:racer_app_finish/Models/Racer.dart';
import 'package:racer_app_finish/Screen/FinishScreen.dart';
import 'package:racer_app_finish/User/User.dart';
import 'package:toast/toast.dart';

import '../Database.dart';
import 'Race.dart';
import 'package:racer_app_finish/ColorsF.dart' as col;

class EndRace extends StatefulWidget {
  Event_l event_l;
  Race race;

  Racer racer;

  EndRace(this.event_l, this.race, this.racer);

  @override
  _EndRaceState createState() => _EndRaceState(event_l, race, racer);
}

class _EndRaceState extends State<EndRace> {
  Event_l event_l;
  Race race;

  Racer racer;
  bool isLoadingSubmitted = false;

  _EndRaceState(this.event_l, this.race, this.racer);

//  end_race() async {
//    setState(() {
//      isLoadingSubmitted = true;
//    });
//    try {
//      await http.post('$url/end_race.php', body: {
//        'id_loop': loop.id.toString(),
//        'User_name_p': user.firstName.toString(),
//        'User_name_last': user.lastName.toString(),
//      }).then((data) async {
//        var da = jsonDecode(data.body);
//        print(da);
//        setState(() {
//          isLoadingSubmitted = false;
//        });
//        if (da["id"]["End_race"] == "true") {
//          loop.state = false;
//          await DBProvider.db.update_loops(loop, loop.id);
//          Navigator.of(context).pushNamedAndRemoveUntil(
//              '/RacesScreen', (Route<dynamic> route) => false);
//        } else if (da["id"] == "You are not subscribed") {
//          Toast.show("You are not subscribed", context,
//              backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
//        }
//      });
//    } catch (ex) {
//      print(ex);
//    }
//  }'
  void displayBottomSheet(BuildContext context) {
    showModalBottomSheet(
        context: context,
        builder: (ctx) {
          return SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: <Widget>[
                Container(
                  color: col.col_from_argb,
                  width: MediaQuery.of(context).size.width,
                  height: 100,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0, top: 8.0),
                        child: Text(
                          "Do you want to end your race?\n You will get registered as DNF\n(Did not finish).",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: font_size_body,
                              fontWeight: FontWeight.w100),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 24.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (_) =>
                                  FinishedScreen(event_l, race, racer)));
                    },
                    child: isLoadingSubmitted
                        ? SpinKitThreeBounce(
                      color: Colors.red,
                      size: 17,
                    )
                        : Container(
                      decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius:
                          BorderRadius.all(Radius.circular(10))),
                      width: 250,
                      height: 50,
                      child: Center(
                        child: Text(
                          "END MY RACE",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: font_size_body,
                              fontWeight: FontWeight.w300),
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 24.0),
                  child: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Container(
                      decoration: BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.all(Radius.circular(10))),
                      width: 250,
                      height: 50,
                      child: Center(
                        child: Text(
                          "KEEP ON RACING",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: font_size_body,
                              fontWeight: FontWeight.w300),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: Container(
          height: 175,
          child: Center(
            child: Padding(
              padding: const EdgeInsets.only(top: 24.0),
              child: Padding(
                padding: const EdgeInsets.only(top: 0.0),
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      //color: Colors.white,
                                      width: 200,
                                      child: Center(
                                        child: Text(
                                          event_l.name,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 15,
                                              fontWeight: FontWeight.w100),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child: Container(
                                        //color: Colors.white,
                                        width: 200,
                                        child: Center(
                                          child: Text(
                                            race.name,
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 15,
                                                fontWeight: FontWeight.w100),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child: Container(
                                        width: 200,
                                        child: Center(
                                          child: Text(
                                            "Start",
                                            style: TextStyle(
                                                color: Colors.white54,
                                                fontSize: 15,
                                                fontWeight: FontWeight.w100),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          color: Colors.blue,
                          height: 2,
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        preferredSize: Size.fromHeight(120),
      ),
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          children: <Widget>[
            Container(
              color: col.col_from_argb,
              width: MediaQuery.of(context).size.width,
              height: 100,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, top: 8.0),
                    child: Text(
                      "Do you want to end your race?\n You will get registered as DNF\n(Did not finish).",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: font_size_body,
                          fontWeight: FontWeight.w100),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 24.0),
              child: InkWell(
                onTap: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (_) =>
                              FinishedScreen(event_l, race, racer)));
                },
                child: isLoadingSubmitted
                    ? SpinKitThreeBounce(
                        color: Colors.red,
                        size: 17,
                      )
                    : Container(
                        decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius:
                                BorderRadius.all(Radius.circular(10))),
                        width: 250,
                        height: 50,
                        child: Center(
                          child: Text(
                            "END MY RACE",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: font_size_body,
                                fontWeight: FontWeight.w300),
                          ),
                        ),
                      ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 24.0),
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  decoration: BoxDecoration(
                      color: Colors.blue,
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                  width: 250,
                  height: 50,
                  child: Center(
                    child: Text(
                      "KEEP ON RACING",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: font_size_body,
                          fontWeight: FontWeight.w300),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  void initState() {
    displayBottomSheet(context);
    super.initState();
  }
}
