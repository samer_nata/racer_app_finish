//class Race {
//  int id;
//  String name;
//  String location;
//  String date_race;
//  bool state=false;
//
//  Race({this.id, this.name, this.location, this.date_race, this.state});
//
//  factory Race.fromJSON(json) {
//    return Race(
//        id: json["id"],
//        name: json["name"],
//        date_race: json["date_race"],
//        location: json["location"],
//        state: json["state"] == 0 ? false : true);
//  }
//
//  factory Race.fromJSONserver(json) {
//    return Race(
//        id: int.parse(json["id"]),
//        name: json["name"],
//        date_race: json["date_race"],
//        location: json["location"],
//        state: json["state"] == "0" ? false : true);
//  }
//
//  Map<String, dynamic> toMap() => {
//    "id": id,
//    "name": name,
//    "date_race": date_race,
//    "state": state == true ? 1 : 0,
//    "location": location,
//
//  };
//
//  @override
//  String toString() {
//    return 'Race{id: $id, name: $name, location: $location, date_race: $date_race, state: $state}';
//  }
//}
