import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:http/http.dart' as http;
import 'package:imei_plugin/imei_plugin.dart';
import 'package:racer_app_finish/Enum.dart';
import 'package:racer_app_finish/ManagerFunction/OurTabBarView/ScreenManager/ControlPage.dart';
import 'package:racer_app_finish/ManagerFunction/OurTabBarView/ScreenManager/EventsScreenManager.dart';
import 'package:racer_app_finish/ManagerFunction/OurTabBarView/ScreenManager/Login_Manager_From_Racer.dart';
import 'package:racer_app_finish/ManagerFunction/OurTabBarView/ScreenManager/RacesScreenManager.dart';
import 'package:racer_app_finish/Models/Checked.dart';

import 'package:racer_app_finish/Models/Course.dart';
import 'package:racer_app_finish/Models/Details_Tracking.dart';
import 'package:racer_app_finish/Models/Event.dart';
import 'package:racer_app_finish/Models/Manager.dart';
import 'package:racer_app_finish/Models/Race.dart';
import 'package:racer_app_finish/Models/Racer.dart';
import 'package:racer_app_finish/Models/Trackers.dart';
import 'package:racer_app_finish/Models/TrackingWithInformation.dart';
import 'package:racer_app_finish/Screen/EventsScreenRacer.dart';
import 'package:racer_app_finish/User/User.dart';
import 'package:racer_app_finish/ColorsF.dart' as col;
import 'package:toast/toast.dart';
//import 'package:wakelock/wakelock.dart';

import '../Appearance.dart';
import '../Database.dart';
import '../SplashScreen.dart';
import 'CheckedScreen.dart';

class CheckMeInScreen extends StatefulWidget {
  Event_l event_l;
  Race race;

  bool is_first;

  CheckMeInScreen(this.event_l, this.race, this.is_first);

  @override
  _CheckMeInScreenState createState() =>
      _CheckMeInScreenState(event_l, race, is_first);
}

class _CheckMeInScreenState extends State<CheckMeInScreen> {
  bool is_first;
  Event_l event_l;
  Race race;

  Racer racer;
  bool is_finish_load = false;
  String imei;
  TrackingInformation trackingInformation;

  _CheckMeInScreenState(this.event_l, this.race, this.is_first);

  bool isLoadingSubmitted = false;
  Duration duration = new Duration(minutes: 30);

  List laps_list = new List();
  Course course;
  var _key_scaffold = GlobalKey<ScaffoldState>();
  List<Checked> checked_list = new List();
  bool donecourses = false;
  bool doneRaces = false;
  bool donefromserver = false;
  DetailsTracking detailsTracking;
  List<Racer> racer_list = new List();
  List<Manager> manager_list = new List();
  List<Checked> list_checked = new List();
  Checked checked;
  bool is_dispose = false;

//  get_laps() {
//    DBProvider.db.LapsTimes_list(loop.id).then((data) {
//      setState(() {
//        laps_list = data;
//      });
//    });
//  }
//
//get_course() async {
//  List<Course> lst =
//      await DBProvider.db.Course_list_from_id(race.courseId);
//   course = lst[0];
//}
  Drawer get_drawer() {
    child:
    return Drawer(
      child: Container(
        color: col.col_from_argb,
        child: ListView(
          scrollDirection: Axis.vertical,
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.only(top: 40),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white24),
                child: CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Container(
                    width: 80,
                    height: 50,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('Images/LaLigaLogo.png'),
                            fit: BoxFit.fill)),
                  ),
                ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: checked_list.map((e) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    child: InkWell(
                      onTap: () async {
                        Event_l event;
                        Race race;
                        await DBProvider.db.Events_list().then((value) {
                          event = value.firstWhere(
                              (element) => element.reference == e.ref_event,
                              orElse: () => null);
//                          print('event>>>> ${event}');
                        });
                        if (event != null) {
                          await DBProvider.db
                              .Race_list_from_event(event)
                              .then((value) {
                            race = value.firstWhere(
                                (element) => element.reference == e.reference,
                                orElse: () => null);
                          });
                          print('race>>>> ${race}');
                        }
                        print('here>>>>');
                        if (event != null &&
                            race != null &&
                            racer_list.firstWhere((element) =>
                                    element.ref_event == event.reference &&
                                    element.ref_race == race.reference) !=
                                null) {
                          print('here>>>ddddd>');
                          e.view = true;

                          await DBProvider.db.update_checked(e);
                          Navigator.of(context).pop();
                          Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) =>
                                          CheckMeInScreen(event, race, false)))
                              .then((value) {
                            get_checked_list();
                          });
                        }
                      },
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: e.view ? Colors.white54 : Colors.blueGrey,
                        ),
                        child: Center(
                          child: Text(
                            '${e.name_event} - ${e.name}',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 18,
                                fontWeight: FontWeight.w100),
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              }).toList(),
            )
          ],
        ),
      ),
    );
  }

  Future<void> get_course() async {
    await DBProvider.db.Course_from_race(race).then((value) {
      setState(() {
        course = value;
        donecourses = true;
      });
    });
  }

  Future<void> get_checked_list() async {
    await DBProvider.db.Checked_list_open_races().then((value) {
      if (value.length > 0) {
        setState(() {
          checked_list = value;
        });
      } else {
        setState(() {
          checked_list = [];
        });
      }
    });
  }

  Future<void> get_manager() {
    DBProvider.db.Manager_list().then((value) {
      if (value.length > 0) {
        setState(() {
          manager_list = value;
        });
      } else {
        setState(() {
          manager_list = [];
        });
      }
    });
  }

  Future<void> get_racer() async {
    await DBProvider.db.Racer_list().then((value) {
      if (value != null && value.length > 0) {
        racer_list = value;
        if (value.firstWhere(
                (element) =>
                    element.ref_event == event_l.reference &&
                    element.ref_race == race.reference,
                orElse: () => null) !=
            null) {
          racer = value.firstWhere(
              (element) =>
                  element.ref_event == event_l.reference &&
                  element.ref_race == race.reference,
              orElse: () => null);
          is_finish_load = true;
          print('racer>>>>>>>>>>>> ${racer.toString()}');
          checked = new Checked(
              state: false,
              bib: racer.bib,
              reference: race.reference,
              view: false,
              is_open: false,
              name: race.name,
              ref_event: event_l.reference,
              name_event: event_l.name);
        }
      }
      if (value == null) {
        racer_list = [];
      }
      setState(() {});
      get_checked_list();
    });
  }

  Future<void> refresh_race() async {
    doneRaces = false;

    await DBProvider.db.Race_list_refresh(race, event_l).then((value) {
      if (value.length > 0) {
        setState(() {
          race = value[0];
        });
      }
      setState(() {
        doneRaces = true;
      });
    });
    await get_infrormation();

    await DBProvider.db
        .DetailsTracking_list_from_race(race, event_l)
        .then((value) {
      if (value.length > 0) {
        detailsTracking = value[0];
      }
      setState(() {
        donefromserver = true;
      });
    });
    get_tracking_per_one_minute();
    get_course();
  }

  Future get_imei() async {
    try {
      imei = await ImeiPlugin.getImei();
    } catch (ex) {
      get_imei();
    }
  }

  Future<void> get_infrormation() async {
    await DBProvider.db
        .TrackingInformation_list_from_race(event_l, race)
        .then((value) {
      if (value.length > 0) {
        trackingInformation = value[0];
//          print('trackingInformation>>TH ${trackingInformation.openingTimeMs}');

      }
    });
  }

  int get_length_checked() {
    return checked_list
        .where((element) => !element.view && element.is_open)
        .length;
  }

  Future<void> get_checked() async {
    await DBProvider.db.Checked_list_from_race(race, event_l).then((value) {
      print('>>>>>>>>>>>>>>CHECKED ${value}');
      if (value.length > 0) {
        setState(() {
          checked = value[0];
        });
      }
    });
  }

  List<TrackingInformation> lst_tracking = new List();

  get_tracking_per_one_minute() async {
    await DBProvider.db.get_trackings_race(race, event_l, is_update: true);

    await DBProvider.db
        .TrackingInformation_list_from_race(event_l, race)
        .then((value) {
      lst_tracking = value;
//      print('lst_tracking>>> ${lst_tracking}');
    });

    TrackingInformation information = lst_tracking.firstWhere(
        (element) => element.reference == race.reference,
        orElse: () => null);

    if (!is_dispose) {
//      print(
//          'invoked>>>>>> ${trackingInformation.openingTimeMs} ${DateTime.now().difference(trackingInformation.openingTimeMs).inMinutes} '
//          '${DateTime.now().difference(trackingInformation.openingTimeMs).inMinutes >= -60}  ${DateTime.now().difference(trackingInformation.openingTimeMs).inDays == 0}');
      setState(() {
        trackingInformation = information;
      });
    }

    Future.delayed(Duration(minutes: 1), () async {
      if (!is_dispose) {
        get_tracking_per_one_minute();
      }
    });
  }

  @override
  void initState() {

    checked = new Checked(
        state: false,
        reference: race.reference,
        view: false,
        is_open: false,
        name: race.name,
        ref_event: event_l.reference,
        name_event: event_l.name);
//    checked=  new Checked( state: false,bib: racer.bib,reference: race.reference,view: false,is_open: false,name: race.name, ref_event: event_l.name);
    load();

    super.initState();
  }

  void load() {
    get_manager();
    refresh_race();
    get_racer();
    get_checked();

    get_imei();
  }

  @override
  void dispose() {
//    Wakelock.disable();
    is_dispose = true;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key_scaffold,

      endDrawer: get_drawer(),
      appBar: PreferredSize(
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.only(top: 8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 0),
                      child: Container(
                        child: Text(
                          "${event_l.name}",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4),
                      child: Container(
                        child: Text(
                          "${race.name}",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4),
                      child: Container(
                        child: Text(
                          "Confirmation",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.only(top: 15, right: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      child: InkWell(
                        onTap: () {
                          if (manager_list.length == 0) {
                            Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => LoginManagerFromRacer(
                                            event_l, race)))
                                .then((value) => get_manager());
                          } else if (manager_list.length > 0) {
                            Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) =>
                                            ControlPage(event_l, race)))
                                .then((value) => get_manager());
                          }
                        },
                        child: Container(
                          width: 20,
                          height: 25,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: manager_list.length == 0
                                      ? AssetImage('Images/profile.png')
                                      : AssetImage('Images/greenProfile.png'),
                                  fit: BoxFit.fill)),
                        ),
                      ),
                    ),
                    Container(
                      child: racer_list.length != 0 && checked_list.length != 0
                          ? InkWell(
                              onTap: () {
                                _key_scaffold.currentState.openEndDrawer();
//                                        _showMyDialog();
                              },
                              child: Stack(
                                alignment: Alignment.topRight,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(
                                        right: get_length_checked() > 0 ? 8 : 0,
                                        top: get_length_checked() > 0 ? 5 : 0),
                                    child: Container(
                                      width: 25,
                                      height: 25,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  'Images/redChecked.png'),
                                              fit: BoxFit.fill)),
                                    ),
                                  ),
                                  get_length_checked() > 0
                                      ? Container(
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Colors.orangeAccent,
                                              border: Border.all(
                                                  color: Colors.white,
                                                  width: 1)),
                                          child: Padding(
                                            padding: const EdgeInsets.all(2.0),
                                            child: Text(
                                              get_length_checked().toString(),
                                              style: TextStyle(
                                                  color: Colors.white),
                                              textAlign: TextAlign.center,
                                            ),
                                          ))
                                      : Container()
                                ],
                              ),
                            )
                          : Container(
                              width: 25,
                              height: 25,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image:
                                          AssetImage('Images/greyChecked.png'),
                                      fit: BoxFit.fill)),
                            ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: InkWell(
                child: Padding(
                  padding: EdgeInsets.only(top: 60),
                  child: Container(
                    width: 80,
                    height: 50,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('Images/LaLigaLogo.png'),
                            fit: BoxFit.fill)),
                  ),
                ),
                onTap: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (_) => EventsScreenRacer(true)),
                      (route) => false);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 120),
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.blue,
                height: 2,
              ),
            ),
          ],
        ),
        preferredSize: Size.fromHeight(100),
      ),
      backgroundColor: Colors.black,
//      floatingActionButton: FloatingActionButton(onPressed: () async {
//       await DBProvider.db.Checked_list();
//      },),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            is_finish_load
                ? Padding(
                    padding: const EdgeInsets.only(left: 3, right: 3, top: 15),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 300,
                      decoration: BoxDecoration(
                          color: col.col_from_argb,
                          border: Border(
                              top: BorderSide(
                                  color: Colors.blueGrey[50], width: 0.3))),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 4, top: 8),
                            child: Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.only(
                                    left: 8.0,
                                  ),
                                  child: Text(
                                    "${racer.firstname} ${racer.surname} ",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: font_size_body,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ),
//                          Padding(
//                            padding: const EdgeInsets.only(
//                              left: 8.0,
//                            ),
//                            child: Text(
//                              "",
//                              style: TextStyle(
//                                  color: Colors.white,
//                                  fontSize: 15,
//                                  fontWeight: FontWeight.w400),
//                            ),
//                          ),
                              ],
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 8.0, bottom: 4.0),
                            child: Text(
                              "club ${racer.club}",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: font_size_body,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.only(left: 8.0, bottom: 4.0),
                            child: Text(
                              "Startnumber ${racer.bib}",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: font_size_body,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
//                    Padding(
//                      padding: const EdgeInsets.only(left: 8.0, bottom: 4.0,top: 12),
//                      child: Text(
//                        "${race.name}",
//                        style: TextStyle(
//                            color: Colors.white,
//                            fontSize: 15,
//                            fontWeight: FontWeight.w400),
//                      ),
//                    ),

                          is_first
                              ? Padding(
                                  padding: const EdgeInsets.only(
                                      left: 8.0, bottom: 4.0),
                                  child: InkWell(
                                    onTap: () {
                                      Navigator.pop(context);
                                    },
                                    child: Text(
                                      "change",
                                      style: TextStyle(
                                          color: Colors.blue,
                                          fontSize: font_size_body,
                                          fontWeight: FontWeight.w400),
                                    ),
                                  ),
                                )
                              : Container(),

                          Padding(
                            padding: const EdgeInsets.only(
                                left: 8.0, bottom: 4.0, top: 30),
                            child: Text(
                              "Starttid:${showdate(race.startTimeMs).toString().substring(10, 16)}",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: font_size_body,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                              left: 8.0,
                              bottom: 3.0,
                            ),
                            child: donecourses && doneRaces
                                ? Text(
                                    "Distans: ${course != null ? course.distance != null ? removeDecimalZeroFormat(double.parse((course.distance / 1000).toStringAsFixed(1))) : '...' : '...'} km",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: font_size_body,
                                        fontWeight: FontWeight.w400),
                                  )
                                : SpinKitThreeBounce(
                                    size: font_size_body,
                                    color: Colors.blue,
                                  ),
                          ),
//                    Padding(
//                      padding: const EdgeInsets.only(
//                        left: 8.0,
//                        top: 8.5,
//                      ),
//                      child: SingleChildScrollView(
//                        scrollDirection: Axis.horizontal,
//                        child: Row(
//                          children: <Widget>[
//                            Text(
//                              "Lap times : ",
//                              style: TextStyle(
//                                  color: Colors.white,
//                                  fontSize: 17,
//                                  fontWeight: FontWeight.w100),
//                            ),
//                            course != null && donecourses
//                                ? Row(
//                                    children: List.generate(
//                                        course.coursepoints.length, (index) {
//                                      return Padding(
//                                        padding: const EdgeInsets.only(left: 4),
//                                        child: Text(
//                                          '${(course.coursepoints[index].distance / 1000).toStringAsFixed(1)} km',
//                                          style: TextStyle(
//                                              color: Colors.white,
//                                              fontSize: 17,
//                                              fontWeight: FontWeight.w100),
//                                        ),
//                                      );
//                                    }),
//                                  )
//                                : Container(),
//                          ],
//                        ),
//                      ),
//                    ),
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 8.0, bottom: 20.0, top: 20.0),
                            child: Text(
                             // "Checkin-tid: ${race.startTimeMs.subtract(Duration(minutes: 30)).hour}${race.startTimeMs.subtract(Duration(minutes: 30)).minute != 0 ? ':${race.startTimeMs.subtract(Duration(minutes: 30)).minute}' : ''}  - ${race.startTimeMs.hour}${race.startTimeMs.minute != 0 ? ':${race.startTimeMs.minute}' : ''}",
                              "Checkin-tid: ${showdate(race.startTimeMs).subtract(Duration(minutes: 30)).toString().substring(10,16)}  -${showdate(race.startTimeMs).toString().substring(10, 16)} ",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: font_size_body,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                : Container(),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: trackingInformation != null &&
                      check_can_check_in(trackingInformation) &&
                      !isLoadingSubmitted &&
                      donefromserver
                  ? InkWell(
                      onTap: () async {
                        chang_loading(true);
//                        await get_infrormation().then((value) async {
//                          await RegisterNewTracker(racer);
//                        });
                        await RegisterNewTracker(racer);
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 85,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(Radius.circular(10)),
                            gradient: LinearGradient(
                                colors: <Color>[
                                  Colors.orange,
                                  Colors.orange,
                                  Colors.orange,
                                  Colors.orange,
                                  Colors.deepOrange,
                                  Colors.deepOrange
                                ],
                                begin: Alignment.topLeft,
                                end: Alignment.bottomRight)),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Stack(
                            children: <Widget>[
                              SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Stack(
                                  children: <Widget>[
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 3.0),
                                          child: Text(
                                            "I AM READY TO RACE. ",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: font_size_body,
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 3.0),
                                          child: Text(
                                            "CHECK ME IN!",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: font_size_body,
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 3.0),
                                          child: Text(
                                            'You can’t go back after check-in',
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: font_size_body,
                                                fontWeight: FontWeight.w400),
                                          ),
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 24.0),
                                  child: Container(
                                    width: 60,
                                    height: 55,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            image: AssetImage(
                                                'Images/whiteChecked.png'),
                                            fit: BoxFit.fill)),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                  : !isLoadingSubmitted
                      ? Container()
                      : SpinKitThreeBounce(
                          size: 17,
                          color: Colors.orange,
                        ),
            ),
            Padding(
              padding: const EdgeInsets.all(4.0),
              child: trackingInformation != null
//                  &&
//                  (trackingInformation.status !=
//                      Status.finished.asString() &&
//                      trackingInformation.status !=
//                          Status.guned.asString())
                      &&
                      !check_can_check_in(trackingInformation) &&trackingInformation.status!=Status.finished.asString()&&
                      donefromserver
                  ? Container(
                      color: Color(0xff121214),
                      width: MediaQuery.of(context).size.width,
                      height: 70,
                      child: Row(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              width: 35,
                              height: 32,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image:
                                          AssetImage('Images/redChecked.png'),
                                      fit: BoxFit.fill)),
                            ),
                          ),
                          Expanded(
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, bottom: 4.0),
                              child: Text(
                                'The check  - in is not open yet\nSend me a notice when open.',
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: font_size_body,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ),
                          checked != null
                              ? Align(
                                  alignment: Alignment.centerRight,
                                  child: Theme(
                                    data: ThemeData(
                                        unselectedWidgetColor: Colors.white),
                                    child: Checkbox(
                                      checkColor: Colors.blue,
                                      activeColor: Colors.white,
                                      onChanged: (bool value) async {
                                        checked.state = value;
                                        await DBProvider.db
                                            .insert_checked(checked);
                                        setState(() {});
                                      },
                                      value: checked.state,
                                    ),
                                  ),
                                )
                              : Container()
                        ],
                      ),
                    )
                  : Container(),
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 16.0,
                top: 3.0,
              ),
              child: InkWell(
                onTap: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (_) => EventsScreenRacer(true)),
                      (route) => false);
                },
                child: Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Container(
                    child: Text(
                      "Back to main menu",
                      style: TextStyle(
                          color: Colors.blue,
                          fontSize: font_size_body,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

//  bool check_can_check_in() {
//    return (trackingInformation.status == Status.open.asString() ||
//                        trackingInformation.status ==
//                                Status.waiting_for_gun.asString() &&
//                            (trackingInformation.openingTimeMs
//                                        .difference(DateTime.now())
//                                        .inMinutes <=
//                                    60 &&
//                                DateTime.now()
//                                        .difference(
//                                            trackingInformation.openingTimeMs)
//                                        .inDays ==
//                                    0));
//  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('vill du logga ut?'),
          actions: <Widget>[
            FlatButton(
              child: Text('Annullera'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: () async {
                await DBProvider.db.Racer_list().then((value) {
                  if (value != null && value.length > 0) {
                    DBProvider.db.deleteAllRacer().then((value) {
                      setState(() {
                        racer_list = [];
                      });
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          '/SplashScreen', (Route<dynamic> route) => false);
                      return;
                    });
                  }
                });
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/SplashScreen', (Route<dynamic> route) => false);
              },
            ),
          ],
        );
      },
    );
  }

  Future RegisterNewTracker(Racer racer_new) async {
    print(">>>>>>>>>>>>>>.addRacer");
    try {
//      final result = await InternetAddress.lookup('google.com');
      if (2 > 1) {
        {
          var bodys = jsonEncode({
            "bib": racer_new.bib,
            "imei": imei,
          });
          await http
              .put(
                  'http://54.77.120.67:8080/rest/_App/trackings/${detailsTracking.reference_race}/trackers',
                  headers: <String, String>{
                    'Content-Type': 'application/json;charset=UTF-8',
                  },
                  body: bodys)
              .then((val) async {
            print('RegisterNewTracker>>>');
            if (val.statusCode != 200) {
              chang_loading(false);
              throw Exception('Failed to load data');
            } else if (val.body.isEmpty) {
              print("Empty");
            } else if (val.statusCode == 200) {
              Map<String, dynamic> data = jsonDecode(val.body);
//              Racer racer = Racer.fromJSONserver(data);
              print('data>>${data}');
              if (data.containsKey("errors")) {
                print('here_erroe>>>>>');

                throw Exception('${data['errors']}');
              }

              List list_old = detailsTracking.trackers['trackers'];
              print('list_old>>>');
              list_old.add(data);
              await detailsTracking.trackers
                  .update('trackers', (value) => list_old);
              await DBProvider.db.update_detailstracking(detailsTracking);
              print("data>>>>> ${data}");
            }

//            Navigator.pop(context);
          });
        }
      }
      await http.post(
        'http://54.77.120.67:8080/rest/_App/trackings/${detailsTracking.reference_race}/trackers/${racer_new.bib}/checkin',
        headers: <String, String>{
          'Content-Type': 'application/json;charset=UTF-8',
        },
      ).then((value) async {
        if (value.statusCode != 200) {

          chang_loading(false);
          throw Exception('Failed to load data');
        } else if (value.body.isEmpty) {
          throw Exception('Empty');
        } else if (value.statusCode == 200) {
          Map<String, dynamic> data = jsonDecode(value.body);
//              Racer racer = Racer.fromJSONserver(data);
          if (data.containsKey("errors")) {
            print('here_erroe>>>>>');

            throw Exception('${data['errors']}');
          }
          Tracker tracker = Tracker.fromJSON(data);
          if (tracker.status == 'checkedin' && tracker.devices.contains(imei)) {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(
                    builder: (_) =>
                        CheckedScreen(event_l, race, racer_new, imei)),
                (route) => false);
          }
        }

//            N
      });
    } on SocketException catch (_) {
      chang_loading(false);
      toast('${_.message}');
    }
  }

  void chang_loading(bool state) {
    setState(() {
      isLoadingSubmitted = state;
    });
  }

  void toast(String msg) {
    Toast.show("${msg}", context,
        backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
  }

  String removeDecimalZeroFormat(double n) {
    return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 1);
  }
}
