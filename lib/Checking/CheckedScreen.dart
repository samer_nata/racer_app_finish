import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geolocator/geolocator.dart';

import 'package:racer_app_finish/Appearance.dart';
import 'package:racer_app_finish/Enum.dart';
import 'package:racer_app_finish/ManagerFunction/OurTabBarView/ScreenManager/EventsScreenManager.dart';
import 'package:racer_app_finish/ManagerFunction/OurTabBarView/ScreenManager/Login_Manager_From_Racer.dart';
import 'package:racer_app_finish/ManagerFunction/OurTabBarView/ScreenManager/RacesScreenManager.dart';
import 'package:racer_app_finish/Models/Course.dart';
import 'package:racer_app_finish/Models/Coursepoints.dart';
import 'package:racer_app_finish/Models/Details_Tracking.dart';
import 'package:racer_app_finish/Models/Event.dart';
import 'package:racer_app_finish/Models/Manager.dart';
import 'package:racer_app_finish/Models/Race.dart';
import 'package:racer_app_finish/Models/Racer.dart';
import 'package:racer_app_finish/Models/Trackers.dart';
import 'package:racer_app_finish/Models/TrackingWithInformation.dart';
import 'package:racer_app_finish/Race/EndRace.dart';
import 'package:racer_app_finish/Race/Race.dart';
import 'package:racer_app_finish/Screen/FinishScreen.dart';
import 'package:racer_app_finish/User/User.dart';
import 'package:toast/toast.dart';
import 'package:http/http.dart' as http;
import 'package:racer_app_finish/ColorsF.dart' as col;

//import 'package:wakelock/wakelock.dart';
//import 'package:wakelock/wakelock.dart';

import '../Database.dart';
import '../LiveResultsRacer.dart';
import 'PositionItem.dart';

class CheckedScreen extends StatefulWidget {
  Event_l event_l;
  Race race;

  Racer racer;

  String imei;

  CheckedScreen(this.event_l, this.race, this.racer, this.imei);

  @override
  _CheckedScreenState createState() =>
      _CheckedScreenState(event_l, race, racer, imei);
}

class _CheckedScreenState extends State<CheckedScreen> {
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic> _deviceData = <String, dynamic>{};
  Event_l event_l;
  Race race;

  Racer racer;
  DetailsTracking detailsTracking_p;
  String imei;
  List<TrackingInformation> lst_tracking = new List();
  bool is_finish_from_server = false;
  bool is_dispose = false;
  bool isLoadingSubmitted = false;
  bool your_race_is_on = false;
  double distanceInMeters = 0.0;
  List<Manager> manager_list = new List();

//  TextEditingController textEditingController_coordinates =
//      new TextEditingController();
//  TextEditingController textEditingController_distance =
//      new TextEditingController();
//  TextEditingController textEditingController_bearingBetween =
//  new TextEditingController();
  var _key_scaffold_n = new GlobalKey<ScaffoldState>();

  Coursepoints course_point;
  List<Coursepoints> list_coursepoints_send = new List();

  final List<PositionItem> _positionItems = <PositionItem>[];
  final List<Position> _positions_list = <Position>[];
  bool is_finish_server = false;
  DateTime date_start;
  StreamSubscription<Position> _positionStreamSubscription;

  _CheckedScreenState(this.event_l, this.race, this.racer, this.imei);

  Course course;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key_scaffold_n,
      appBar: PreferredSize(
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.only(top: 8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 0),
                      child: Container(
                        child: Text(
                          "${event_l.name}",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4),
                      child: Container(
                        child: Text(
                          "${race.name}\nStart",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
//                    Padding(
//                      padding: const EdgeInsets.only(top: 4),
//                      child: Container(
//                        child: Text(
//                          "Start",
//                          style: TextStyle(
//                              color: Colors.white,
//                              fontSize: 20,
//                              fontWeight: FontWeight.w400),
//                        ),
//                      ),
//                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 120),
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.blue,
                height: 2,
              ),
            ),
          ],
        ),
        preferredSize: Size.fromHeight(100),
      ),
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 15),
              child: Container(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                          color: col.col_from_argb,
                          width: MediaQuery.of(context).size.width,
//                height: 200,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "You are now ready to start. ",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: font_size_body,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 8.0, top: 0),
                                child: Text(
                                  "Your phone is connected and everything is  set up. Please bring your phone during the whole race.  ",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: font_size_body,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 8.0, top: 30),
                                child: Text(
                                  "You can now procced to the starting  -line.",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: font_size_body,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 8.0, top: 30),
                                child: Text(
                                  "${date_start != null ? 'Your start time is ${date_start.toString().substring(10, 16)}' : ''} ",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: font_size_body,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                              Padding(
                                padding:
                                    const EdgeInsets.only(left: 8.0, top: 30),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Good luck. ",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: font_size_body,
                                          fontWeight: FontWeight.w400),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 30),
                                      child: Center(
                                        child: your_race_is_on
                                            ? Text(
                                                "Your race is on",
                                                style: TextStyle(
                                                    color: Colors.green,
                                                    fontSize: font_size_body,
                                                    fontWeight:
                                                        FontWeight.w400),
                                              )
                                            : Text(
                                                "Your race has not started yet.",
                                                style: TextStyle(
                                                    color: Colors.green,
                                                    fontSize: font_size_body,
                                                    fontWeight:
                                                        FontWeight.w400),
                                              ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: Container(
//                color: Color(0xff121214),
//                width: MediaQuery.of(context).size.width,
//                height: 50,
//                child: Center(
//                  child: Text(
//                    "Your race is on",
//                    style: TextStyle(
//                        color: Colors.green,
//                        fontSize: 15,
//                        fontWeight: FontWeight.w100),
//                  ),
//                ),
//              ),
//            ),

//            Align(
//                alignment: Alignment.bottomRight,
//                child: Padding(
//                  padding: const EdgeInsets.only(top: 24.0, right: 16.0),
//                  child: InkWell(
//                    onTap: () {
////                      Navigator.push(
////                          context,
////                          new MaterialPageRoute(
////                              builder: (context) =>
////                                  new LiveResults(event_l, race)));
//                    },
//                    child: Text(
//                      "Proceed as an Organizer",
//                      style: TextStyle(
//                          color: Colors.blue,
//                          fontSize: 15,
//                          fontWeight: FontWeight.w300),
//                    ),
//                  ),
//                )),
                    ],
                  ),
                  decoration: BoxDecoration(
                      color: col.col_from_argb,
                      border: Border(
                          top: BorderSide(
                              color: Colors.blueGrey[50], width: 0.3)))),
            ),
            is_finish_from_server
                ? Align(
                    alignment: Alignment.bottomRight,
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height / 7,
                          right: 16.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Container(
                            child: InkWell(
                              onTap: () {
                                displayBottomSheet(context);
//                          Navigator.push(
//                              context,
//                              new MaterialPageRoute(
//                                  builder: (context) =>
//                                      new EndRace(event_l, race, racer)));
//                              Navigator.push(
//                                  context,
//                                  new MaterialPageRoute(
//                                      builder: (context) =>
//                                          new EndRace(event_l, race, racer)));
                              },
                              child: Text(
                                "End Your Race (DNF)",
                                style: TextStyle(
                                    color: Colors.blue,
                                    fontSize: font_size_body,
                                    fontWeight: FontWeight.w400),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(
                                top: 24.0, right: 16.0, bottom: 4),
                            child: Container(
                              child: InkWell(
                                onTap: () {
                                  if (manager_list.length == 0) {
                                    Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (_) =>
                                                    LoginManagerFromRacer(
                                                        event_l, race)))
                                        .then((value) => get_manager());
                                  } else if (manager_list.length > 0) {
                                    Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (_) =>
                                                    RacesScreenManager(
                                                        event_l)))
                                        .then((value) => get_manager());
                                  }
                                },
                                child: Text(
                                  "Proceed as an Organizer",
                                  style: TextStyle(
                                      color: Colors.blue,
                                      fontSize: font_size_body,
                                      fontWeight: FontWeight.w400),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ))
                : SpinKitThreeBounce(
                    size: 17,
                    color: Colors.blue,
                  ),
//            TextField(
//              readOnly: true,
//              onChanged: (value) {
//                setState(() {
//                  textEditingController_coordinates.text = value;
//                });
//              },
//              style: TextStyle(
//                  color: Colors.white,
//                  fontSize: font_size_body,
//                  fontWeight: FontWeight.w400),
//              textAlign: TextAlign.left,
//              controller: textEditingController_coordinates,
//              decoration: new InputDecoration(
//                contentPadding:
//                EdgeInsets.only(left: 8, right: 8.0, bottom: 8.0, top: 10),
//                //border: InputBorder.none,
//                hintStyle: TextStyle(
//                    color: Colors.white,
//                    fontSize: font_size_body,
//                    fontStyle: FontStyle.italic,
//                    fontWeight: FontWeight.w400),
//                hintText: '.',
//              ),
//            ),
//            Text(
//              'purpose:${course_point != null
//                  ? course_point.distance.round()
//                  : ''} ',
//              style: TextStyle(
//                  color: Colors.white,
//                  fontSize: font_size_body,
//                  fontWeight: FontWeight.w400),
//            ),
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: TextField(
//                readOnly: true,
//                onChanged: (value) {
////                setState(() {
////                  textEditingController_distance.text = value;
////                });
//                },
//                style: TextStyle(
//                    color: Colors.white,
//                    fontSize: font_size_body,
//                    fontWeight: FontWeight.w400),
//                textAlign: TextAlign.left,
//                controller: textEditingController_distance,
//                decoration: new InputDecoration(
//                  contentPadding:
//                  EdgeInsets.only(left: 0, right: 0.0, bottom: 0.0, top: 0),
//                  //border: InputBorder.none,
//                  hintStyle: TextStyle(
//                      color: Colors.white,
//                      fontSize: font_size_body,
//                      fontStyle: FontStyle.italic,
//                      fontWeight: FontWeight.w400),
//                  hintText: '.',
//                ),
//              ),
//            ),
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: TextField(
//                readOnly: true,
//                onChanged: (value) {
////                setState(() {
////                  textEditingController_distance.text = value;
////                });
//                },
//                style: TextStyle(
//                    color: Colors.white,
//                    fontSize: font_size_body,
//                    fontWeight: FontWeight.w400),
//                textAlign: TextAlign.left,
//                controller: textEditingController_bearingBetween,
//                decoration: new InputDecoration(
//                  contentPadding:
//                  EdgeInsets.only(left: 0, right: 0.0, bottom: 0.0, top: 0),
//                  //border: InputBorder.none,
//                  hintStyle: TextStyle(
//                      color: Colors.white,
//                      fontSize: font_size_body,
//                      fontStyle: FontStyle.italic,
//                      fontWeight: FontWeight.w400),
//                  hintText: '.',
//                ),
//              ),
//            ),
          ],
        ),
      ),
    );
  }

  void displayBottomSheet(BuildContext context) {
    bool is_loading_end = false;
    showModalBottomSheet(
        context: context,
        builder: (ctx) {
          return StatefulBuilder(
            builder: (c, StateSetter setStaten) {
              return SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Container(
                  decoration: BoxDecoration(
                      color: col.col_from_argb,
                      border: Border(
                          top: BorderSide(
                              color: Colors.blueGrey[50], width: 0.3))),
                  child: Column(
                    children: <Widget>[
                      Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 8.0, top: 8.0),
                              child: Text(
                                "Do you want to end your race?\n You will get registered as DNF\n(Did not finish).",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: font_size_body,
                                    fontWeight: FontWeight.w100),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 24.0),
                        child: InkWell(
                          onTap: () async {
                            setStaten(() {
                              isLoadingSubmitted = true;
                            });

                            if (!is_stop) {
                              await stop_end_race();
                            }
                          },
                          child: isLoadingSubmitted
                              ? SpinKitThreeBounce(
                                  color: Colors.red,
                                  size: 17,
                                )
                              : Container(
                                  decoration: BoxDecoration(
                                      color: Colors.red,
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10))),
                                  width: 250,
                                  height: 50,
                                  child: Center(
                                    child: Text(
                                      "END MY RACE",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: font_size_body,
                                          fontWeight: FontWeight.w300),
                                    ),
                                  ),
                                ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 24.0, bottom: 10),
                        child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                color: Colors.blue,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(10))),
                            width: 250,
                            height: 50,
                            child: Center(
                              child: Text(
                                "KEEP ON RACING",
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: font_size_body,
                                    fontWeight: FontWeight.w300),
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              );
            },
          );
        });
  }

  Future<void> get_manager() {
    DBProvider.db.Manager_list().then((value) {
      if (value.length > 0) {
        setState(() {
          manager_list = value;
        });
      } else {
        setState(() {
          manager_list = [];
        });
      }
    });
  }

  bool serviceEnabled=false,check_permission_gps_granted=false;
  get_service() async {

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      print("here>>>1");
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      move((){
        Geolocator.openLocationSettings();
      });
      delay_enable_location();

    } else {
      print("here>>>4");
      await check_and_request();
    }
    print("here>>>5");

  }
  Future<void> delay_enable_location(){
    Future.delayed(Duration(seconds: 2),() async {

      serviceEnabled = await Geolocator.isLocationServiceEnabled();

      if(!check_permission_gps_granted&&!is_dispose&&!call_check_and_request&&!serviceEnabled){
        delay_enable_location();
      }
      else if(serviceEnabled&&!call_check_and_request&&!is_dispose){
        check_and_request();

      }
    });
  }
  bool call_check_and_request=false;
  bool observe_permission=true;

  Future<void> delay_permission(){
    Future.delayed(Duration(seconds: 2),() async {
      LocationPermission permission;
      serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if(serviceEnabled&&!check_permission_gps_granted&&!is_dispose&&observe_permission){
        observe_permission=false;
        permission = await Geolocator.checkPermission();
        if (permission == LocationPermission.always ||
            permission == LocationPermission.whileInUse) {
          _toggleListening();
          setState(() {
            check_permission_gps_granted = true;
          });
        }
      }
      else {
        delay_permission();
      }

    });
  }

  Future <void>  move (Function function){
    Future.delayed(Duration(seconds: 2),() async {
   if(!is_dispose){
     await function.call();
   }
    });
  }
  check_and_request() async {
    call_check_and_request=true;
    LocationPermission permission;
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.always ||
        permission == LocationPermission.whileInUse) {
      _toggleListening();
      setState(() {
        check_permission_gps_granted = true;
      });
    } else if (permission == LocationPermission.denied &&
        Platform.isAndroid &&
        _deviceData.containsKey("version.sdkInt") &&
        int.tryParse(_deviceData['version.sdkInt'].toString()) < 30) {
      permission = await Geolocator.requestPermission();
    } else if (Platform.isAndroid &&
        _deviceData.containsKey("version.sdkInt") &&
        int.tryParse(_deviceData['version.sdkInt'].toString()) >= 30) {
//      _deviceData["version.sdkInt"]="30";
//              DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
//              AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      toast("Please grant GPS access permission");
      move((){
        Geolocator.openAppSettings();
      });

    }
    if (permission == LocationPermission.deniedForever) {
      // Permissions are denied forever, handle appropriately.
      return Future.error(
          'Location permissions are permanently denied, we cannot request permissions.');
    } else if (permission == LocationPermission.denied) {
      // Permissions are denied, next time you could try
      // requesting permissions again (this is also where
      // Android's shouldShowRequestPermissionRationale
      // returned true. According to Android guidelines
      // your App should show an explanatory UI now.
      return Future.error('Location permissions are denied');
    } else if (permission == LocationPermission.always ||
        permission == LocationPermission.whileInUse) {
      _toggleListening();
      setState(() {
        check_permission_gps_granted = true;
      });
    }
    delay_permission();
  }




  Future<void> stop_end_race() async {
    if (is_stop) return;
    setState(() {
      isLoadingSubmitted = true;
    });
    is_stop = true;
    is_dispose = true;
    print('callstop>>>>>>>');
    /**start*temp1**/
//    await register_device_msg('stop').then((value) {
//      if (value) {
//        Navigator.pushAndRemoveUntil(
//            context,
//            MaterialPageRoute(
//                builder: (_) => FinishedScreen(event_l, race, racer)),
//            (route) => false);
//      }
//    });
    /**end*temp1**/
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (_) => FinishedScreen(event_l, race, racer)),
        (route) => false);
  }

  Position position_five;
  bool is_stop = false;

  bool send_start = false;

  Future<void> get_location_in_five_seconds() async {
//    print('looooooood>');

    if (trackingInformation != null &&
        position_five != null &&
        !is_stop &&
        send_start &&
        trackingInformation.status == Status.guned.asString()) {
//      await register_device_msg(
//        'pos',
//      );
      //   await register_device_msg_to_my_server('pos');
    } else if (trackingInformation != null &&
            position_five != null &&
            !is_stop &&
            !send_start
//        &&
//        trackingInformation.status != Status.guned.asString()
        ) {
//      await register_device_msg(
//        'start',
//      );
      //  await register_device_msg_to_my_server('start');

    } else if (trackingInformation != null &&
        trackingInformation.status == Status.finished.asString()) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (_) => FinishedScreen(event_l, race, racer)),
          (route) => false);
    }
    Future.delayed(Duration(seconds: 5), () {
      if (!is_dispose && !is_stop) {
        get_location_in_five_seconds();
      }
    });
  }

  Future<bool> register_device_msg(
    String typep,
  ) async {
    try {
//      if (position_five == null) return;
      if (typep == 'start') {
        send_start = true;
      }
      print('typep>>> ${typep} position_five>>>>> ${position_five}   ');
      if (position_five == null) {
        Navigator.pushAndRemoveUntil(
            context,
            MaterialPageRoute(
                builder: (_) => FinishedScreen(event_l, race, racer)),
            (route) => false);
        return false;
      }

      var date_time = DateTime.now();
      var date_iso = date_time.toUtc().toIso8601String();
      var ind = date_iso.indexOf('.');

      var date_first = date_iso.substring(0, ind);

      var date_after = date_iso.substring(ind, ind + 4);
      var _bodys;

      _bodys = jsonEncode({
        "clientTime": date_time.millisecondsSinceEpoch,
        "type": typep,
        "latitude": position_five.latitude,
        "longitude": position_five.longitude,
        "time": '${date_first + date_after}Z',
        "altitude": position_five.altitude,
        'accuracy': position_five.accuracy
      });

      await http
          .put(
              'http://54.77.120.67:8080/rest/_App/trackings/${detailsTracking_p.reference_race}/devices/$imei/msg',
              headers: <String, String>{
                'Content-Type': 'application/json',
              },
              body: _bodys)
          .then((val) async {
        print('val.status>> ${val.statusCode}');
        if (val.statusCode != 200) {
          throw Exception(false);
        }
        if (val.body.isEmpty) {
          throw Exception(false);
        } else {
          Map<String, dynamic> data = jsonDecode(val.body);
          print('datagps>>> ${data}');
          //   Toast.show(data.toString(), context);
          List trackers = data['trackers'];
          //   await register_device_msg_to_my_server(typep);
          if (typep == 'pos' && !your_race_is_on) {
            setState(() {
              your_race_is_on = true;
            });
          }

          if (trackers.length > 0) {
            List<Tracker> lisTrackers = new List();
            lisTrackers = trackers.map((e) => Tracker.fromJSON(e)).toList();

            if (lisTrackers[0].status == 'finished') {
              await stop_end_race();
            }
          }

          if (position_five != null && _positions_list.length >= 2) {
            _positionItems.add(PositionItem(
                PositionItemType.position, position_five.toString()));
            distanceInMeters = (Geolocator.distanceBetween(
                    _positions_list[0].latitude,
                    _positions_list[0].longitude,
                    position_five.latitude,
                    position_five.longitude)
                .toDouble());
            toast('my distance is: ${distanceInMeters}');
          }

          return true;
        }
//            Navigator.pop(context);
      });
      return true;
    } on SocketException catch (ex) {
      if (typep == 'start') {
        send_start = false;
      }
      print('ex>>>>>${ex}');
      toast(ex.message);
      setState(() {
        isLoadingSubmitted = false;
      });
      if (typep == 'stop') {
        is_stop = false;
        is_dispose = false;
      }
      return false;
    } on Exception catch (ex) {
      print('ex>>>>>${ex}');
      setState(() {
        isLoadingSubmitted = false;
      });
      if (typep == 'start') {
        send_start = false;
      }
      if (typep == 'stop') {
        is_stop = false;
        is_dispose = false;
      }

      toast(ex.toString());
      return false;
    }
  }

  Future<void> get_information_from_server() async {
    try {
      await DBProvider.db
          .get_information_race(race, event_l)
          .then((value) async {
        await refresh_informa();

//        await _determinePosition();
//        _toggleListening();
        get_tracking_per_twenty_seconds();
        get_location_in_five_seconds();
      });
    } catch (ex) {
      setState(() {
        is_finish_from_server = true;
      });
    }
  }

  TrackingInformation trackingInformation;
  bool first_invoked = true;

  void get_date_my_start(DetailsTracking detailsTracking) {
//    print('get_date_my_start>> ${detailsTracking.lst_timings}');
    detailsTracking.lst_timings
        .where((element) =>
            element.bib == racer.bib &&
            element.courspoint == course.coursepoints.first.reference)
        .forEach((element) {
      if (date_start == null && !is_dispose) {
        setState(() {
          date_start = DateTime.fromMillisecondsSinceEpoch(element.duration)
              .subtract(Duration(hours: 1));
        });
      }
    });
    print('get_date_my_start_finish');
  }

  Future<void> get_tracking_per_twenty_seconds() async {
    await DBProvider.db.get_trackings_race(race, event_l, is_update: true);
    await DBProvider.db.TrackingInformation_list().then((value) {
      lst_tracking = value;
    });

    TrackingInformation information = lst_tracking.firstWhere(
        (element) =>
            element.reference == race.reference &&
            element.ref_event == event_l.reference,
        orElse: () => null);

    await DBProvider.db.DetailsTracking_limit1(race, event_l).then((value) {
      get_date_my_start(value);
      value.lst_trackers
          .where((element) =>
              element.bib == racer.bib && element.status == "finished")
          .forEach((element) async {
        await stop_end_race();
      });
    });

//    if (date_start == null && detailsTracking.lst_timings.length > 0) {
//      get_date_my_start(detailsTracking);
//    }
    if (information != null) {
      trackingInformation = information;
      if (trackingInformation.status == Status.guned.asString() &&
          !your_race_is_on) {
        setState(() {
          your_race_is_on = true;
        });
      }
    }

    if (trackingInformation == null ||
        trackingInformation.status == Status.waiting_for_gun.asString() ||
        trackingInformation.status == Status.open.asString() ||
        trackingInformation.status == Status.upcoming.asString() ||
        date_start == null) {
      Future.delayed(Duration(seconds: 20), () {
        if (!is_dispose) {
          get_tracking_per_twenty_seconds();
        }
      });
    }
  }

  Future<void> refresh_informa() async {
    await DBProvider.db.Race_list_refresh(race, event_l).then((value) {
      race = value[0];
    });
    await DBProvider.db.DetailsTracking_limit1(race, event_l).then((value) {
      detailsTracking_p = value;
    });
    await get_course();
    setState(() {
      is_finish_from_server = true;
    });
    get_manager();
  }

  get_course() async {
    await DBProvider.db.Course_list_from_id(detailsTracking_p.id).then((value) {
      course = value[0];

      course.coursepoints.sort((a, b) => a.compareTo(b));
      print('course>>> ${course.coursepoints}');
    });
  }

  Future<void> initPlatformState() async {
    Map<String, dynamic> deviceData = <String, dynamic>{};

    try {
      if (Platform.isAndroid) {
        deviceData = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
      } else if (Platform.isIOS) {
        deviceData = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {
      deviceData = <String, dynamic>{
        'Error:': 'Failed to get platform version.'
      };
    }

    if (!mounted) return;

    _deviceData = deviceData;
    get_service();
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'androidId': build.androidId,
      'systemFeatures': build.systemFeatures,
    };
  }

  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'identifierForVendor': data.identifierForVendor,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }

  @override
  void initState() {
    //  wake(true);
    initPlatformState();
    get_information_from_server();

//    che();

    super.initState();
  }

//Future<void>wake(bool val) async {
//// await Wakelock.toggle(on: val);
//}

  Position first_compare;

  bool _isListening() => !(_positionStreamSubscription == null ||
      _positionStreamSubscription.isPaused);

  void _toggleListening() {
    if (_positionStreamSubscription == null) {
      final positionStream = Geolocator.getPositionStream(
          desiredAccuracy: LocationAccuracy.high,
          forceAndroidLocationManager: true);
      _positionStreamSubscription = positionStream.handleError((error) async {
        print('erroe>>> ${error}');
        _positionStreamSubscription.cancel();
        _positionStreamSubscription = null;
        await stop_end_race();
      }).listen(
        (position) {
          _positions_list.add(position);

          position_five = position;
//          position_five.latitude=52.520008;
//          position_five.longitude=13.404954;
          // position_five.accuracy=4.7;
        },
        onError: (e) async {
          print('erroe2>>> ${e}');
          await stop_end_race();
        },
      );
      _positionStreamSubscription.pause();
    }

    setState(() {
      if (_positionStreamSubscription.isPaused) {
        _positionStreamSubscription.resume();
      } else {
        _positionStreamSubscription.pause();
      }
    });
  }

  @override
  void dispose() {
    if (_positionStreamSubscription != null) {
      _positionStreamSubscription.cancel();
      _positionStreamSubscription = null;
    }
    is_dispose = true;
//    get_location('stop');
//    super.dispose();
//    WidgetsBinding.instance.removeObserver(this);

    super.dispose();
  }

  void toast(String msg) {
    if (!is_dispose)
      Toast.show("${msg}", context,
          backgroundColor: Colors.blueGrey, duration: Toast.LENGTH_LONG);
  }
}
