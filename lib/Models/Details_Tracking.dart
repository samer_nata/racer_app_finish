import 'dart:convert';

import 'Event.dart';
import 'Race.dart';
import 'Timings.dart';
import 'Trackers.dart';
import 'TrackingWithInformation.dart';

class DetailsTracking {
  int id, id_p;
  Map<String, dynamic> timings, trackers;
  List<Timing> lst_timings = new List();
  List<Tracker> lst_trackers = new List();
  String reference_race, ref_event;

//Time startTimeMs;
  DetailsTracking(
      {this.id,
      this.trackers,
      this.timings,
      this.lst_timings,
      this.lst_trackers,
      this.reference_race,
      this.id_p,
      this.ref_event});

  factory DetailsTracking.fromJSONSERVER(Map<String, dynamic> json,
      TrackingInformation trackingInformation, Race race, Event_l event_l) {
//    List list = json['timings'];
//    jsonDecode(json);
    if (json != null) {
      return DetailsTracking(
          id: trackingInformation.id,
          id_p: trackingInformation.id,
          timings: {'timings': json['timings']},
          trackers: {'trackers': json['trackers']},
          ref_event: event_l.reference,
          reference_race: race.reference);
    }
    return null;
  }
  void get_trackers(){

    List ls_track=new List();
    List<Tracker>list_trackers=new List();
    if(trackers.containsKey("trackers")){
      ls_track=  trackers['trackers'];

      list_trackers=  ls_track.map((e) => Tracker.fromJSON(e)).toList(growable: true);
    }
    lst_trackers=list_trackers;

  }
  void get_timigs(){
    List ls_tim=new List();
    List<Timing>list_timings=new List();
    if(timings.containsKey("timings")){
      ls_tim=  timings['timings'];

      list_timings=  ls_tim.map((e) => Timing.fromJSON(e)).toList(growable: true);
    }
    lst_timings=list_timings;

  }
  factory DetailsTracking.fromJsonServerPublic(Map<String, dynamic> json) {

    List ls_tim=new List();
    List<Timing>list_timings=new List();
    if(json.containsKey("timings")){
      ls_tim=  json['timings'];

      list_timings=  ls_tim.map((e) => Timing.fromJSON(e)).toList(growable: true);
    }
    List ls_track=new List();
    List<Tracker>list_trackers=new List();
    if(json.containsKey("trackers")){
      ls_track=  json['trackers'];

      list_trackers=  ls_track.map((e) => Tracker.fromJSON(e)).toList(growable: true);
    }

    if (json != null) {
//      print(json != null);
    try{

      return DetailsTracking(

          timings:json.containsKey("timings") ? {"timings":json['timings']}:null,
          trackers: json.containsKey("trackers") ? {"trackers":json["trackers"]}:null ,
          lst_timings: list_timings
          ,lst_trackers: list_trackers


      );
    }
    catch(ex){print(ex);}
    }
    return null;
  }

  factory DetailsTracking.fromJSON(Map<String, dynamic> json) {
    Map<String, dynamic> jr_tim = jsonDecode(json['timings']);
    List jr_ti = jr_tim['timings'];
    Map<String, dynamic> jr_trackers = jsonDecode(json['trackers']);
    List jr_tr = jr_trackers['trackers'];
    print("jr_tr>>>${jr_tr}");
    return DetailsTracking(
        id: json['id'],
        timings: jsonDecode(json['timings']),
        trackers: jsonDecode(json['trackers']),
        reference_race: json['reference_race'],
        id_p: json['id_p'],
        ref_event: json['ref_event'],
        lst_timings: List.generate(jr_ti.length, (i) {
          return Timing.fromJSON(jr_ti[i]);
        }, growable: true),
        lst_trackers: List.generate(jr_tr.length, (i) {
          return Tracker.fromJSON(jr_tr[i]);
        }, growable: true));
  }

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "timings": jsonEncode(timings),
      "trackers": jsonEncode(trackers),
      'id_p': id_p,
      "reference_race": reference_race,
      "ref_event": ref_event
    };
  }

  @override
  String toString() {
    return 'DetailsTracking{timings: $timings, trackers: $trackers, lst_timings: $lst_timings, lst_trackers: $lst_trackers}';
  }
}
