class Tracker {
  int id, bib, duration;
  DateTime regTimeMs, checkinTimeMs, gunTimeMs;
  String status, type, participantRef,tracking;
  List<String> devices;
  double distance;
  Map<String, dynamic> position = Map();
  Map<String, dynamic> result = Map();

//Time startTimeMs;
  Tracker(
      {this.participantRef,
      this.bib,
      this.id,
      this.status,
      this.type,
      this.checkinTimeMs,
      this.devices,
      this.regTimeMs,
      this.position,
      this.duration,
      this.gunTimeMs,
      this.distance,
      this.result,this.tracking});

  factory Tracker.fromJSON(Map<String, dynamic> json) {
//    jsonDecode(json);

    if (json != null) {
      return Tracker(
          bib: json["bib"],
          id: json['id'],
          participantRef: json['participantRef'],
          type: json['type'],
          checkinTimeMs: json.containsKey('checkinTimeMs')
              ? DateTime.fromMillisecondsSinceEpoch(json['checkinTimeMs']).subtract(Duration(hours: 1))
              : null,
          devices: List.generate(
              json["devices"].length, (index) => json["devices"][index]),
          regTimeMs: json.containsKey('regTimeMs')
              ? DateTime.fromMillisecondsSinceEpoch(json['regTimeMs']).subtract(Duration(hours: 1))
              : null,
          status: json['status'],
          position: json.containsKey('position') ? json['position'] : null,
          distance: json.containsKey('distance')
              ? double.parse(json['distance'].toString())
              : null,
          duration: json.containsKey('duration') ? json['duration'] : null,
          gunTimeMs: json.containsKey('gunTimeMs')
              ? DateTime.fromMillisecondsSinceEpoch(json['gunTimeMs']).subtract(Duration(hours: 1))
              : null,
          result: json.containsKey('result') ? json['result'] : null,
          tracking:json.containsKey("tracking")?json["tracking"]:null);
    }
    return null;
  }


  @override
  String toString() {
    return 'Tracker{bib: $bib, duration: $duration, regTimeMs: $regTimeMs, checkinTimeMs: $checkinTimeMs, status: $status, participantRef: $participantRef, tracking: $tracking, distance: $distance, result: $result}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Tracker &&
          runtimeType == other.runtimeType &&
          id == other.id &&
          bib == other.bib &&
          duration == other.duration &&
          regTimeMs == other.regTimeMs &&
          checkinTimeMs == other.checkinTimeMs &&
          gunTimeMs == other.gunTimeMs &&
          status == other.status &&
          type == other.type &&
          participantRef == other.participantRef &&
          devices == other.devices &&
          distance == other.distance &&
          position == other.position &&
          result == other.result;

  @override
  // TODO: implement hashCode
  int get hashCode => super.hashCode;

}
