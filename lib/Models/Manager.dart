class Manager {
  int id;
  String email;
  String password;



  Manager({this.id, this.email, this.password});

  factory Manager.fromJSON(json) {
    return Manager(
        id: json["id"],
        email: json["email"],
        password: json["password"],
     );
  }

  factory Manager.fromJSONserver(json) {
    return Manager(
        id: int.parse(json["id"]),
        email: json["name"],
        password: json["password_p"],
       );
  }

  Map<String, dynamic> toMap() => {
    "id": id,
    "email": email,
    "password": password,


  };

  @override
  String toString() {
    return 'UserRegister{id: $id, email: $email, password: $password, }';
  }
}
