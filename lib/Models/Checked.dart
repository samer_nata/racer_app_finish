import 'dart:collection';
import 'dart:convert';

import 'Coursepoints.dart';

class Checked {
  int bib;

  String reference, name, ref_event,name_event;
  bool state = false;
  bool view = false;
  bool is_open = false;

//Time startTimeMs;
  Checked(
      {this.bib,
      this.reference,
      this.state,
      this.view,
      this.is_open,
      this.name,this.name_event,
      this.ref_event});

  factory Checked.fromJSON(Map<String, dynamic> json) {
    return Checked(
        is_open: json['is_open'] == 0 ? false : true,
        bib: json['bib'],
        reference: json['reference'],
        state: json['state'] == 0 ? false : true,
        view: json['view'] == 0 ? false : true,
        name: json['name'],
        ref_event: json['ref_event'],name_event:json['name_event'] );
  }

  Map<String, dynamic> toMap() {
    return {
      'bib': bib,
      'reference': reference,
      'state': state == true ? 1 : 0,
      'view': view == true ? 1 : 0,
      'is_open': is_open == true ? 1 : 0,
      'name': name,
      'ref_event': ref_event,'name_event':name_event
    };
  }

  @override
  String toString() {
    return 'Checked{bib: $bib, reference: $reference, name: $name, ref_event: $ref_event, name_event: $name_event, state: $state, view: $view, is_open: $is_open}';
  }
}
