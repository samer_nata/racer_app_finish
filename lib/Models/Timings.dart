import 'dart:convert';
import 'package:intl/intl.dart' as nl;

class Timing {
  String courspoint, category, position, positionCat, source, participantRef;
  int bib, duration;
  String time, tracking;

//  int timer = 0;
  int temp_time;

//  bool is_start = false;

//Time startTimeMs;
  Timing(
      {this.duration,
      this.bib,
      this.category,
      this.position,
      this.courspoint,
      this.participantRef,
      this.positionCat,
      this.source,
      this.time,
//      this.timer,
      this.temp_time,
      this.tracking
//      this.is_start
      });

  factory Timing.fromJSON(Map<String, dynamic> json) {
//    print("HIIIII ${json["time"]}");
//    jsonDecode(json);
    if (json != null) {
//      print(new DateFormat.yMMMd().format(new DateTime.now()));
//   final DateFormat alfrescoDateFormat = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy")
//

      return Timing(
        bib: json["bib"],
        time: json["time"],
        duration: json["duration"],
        courspoint: json['courspoint'],
        category: json['category'],
        source: json['source'],
        participantRef: json['participantRef'],
        position: json['position'],
        positionCat: json['positionCat'],
          tracking:json.containsKey("tracking")?json["tracking"]:null
      );
    }
    return null;
  }


  @override
  String toString() {
    return 'Timing{courspoint: $courspoint, position: $position, source: $source, participantRef: $participantRef, bib: $bib, duration: $duration, tracking: $tracking}';
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Timing &&
          runtimeType == other.runtimeType &&
          courspoint == other.courspoint &&
          category == other.category &&
          position == other.position &&
          positionCat == other.positionCat &&
          source == other.source &&
          participantRef == other.participantRef &&
          bib == other.bib &&
          duration == other.duration;

  @override
  // TODO: implement hashCode
  int get hashCode => super.hashCode;
}
