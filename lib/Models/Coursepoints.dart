import 'dart:collection';
import 'dart:convert';



class Coursepoints implements Comparable {
  String label, type, reference;
  double distance;
  int lapIndex;
  bool select=false;
  Coursepoints coursepoints;

//Time startTimeMs;
  Coursepoints(
      {this.distance, this.reference, this.label, this.lapIndex, this.type});

  factory Coursepoints.fromJSON(json) {
    return Coursepoints(
        distance: double.parse(json['distance'].toString()),
        label: json['label'],
        lapIndex: json['lapIndex'],
        reference: json['reference'],
        type: json['type']);
  }

  static List<Coursepoints> get_coursetpoint(List ls) {
    List<Coursepoints> list = new List();
//    double distance=0.0;

    ls.forEach((element) {
      var course_p= Coursepoints.fromJSON(element);
//      course_p.distance=distance;
      list.add(course_p);
//      distance=distance+50.0;
    });

    return list;
  }

  Map<String, dynamic> toMap() {
    return {
      "distance": distance,
      "label": label,
      "lapIndex": lapIndex,
      "reference": reference,
      "type": type,
    };
  }


  @override
  String toString() {
    return 'Coursepoints{label: $label, type: $type, reference: $reference, distance: $distance, lapIndex: $lapIndex, select: $select, coursepoints: $coursepoints}';
  }

  @override
    int compareTo( another) {
    // TODO: implement compareTo
    if(lapIndex>another.lapIndex){
      return 1;
    }
    else if(lapIndex==another.lapIndex){
      return 0;
    }
    return -1;

  }
}
