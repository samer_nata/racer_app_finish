import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:racer_app_finish/Enum.dart';
import 'package:racer_app_finish/Models/Racer.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

import 'Models/Checked.dart';
import 'Models/Course.dart';
import 'Models/Details_Tracking.dart';
import 'Models/Event.dart';

import 'Models/Manager.dart';
import 'Models/Note.dart';
import 'Models/Race.dart';

import 'package:http/http.dart' as http;

import 'Models/TrackingWithInformation.dart';

class DBProvider {
  DBProvider._();

  static final DBProvider db = DBProvider._();
  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    _database = await initDB();
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "racer_mief_f.db");
    return await openDatabase(path, version: 2,
        onCreate: (Database db, int version) async {
      await db.execute(
          "CREATE TABLE Manager (id int PRIMARY KEY ,email TEXT ,password TEXT);");

      await db.execute(
          "CREATE TABLE Racer (id  int ,bib INTEGER PRIMARY KEY,club TEXT ,surname TEXT,firstname TEXT,ref_event TEXT,ref_race TEXT,Unique(ref_event,ref_race) );");

      await db.execute(
          "CREATE TABLE Events (startTimeMs INTEGER ,defaultRace TEXT,endTimeMs INTEGER ,location TEXT,name TEXT UNIQUE ,reference TEXT);");
      await db.execute(
          "CREATE TABLE Races (id int PRIMARY KEY,startTimeMs INTEGER ,name TEXT,jso JSON,racers JSON,courseId int,reference TEXT,is_start BOOLEAN,ref_event TEXT,FOREIGN KEY (ref_event) REFERENCES Events(reference)  on DELETE CASCADE on update CASCADE);");
      await db.execute(
          "CREATE TABLE Checked (bib int,reference TEXT ,  state Boolean ,view Boolean, is_open Boolean,name TEXT , ref_event TEXT,name_event TEXT,UNIQUE(bib,reference,ref_event));");

      await db.execute(
          "CREATE TABLE Tracking (id int PRIMARY KEY,duration INTEGER ,startTimeMs INTEGER,courseId int,status TEXT,openingTimeMs INTEGER,gunTimeMs INTEGER,race TEXT,reference TEXT ,ref_event TEXT,UNIQUE(reference,ref_event),FOREIGN KEY (reference) REFERENCES Races(reference)  on DELETE CASCADE on update CASCADE ,FOREIGN KEY (ref_event) REFERENCES Events(reference)  on DELETE CASCADE on update CASCADE );");
      await db.execute(
          "CREATE TABLE Details_Tracking (timings JSON, trackers JSON,reference TEXT ,ref_event TEXT,reference_race TEXT,id_p int PRIMARY KEY,id int ,FOREIGN KEY (id) REFERENCES Tracking(id)  on DELETE CASCADE on update CASCADE );");
//      await db.execute(
//          "CREATE TABLE Tracking_for_racer (id_course int  not null,name_race TEXT,bib_racer int ,id_tracking int ,jso_trackers JSON,jso_timings JSON,FOREIGN KEY (id_course) REFERENCES Course(id)  on DELETE CASCADE on update CASCADE,FOREIGN KEY (name_race) REFERENCES Races(name)  on DELETE CASCADE on update CASCADE,FOREIGN KEY (bib_racer) REFERENCES Racer(bib)  on DELETE CASCADE on update CASCADE ,FOREIGN KEY (id_tracking) REFERENCES Tracking(bib)  on DELETE CASCADE on update CASCADE);");

      await db.execute(
          "CREATE TABLE Course (id int  PRIMARY KEY,distance REAL,name TEXT,startPos JSON,endPos JSON,js_coursepoints JSON);");

      await db.execute("PRAGMA foreign_keys = ON;");
    });
  }

  open() async {
    if (_database != null) {
      await openDatabase("racer_mief_f.db");
    }
  }

  Future<void> close() async {
//    await refresh_state_app();
    await _database.close();
  }

/**********************************************************************************/

  /*****************************END NOTE******************************************************************/
  /************************************Event*******************************************/

  Future<void> insert_event(Event_l event) async {
    await _database.insert(
      'Events',
      event.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  bool is_finish_load = false;

  Future<void> insertallEvent(List listrace) async {
    is_finish_load = false;
    final db1 = await database;
    listrace.forEach((obj) async {
      Event_l event = new Event_l.fromJSONServer(obj);
      print('event>>>>>>>>>>> ${event.toString()}');
      try {
        db1
            .insert('Events', event.toMap(),
                conflictAlgorithm: ConflictAlgorithm.replace)
            .then((value) async {
          if (value != null && obj['races'] != null) {
            print("pbj>>>>> ${obj}");
            List temp2 = obj['races'];
            await insertallRace(temp2, event);
          }
        });
      } catch (ex) {
        print("ex<<<<<<<<  ${ex}");
      }
      print("obj>>>>>>>>>> ${obj}");
      if (listrace.indexOf(obj) == listrace.length - 1) {
        List<Race> list_races = await Race_list_all_e();
        List<Racer> list_racer = await Racer_list();
        list_racer.forEach((el_racer) async {
          if (list_races.firstWhere(
                  (element) =>
                      element.ref_event == el_racer.ref_event &&
                      element.reference == el_racer.ref_race,
                  orElse: () => null) ==
              null) {
            await delete_racer(el_racer);
          }
        });
      }
    });
    return;
  }

  Future<List<Event_l>> Events_list() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('Events');
    print("EVENT>>>>>>> ${maps}");
    return List.generate(maps.length, (i) {
      return Event_l.fromJSON(maps[i]);
    });
  }

  Future<void> deleteAllEvent() async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");

    await db.delete("Events");
    return;
  }

  /******************************End Event******************************************************/
  /*************************************Ra_RAC**********************************************/

  /***********************************End_ra_race*********************************************************/
  /**********************Race*****************************************************************************************/

  Future<void> insert_race(Race race) async {
    await _database.insert(
      'Races',
      race.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> update_race(Race race) async {
    await _database.update(
      'Races',
      race.toMap(),
      where: "id = ? ",
      whereArgs: [race.id],
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
//    print(race.toString());
  }

//  Future<void> update_race(Race race, int id) async {

//    await _database.update(
//      'Races',
//      race.toMap(),
//      where: "id = ?",
//      whereArgs: [id],
//      conflictAlgorithm: ConflictAlgorithm.replace,
//    );
//  }

  Future<void> get_information_race(Race race, Event_l event_l,
      {bool is_last = false}) async {
    await http
        .get(
            'http://54.77.120.67:8080/rest/_App/races/${race.reference}/startlist/participants',  headers: <String, String>{
      'Content-Type': 'application/json;charset=UTF-8'
    })
        .then((data) async {
//      races = [];
      if (data.statusCode != 200) {
        throw Exception('Failed to load data');
      }
      Map<String, dynamic> dataServer = jsonDecode(data.body);
//      LisSt racers = dataServer['race']['startlist'];
//      print("dataServer>>>>> ${dataServer}");
      race.racers = dataServer['race'];
      await insert_race(race).then((value) async {
        await get_course_race(race);
        await get_trackings_race(race, event_l,
            is_update: false, is_last: is_last);
        print("dataServerfff>>>>> ${dataServer}");
      });

//      if (dataServer.length > 0) {
//        DBProvider.db.insertallRacer(racers, race);
//      } else if (dataServer.length == 0) {
//        DBProvider.db.delete_from_race(race);
//      }
    });
    return;
  }

  Future<void> get_course_race(Race race) async {
    await http
        .get('http://54.77.120.67:8080/rest/_App/courses/${race.courseId}',  headers: <String, String>{
      'Content-Type': 'application/json;charset=UTF-8'
    })
        .then((data) async {
//      races = [];
      if (data.statusCode != 200) {
        throw Exception('Failed to load data');
      }
      Map<String, dynamic> dataServer = jsonDecode(data.body);

      if (dataServer != null && dataServer.containsKey('course')) {
        await DBProvider.db.insertallcourses(dataServer['course'], race);
      } else if (dataServer.length == 0) {
        await DBProvider.db.delete_from_course(race);
      }
    });
  }

  Future<void> get_trackings_race(Race race, Event_l event_l,
      {bool is_update, bool is_last = false}) async {
    try {
      await http
          .get('http://54.77.120.67:8080/rest/_App/trackings/${race.reference}',  headers: <String, String>{
        'Content-Type': 'application/json;charset=UTF-8'
      })
          .catchError((err) {
        print('err>>>>>> ${err}');
      }).then((data) async {
//      races = [];
        if (data.statusCode != 200) {
          throw Exception('Failed to load data');
        }
        Map<String, dynamic> dataServer = jsonDecode(data.body);

        if (is_update) {
          TrackingInformation trackingInformation =
              new TrackingInformation.fromJSONSERVER(
                  dataServer['tracking'], race, event_l);

          await update_tracking(trackingInformation);

          DetailsTracking detailsTracking = new DetailsTracking.fromJSONSERVER(
              dataServer['tracking'], trackingInformation, race, event_l);
          await DBProvider.db.insert_detailstracking(detailsTracking);

          return;
        }
        if (dataServer.length > 0) {
          TrackingInformation trackingInformation =
              new TrackingInformation.fromJSONSERVER(
                  dataServer['tracking'], race, event_l);
          DetailsTracking detailsTracking = new DetailsTracking.fromJSONSERVER(
              dataServer['tracking'], trackingInformation, race, event_l);
//          await DBProvider.db.delete_DetailsTracking(trackingInformation);

          await DBProvider.db
              .insert_tracking(trackingInformation, is_last: is_last);

          await DBProvider.db.insert_detailstracking(detailsTracking);
        } else if (dataServer.length == 0) {
          await DBProvider.db.delete_TrackingInformation(race);
        }
        return;
      });
    } on TimeoutException catch (e) {
//      print('Timeout Error: $e');
    } on SocketException catch (e) {
//      print('Socket Error: $e');
    } on Error catch (e) {
//      print('General Error: $e');
    }

    print('endtracking>>>>>${race.name}>>>>>');
  }

  Future<List> get_trackings_reference(String reference,
      {bool is_update, bool is_last = false}) async {
    try {
    var data=  await http
          .get('http://54.77.120.67:8080/rest/_App/trackings/$reference',  headers: <String, String>{
      'Content-Type': 'application/json;charset=UTF-8'
    })
          .catchError((err) {
        print('err>>>>>> ${err}');
      });
    Event_l event_l=new Event_l();
    Race race = new Race();
    if (data.statusCode != 200) {
      throw Exception('Failed to load data');
    }
    Map<String, dynamic> dataServer = jsonDecode(data.body);
    print("dataServer>>> ${dataServer}");

    await Race_list_all_e().then((value) {
      race = value.firstWhere((element) => element.reference == reference,orElse: ()=>null);
    });
    print("race>>> ${race.ref_event}");

    await   Events_list().then((value) {
      event_l=value.firstWhere((element) =>element.reference==race.ref_event ,orElse: ()=>null);
    });
    print("event>>> ${event_l.toString()}");
     if(race==null||event_l==null){
       return null;
     }
      DetailsTracking detailsTracking=new DetailsTracking();
      if (is_update) {
        TrackingInformation trackingInformation =
        new TrackingInformation.fromJSONSERVER(
            dataServer['tracking'], race, event_l);

        await update_tracking(trackingInformation);

        detailsTracking = new DetailsTracking.fromJSONSERVER(
            dataServer['tracking'], trackingInformation, race, event_l);
        await DBProvider.db.insert_detailstracking(detailsTracking);
        print("detailsTracking>>> ${detailsTracking}");
        List lst=new List();
        lst.add(event_l);
        lst.add(race);
        lst.add(detailsTracking);
        return lst;


      }
      return null;
//        if (dataServer.length > 0) {
//          TrackingInformation trackingInformation =
//              new TrackingInformation.fromJSONSERVER(
//                  dataServer['tracking'], race, event_l);
//          DetailsTracking detailsTracking = new DetailsTracking.fromJSONSERVER(
//              dataServer['tracking'], trackingInformation, race, event_l);
////          await DBProvider.db.delete_DetailsTracking(trackingInformation);
//
//          await DBProvider.db
//              .insert_tracking(trackingInformation, is_last: is_last);
//
//          await DBProvider.db.insert_detailstracking(detailsTracking);
//        } else if (dataServer.length == 0) {
//          await DBProvider.db.delete_TrackingInformation(race);
//        }
//        return;

    } on TimeoutException catch (e) {
//      print('Timeout Error: $e');
    } on SocketException catch (e) {
//      print('Socket Error: $e');
    } on Error catch (e) {
//      print('General Error: $e');
    }


  }

  Future<DetailsTracking> get_trackings_race_automatic(
      Race race, Event_l event_l) async {
    try {
      var re = await http.get(
          'http://54.77.120.67:8080/rest/_App/trackings/${race.reference}',  headers: <String, String>{
        'Content-Type': 'application/json;charset=UTF-8'
      });

      if (re.statusCode != 200) {
        throw Exception('Failed to load data');
      }
      Map<String, dynamic> dataServer = jsonDecode(re.body);

      if (dataServer.length > 0) {
        print('dataServer>>>>> ${dataServer}');
        TrackingInformation trackingInformation =
            new TrackingInformation.fromJSONSERVER(
                dataServer['tracking'], race, event_l);
        DetailsTracking detailsTracking = new DetailsTracking.fromJSONSERVER(
            dataServer['tracking'], trackingInformation, race, event_l);
        Map<String, dynamic> map = detailsTracking.toMap();
//          await DBProvider.db.delete_DetailsTracking(trackingInformation);
        await DBProvider.db.insert_tracking(trackingInformation);
        print('here>>>>>${detailsTracking.lst_timings}');
        return new DetailsTracking.fromJSON(map);
      }
      return null;
    } on TimeoutException catch (e) {
//      print('Timeout Error: $e');
    } on SocketException catch (e) {
//      print('Socket Error: $e');
    } on Error catch (e) {
//      print('General Error: $e');
    }
  }

  Future<DetailsTracking> get_trackings_races_automatic(
      Event_l event_l, Map<String, dynamic> trackers_json) async {
    List<Race> races = await Race_list_from_event(event_l);
    String references = races
        .map((e) => e.reference)
        .toList()
        .toString()
        .replaceAll(" ", "")
        .replaceAll("[", "")
        .replaceAll("]", "");

    try {
      var re = await http.get(
          'http://54.77.120.67:8080/rest/_App/trackings/$references/timings',  headers: <String, String>{
        'Content-Type': 'application/json;charset=UTF-8'
      });

      if (re.statusCode != 200) {
        throw Exception('Failed to load data');
      }
      Map<String, dynamic> dataServer = jsonDecode(re.body);

      if (dataServer.length > 0) {
        print("here>> ${dataServer}");
        dataServer.addAll(trackers_json);
        DetailsTracking detailsTracking =
            new DetailsTracking.fromJsonServerPublic(dataServer);
        print("hereprint>>> 111");
        print("hereprint>>> ${detailsTracking}");
        return detailsTracking;
      }
      return null;
    } on TimeoutException catch (e) {
//      print('Timeout Error: $e');
    } on SocketException catch (e) {
//      print('Socket Error: $e');
    } on Error catch (e) {
//      print('General Error: $e');
    }
  }

  Future<Map<String, dynamic>> get_trackers_races(Event_l event_l) async {
    List<Race> races = await Race_list_from_event(event_l);
    String references = races
        .map((e) => e.reference)
        .toList()
        .toString()
        .replaceAll(" ", "")
        .replaceAll("[", "")
        .replaceAll("]", "");

    try {

      var re = await http.get(
          'http://54.77.120.67:8080/rest/_App/trackings/$references/trackers',  headers: <String, String>{
        'Content-Type': 'application/json;charset=UTF-8'
      });

      if (re.statusCode != 200) {
        throw Exception('Failed to load data');
      }
      Map<String, dynamic> dataServer = jsonDecode(re.body);

      if (dataServer.length > 0) {
        return dataServer;
      }
      return null;
    } on TimeoutException catch (e) {
      print('Timeout Error:>>> $e');
    } on SocketException catch (e) {
      print('Socket Error:>>> $e');
    } on Error catch (e) {
      print('General Error:>>> $e');
    }
  }

  Future<void> insertallRace(List listrace, Event_l event_l) async {
//    final db1 = await database;

    listrace.forEach((obj) async {
      Race race = new Race.fromJSONSERVER(obj);
      race.ref_event = event_l.reference;
      print("race>>>>> ${race.toString()}");
      print('listrace>>>>>>>>>>>>> ${listrace}');
      try {
//        await db1.insert('Races', race.toMap(),
//            conflictAlgorithm: ConflictAlgorithm.replace);
        if (listrace.indexOf(obj) == listrace.length - 1) {
          await get_information_race(race, event_l, is_last: true);
        } else {
          await get_information_race(race, event_l);
        }
      } catch (ex) {
//        is_finish_load = true;
        print("ex>>>>>>>>> ${ex}");
      }
    });

    return;
  }

  Future<List<Race>> Race_list_state_true() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query(
      'Races',
    );
    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }

  Future<List<Race>> Race_list_false(DateTime dateTime) async {
//    safsaf
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('Races',
        where: " startTimeMs  > ? ",
        whereArgs: [dateTime.millisecondsSinceEpoch]);
    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }

  Future<List<Race>> Race_list_from_event(Event_l event_l) async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db
        .query('Races', where: "ref_event = ?", whereArgs: [event_l.reference]);
    print('maps>>>>>>>>>>>>>>>> ${maps}');
    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }

  Future<List<Race>> Race_list_all_e() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('Races');

    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }

  Future<List<Race>> Race_list_refresh(Race race, Event_l event_l) async {
    final db = await database;

    List<Map<String, dynamic>> maps = await db.rawQuery(
        "Select * from Races where ref_event='${race.ref_event}' and id=${race.id} ");

    return List.generate(maps.length, (i) {
      return Race.fromJSON(maps[i]);
    });
  }

  Future<void> deleteAllRace() async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
    await db.delete(
      "Races",
    );
    return;
  }

  Future<void> deleteAllRace_from_Event(Event_l event_l) async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
    await db.delete("Races", where: "ref_event", whereArgs: [event_l.name]);
    return;
  }

//

  /******************************************end race***************************************************/
  /******************************************COURSE*******************************************************************/

  Future<void> insert_course(Course course) async {
    await _database.insert(
      'Course',
      course.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> insertallcourses(Map<String, dynamic> js, Race race) async {
    final db1 = await database;
//    await delete_from_course(race);

    Course course = new Course.fromJSONServer(js);

    try {
      db1
          .insert('Course', course.toMap(),
              conflictAlgorithm: ConflictAlgorithm.replace)
          .then((value) {
//            await insert_Ra_RAC(ra_rac);
      });
    } catch (ex) {}

    return;
  }

  Future<List<Course>> Course_list(Race race) async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('Course',
        where: 'id = ?', whereArgs: [race.courseId], limit: 1);
    return List.generate(maps.length, (i) {
      return Course.fromJSON(maps[i]);
    });
  }

  Future<List<Course>> Course_list_from_id(int id) async {
    final db = await database;

    List<Map<String, dynamic>> maps = await db.rawQuery(
        "SELECT * FROM COURSE WHERE id = ( SELECT courseId from Tracking where id = ${id} )");

    return List.generate(maps.length, (i) {
      return Course.fromJSON(maps[i]);
    });
  }

  Future<List<Course>> Course_list_all(Event_l event_l) async {
    final db = await database;
//    print('event_l>>>>>>> ${event_l.toString()}');
//    List<Map<String, dynamic>> mapst = await db.rawQuery(
//        "SELECT courseId,ref_event from Races ");
//
//
//    print("mapst>>>>>>>>>>>>>>>>>>>>>> ${mapst}");
    List<Map<String, dynamic>> maps = await db.rawQuery(
        "SELECT * from Course where id in (SELECT courseId from Races where ref_event = '${event_l.reference}' )");
    return List.generate(maps.length, (i) {
      return Course.fromJSON(maps[i]);
    });
  }

  Future<Course> Course_from_race(Race race) async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query(
      'Course',
      where: 'id = ?',
      whereArgs: [race.courseId],
    );

    return maps.length > 0 ? Course.fromJSON(maps[0]) : null;
  }

  Future<void> delete_from_course(Race race) async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
    await db.execute("DELETE from Course where id = ${race.id}");
    return;
  }

  /****************************************END COURSE******************************************************************************/
  /*****************************Racer **********************************************/

  /***************************End Racer************************************************/
  Future<void> insert_racer(Racer racer) async {
//    await deleteAllRacer();
    print('racer>>>>>${racer.toString()}');
    await _database.insert(
      'Racer',
      racer.to_Map(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Racer>> Racer_list() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.rawQuery("SELECT * FROM Racer ");
    print('maps>>>rrrr>>>> ${maps}');
    return List.generate(maps.length, (i) {
      return Racer.fromJSON(maps[i]);
    });
  }

  Future<Racer> Racer_list_from(Event_l event, Race race) async {
    final db = await database;
    List<Map<String, dynamic>> mapspr =
        await db.rawQuery("SELECT * FROM Racer  ");
    print('maps>>>rrrr>>>>ttt ${mapspr}');
    print('event>>>> ${event.reference}   race ${race.reference} ');
    List<Map<String, dynamic>> maps = await db.rawQuery(
        "SELECT * FROM Racer where ref_event = '${event.reference}' and ref_race = '${race.reference}'  ");
    print('maps>>>rrrr>>>> ${maps}');
    return maps.length != 0 ? Racer.fromJSON(maps[0]) : null;
  }

  Future<void> deleteAllRacer() async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
    await db.delete("Racer");
    return;
  }

  Future<void> delete_racer(Racer racer) async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
    await db.rawDelete(
        "Delete from Racer where ref_event ='${racer.ref_event}' and  ref_race ='${racer.ref_race}' and bib =${racer.bib}  ");
    return;
  }

  /****************************************MANAGER**********************************************************************/
  Future<void> insert_manager(Manager manager) async {
    await _database.insert(
      'Manager',
      manager.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Manager>> Manager_list() async {
    final db = await database;
    List<Map<String, dynamic>> maps =
        await db.rawQuery("SELECT * FROM Manager");
    print('maps>>Manager>>${maps}');

    return maps != null
        ? List.generate(maps.length, (i) {
            return Manager.fromJSON(maps[i]);
          })
        : null;
  }

  Future<void> deleteAllManager() async {
    final db = await database;

    await db.delete("Manager");
    return;
  }

  /**********************************end manager ****************************************************/

  /**************************************loops***************************************************************/

  /**************************************TRACKING_WITH_INFORMATIN***************************************************************************************/

//  Future<void> check_is_open() async {
//    List<Checked> list_checked = new List();
//  await  TrackingInformation_list().then((list_tracking) async {
//      print('list_tracking>>>>F ${list_tracking}');
//      await Checked_list().then((value) {
//        list_checked = value.where((element) => !element.view);
//
//        print('list_checked>>>vvv ${list_checked}');
//      });
//      list_tracking.forEach((trackingInformation) async {
//        if (trackingInformation != null &&
//            trackingInformation.status == Status.open.toString() &&
//            trackingInformation.status == Status.waiting_for_gun.toString() &&
//            trackingInformation.openingTimeMs
//                    .difference(DateTime.now())
//                    .inMinutes <=
//                60 &&
//            trackingInformation.openingTimeMs
//                    .difference(DateTime.now())
//                    .inDays ==
//                0) {
//          list_checked
//              .where((element) =>
//                  element.reference == trackingInformation.reference &&
//                  element.ref_event == trackingInformation.ref_event)
//              .forEach((element) async {
//            Checked checked = element;
//            checked.state = true;
//            checked.is_open = true;
//            await update_checked(checked);
//          });
//        } else if (trackingInformation.status == Status.finished.toString() ||
//            trackingInformation.status == Status.guned.toString()) {
//          list_checked
//              .where((element) =>
//                  element.reference == trackingInformation.reference &&
//                  element.ref_event == trackingInformation.ref_event)
//              .forEach((element) async {
////        Checked checked =element;
////        checked.state=true;
//            await delete_Checked(element);
//          });
//        }
//
//        await _database.insert(
//          'Tracking',
//          trackingInformation.toMap(),
//          conflictAlgorithm: ConflictAlgorithm.replace,
//        );
//        Future.delayed(Duration(minutes: 1), check_is_open);
//      });
//    });
//  }

  Future<void> insert_tracking(TrackingInformation trackingInformation,
      {bool is_last = false}) async {
//    trackingInformation.openingTimeMs=DateTime.now().add(Duration(minutes: 40));
    List<Checked> list_checked = new List();
    await Checked_list().then((value) {
      list_checked = value;
    });

    if (trackingInformation != null &&
        check_can_check_in(trackingInformation)) {
      list_checked
          .where((element) =>
              !element.view &&
              element.reference == trackingInformation.reference &&
              element.ref_event == trackingInformation.ref_event)
          .forEach((element) async {
        Checked checked = element;
        checked.state = true;
        checked.is_open = true;

        await update_checked(checked);
      });
    } else if (trackingInformation.status == Status.finished.asString() ||
        trackingInformation.status == Status.guned.asString()) {
      list_checked
          .where((element) =>
              element.reference == trackingInformation.reference &&
              element.ref_event == trackingInformation.ref_event)
          .forEach((element) async {
//        Checked checked =element;
//        checked.state=true;
        await delete_Checked(element);
      });
    }
//    else if (trackingInformation != null &&
//        trackingInformation.status ==Status.open.toString() ||
//        trackingInformation.status == Status.waiting_for_gun.toString() &&
//        trackingInformation.openingTimeMs
//                .difference(DateTime.now())
//                .inMinutes <=
//            60 &&
//        trackingInformation.openingTimeMs.difference(DateTime.now()).inDays <
//            0) {
//      list_checked
//          .where((element) =>
//              element.reference == trackingInformation.reference &&
//              element.ref_event == trackingInformation.ref_event)
//          .forEach((element) async {
//        Checked checked = element;
//        checked.state = true;
//        checked.is_open = true;
//        await delete_Checked(checked);
//      });
//    }

    await _database
        .insert(
      'Tracking',
      trackingInformation.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    )
        .then(
      (va) {
        if (is_last) {
          print('lastitem>>>>>>');
          is_finish_load = true;
        }
      },
    );
  }

  Future<void> update_tracking(TrackingInformation trackingInformation) async {
    List<Checked> list_checked = new List();
    //fake time
//     trackingInformation.openingTimeMs=DateTime.now().add(Duration(minutes: 30));
    await Checked_list().then((value) {
      list_checked = value;
    });
    if (trackingInformation.status == Status.finished.asString() ||
        trackingInformation.status == Status.guned.asString()) {
      list_checked
          .where((element) =>
              element.reference == trackingInformation.reference &&
              element.ref_event == trackingInformation.ref_event)
          .forEach((element) async {
//        Checked checked =element;
//        checked.state=true;
        await delete_Checked(element);
      });
    }
//    trackingInformation.openingTimeMs=DateTime.now().add(Duration(minutes: 40));

    await _database.update(
      'Tracking',
      trackingInformation.toMap(),
      where: "reference = ? and  ref_event = ?",
      whereArgs: [trackingInformation.reference, trackingInformation.ref_event],
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> insertalltracking(
      Map<String, dynamic> js, Race race, Event_l event_l) async {
    final db1 = await database;
//    await delete_from_course(race);
    db1.transaction((db) async {
      TrackingInformation trackingInformation =
          new TrackingInformation.fromJSONSERVER(js, race, event_l);

      try {
        db
            .insert('Tracking', trackingInformation.toMap(),
                conflictAlgorithm: ConflictAlgorithm.replace)
            .then((value) {
//            await insert_Ra_RAC(ra_rac);
        });
      } catch (ex) {}
    });

    return;
  }

  Future<List<TrackingInformation>> TrackingInformation_list() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query(
      'Tracking',
    );
    print('maps>>> ${maps}');
    return List.generate(maps.length, (i) {
      return TrackingInformation.fromJSON(maps[i]);
    });
  }

  Future<List<TrackingInformation>> TrackingInformation_list_from_race(
      Event_l event_l, Race race) async {
    final db = await database;

    List<Map<String, dynamic>> maps = await db.query('Tracking',
        where: "reference = ? and ref_event = ? ",
        whereArgs: [race.reference, event_l.reference]);
    return List.generate(maps.length, (i) {
      return TrackingInformation.fromJSON(maps[i]);
    });
  }

  Future<List<TrackingInformation>> TrackingInformation_list_from_event(
      Event_l event_l) async {
    final db = await database;

    List<Map<String, dynamic>> maps = await db.query('Tracking',
        where: "ref_event = ? ", whereArgs: [event_l.reference]);
    return List.generate(maps.length, (i) {
      return TrackingInformation.fromJSON(maps[i]);
    });
  }

  Future<void> delete_all_TrackingInformation() async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
    await db.execute("DELETE from TrackingInformation ");
    return;
  }

  Future<void> delete_TrackingInformation(Race race) async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
    await db.execute(
        "DELETE from TrackingInformation where reference = ${race.reference}");
    return;
  }

  /******************************END********  TRACKING_WITH_INFORMATIN******************************************************************************************************************/

  /**************************************DETAILS Tracking*******************************************************************************************************************/
  Future<void> insert_detailstracking(DetailsTracking detailsTracking) async {
    await _database.insert(
      'Details_Tracking',
      detailsTracking.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> update_detailstracking(DetailsTracking detailsTracking) async {
    await _database.update(
      'Details_Tracking',
      detailsTracking.toMap(),
      where: "id = ?",
      whereArgs: [detailsTracking.id],
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<DetailsTracking>> detailstracking_list_from_event(
      Event_l event_l) async {
    List<DetailsTracking> lst_details = new List();
    Race_list_from_event(event_l).then((lst_race) {
      lst_race.forEach((race) {
        DetailsTracking_list_from_race(race, event_l).then((value) {
          lst_details.addAll(value);
        });
      });
    });

    return lst_details;
  }

  Future<List<DetailsTracking>> DetailsTracking_list_from_race(
      Race race, Event_l event_l) async {
    final db = await database;
    print("race>>>>> ${race.reference}");

    List<Map<String, dynamic>> maps = await db.query('Details_Tracking',
        where: "reference_race = ? and ref_event = ?",
        whereArgs: [race.reference, event_l.reference]);
    print('maps>22>>>>>>>  ${maps}');
    return List.generate(maps.length, (i) {
      return DetailsTracking.fromJSON(maps[i]);
    });
  }

  Future<DetailsTracking> DetailsTracking_limit1(
      Race race, Event_l event_l) async {
    final db = await database;
//    afs.reference_race

    List<Map<String, dynamic>> maps = await db.query('Details_Tracking',
        where: " reference_race = ? and ref_event = ? ",
        whereArgs: [race.reference, event_l.reference],
        limit: 1);
    print('maps>>>>>ddd ${maps}');
    return DetailsTracking.fromJSON(maps[0]);
  }

  Future<List<DetailsTracking>> DetailsTracking_list(Event_l event_l) async {
    final db = await database;

    ///INNER JOIN  Races ON  Tracking.reference = Races.reference where ref_event = ${event_l.name} where Races.reference in (SELECT reference from Races where ref_event =${event_l.name})
    List<Map<String, dynamic>> maps_new = await db.rawQuery(
        "SELECT * from Details_Tracking  where ref_event = '${event_l.reference}'  ORDER BY Details_Tracking.id DESC");
    print('res>>>> ${maps_new}');
    return List.generate(maps_new.length, (i) {
      return DetailsTracking.fromJSON(maps_new[i]);
    });
  }

  Future<void> delete_all_DetailsTracking() async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
    await db.execute("DELETE from Details_Tracking ");
    return;
  }

  Future<void> delete_DetailsTracking(Race race, Event_l event_l) async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
    await db.execute(
        "DELETE from Details_Tracking where reference_race = ${race.name} and ref_event = '${event_l.reference}' ");
    return;
  }

  /******************************END***************************DETAILS Tracking*****************************************************************************************************************/
  /**********************************Begin********Checked**************************************************************************************************************************************/
  Future<void> insert_checked(Checked checked) async {
    print('checked>>> ${checked.toString()}');
    await _database.insert(
      'Checked',
      checked.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<void> update_checked(Checked checked) async {
    print('checked>>>>>up ${checked.toString()}');

    await _database.update(
      'Checked',
      checked.toMap(),
      where: 'reference = ? and ref_event = ?',
      whereArgs: [checked.reference, checked.ref_event],
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }

  Future<List<Checked>> Checked_list() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query(
      'Checked',
    );

    return List.generate(maps.length, (i) {
      return Checked.fromJSON(maps[i]);
    });
  }

  Future<List<Checked>> Checked_list_open_races() async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.query('Checked',
        where: 'state = ? and is_open = ?', whereArgs: [1, 1]);
    print('listchecked>>>> ${maps}');
    return List.generate(maps.length, (i) {
      return Checked.fromJSON(maps[i]);
    });
  }

  Future<List<Checked>> Checked_list_from_race(
      Race race, Event_l event_l) async {
    final db = await database;
    List<Map<String, dynamic>> maps = await db.rawQuery(
        " SELECT * FROM Checked where reference = '${race.reference}' and ref_event = '${event_l.reference}' ");
    return List.generate(maps.length, (i) {
      return Checked.fromJSON(maps[i]);
    });
  }

  Future<void> delete_all_Checked() async {
    final db = await database;

    await db.execute("DELETE from Checked ");
    return;
  }

  Future<void> delete_Checked(Checked checked) async {
    final db = await database;
//    await db.execute("PRAGMA foreign_keys = ON;");
    await db.execute(
        "DELETE from Checked where reference = '${checked.reference}'  ");
    return;
  }

  Future<void> delete_Checked_from_race(Race race) async {
    final db = await database;
    await db
        .execute("DELETE from Checked where reference = '${race.reference}'  ");
    return;
  }
/**********************************End********Checked**************************************************************************************************************************************/

}
