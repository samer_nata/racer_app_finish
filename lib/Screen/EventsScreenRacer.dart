import 'dart:async';
import 'dart:convert';

import 'dart:io';


import 'package:device_info/device_info.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geolocator/geolocator.dart';

import 'package:http/http.dart' as http;
import 'package:racer_app_finish/Appearance.dart';
import 'package:racer_app_finish/Checking/CheckMeInScreen.dart';
import 'package:racer_app_finish/Enum.dart';
import 'package:racer_app_finish/Models/Checked.dart';
import 'package:racer_app_finish/Models/Event.dart';

import 'package:racer_app_finish/Models/Race.dart';
import 'package:racer_app_finish/Models/Racer.dart';

import 'package:racer_app_finish/Models/User.dart';
import 'package:racer_app_finish/ColorsF.dart' as col;
import '../Database.dart';

import 'package:intl/intl.dart' as nl;

import '../SplashScreen.dart';
import 'RacesScreenRacer.dart';

class EventsScreenRacer extends StatefulWidget {
  bool try_load;

  EventsScreenRacer(this.try_load);

  @override
  _EventsScreenRacerState createState() => _EventsScreenRacerState(try_load);
}

class _EventsScreenRacerState extends State<EventsScreenRacer> {
  static final DeviceInfoPlugin deviceInfoPlugin = DeviceInfoPlugin();
  Map<String, dynamic> _deviceData = <String, dynamic>{};
  bool try_load;
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  ScrollController _scrollController;
  List<Race> races = new List();

  List<Race> races_next = new List();
  List<Racer> racer_list = new List();
  List<Checked> checked_list = new List();
  Racer racer =
      new Racer(bib: 5, id: 4, surname: 's', firstname: 's', club: 's');
  bool is_finish_loading = false;

//  DateTime time_d=DateTime.fromMicrosecondsSinceEpoch(1599411600000);

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('vill du logga ut?'),
          actions: <Widget>[
            FlatButton(
              child: Text('Annullera'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: () async {
                DBProvider.db.deleteAllRacer().then((value) {
                  setState(() {
                    racer_list = [];
                  });
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      '/EventsScreen', (Route<dynamic> route) => false);
                  return;
                });
              },
            ),
          ],
        );
      },
    );
  }

  Future<void> get_race_next() async {
    await DBProvider.db.Race_list_false(DateTime.now()).then((data) {
      print(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> ${data}");
      setState(() {
        races_next = data;
      });
    });
  }

  Widget ourBottomSheet() {
    get_race_next();
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
            topRight: Radius.circular(25), topLeft: Radius.circular(25)),
        color: Colors.blueAccent[200],
      ),
      height: 325,
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              SizedBox(
                width: MediaQuery.of(context).size.width,
                height: 40,
              ),
              Expanded(
                child: ListView.builder(
                    itemCount: races_next.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0, top: 2.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width / 1.1,
                          child: SingleChildScrollView(
                            scrollDirection: Axis.horizontal,
                            child: Row(
                              children: <Widget>[
                                Text(
                                  "${races_next[index].name}",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: font_size_body,
                                      fontWeight: FontWeight.w100),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(2.0),
                                  child: Column(
                                    children: List.generate(
                                        races_next[index]
                                            .jso['splitNames']
                                            .length,
                                        (index2) => Text(
                                              races_next[index]
                                                  .jso['splitNames'][index2],
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: font_size_body,
                                                  fontWeight: FontWeight.w100),
                                            )),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    "${showdate(races_next[index].startTimeMs)}",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: font_size_body,
                                        fontWeight: FontWeight.w100),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      );
                    }),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(right: 16.0, top: 16.0),
            child: Align(
              alignment: Alignment.topRight,
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.close,
                  color: Colors.white,
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.topLeft,
            child: Padding(
              padding: const EdgeInsets.only(top: 16.0, left: 8.0),
              child: Text(
                "Other events : ",
                style: TextStyle(
                    color: Colors.white,
                    fontSize: font_size_body,
                    fontWeight: FontWeight.w700),
              ),
            ),
          )
        ],
      ),
    );
  }

//  Future<void> getarrangments() async {
//    await http
//        .get(
//            'http://54.77.120.67:8080/rest/organizers/laliga/arrangements?class=App')
//        .then((data) async {
////      races = [];
//      var dataServer = jsonDecode(data.body);
//      List temp = dataServer['arrangements'];
//
////    List temp2 = temp[0]['races'];
////    List temp3= temp2[0]['splitNames'];
//
////    print(temp);
////    temp.forEach((element) {
//////      print(element);
////      Event_l ev=new Event_l.fromJSONServer(element);
////      print(ev.toString());
////    });
//
////print("temp<<<<<<<<<<<<<<${temp}");
//      if (temp != null && dataServer.length > 0) {
//        await DBProvider.db.deleteAllEvent();
//
//        await DBProvider.db.insertallEvent(temp);
//        setState(() {});
//      } else if (dataServer.length == 0) {
//        await DBProvider.db.deleteAllEvent().then((value) {
//          setState(() {
//            print('>>>>>>>>is_finish_loading ${is_finish_loading}');
//            is_finish_loading = true;
//          });
//        });
//        setState(() {});
//      }
//    });
//  }

  Future<void> getarrangments() async {
    try {
      await http
          .get(
              'http://54.77.120.67:8080/rest/organizers/laliga/arrangements?class=App')
          .catchError((err) {
        final snackBar = new SnackBar(
            action: SnackBarAction(
              label: 'Undo',
              onPressed: () {
                // Some code to undo the change.
              },
            ),
            content: Row(
              children: <Widget>[
                Text(' please check internet connection'),
                InkWell(
                  child: Icon(Icons.refresh),
                  onTap: () {
//              _scaffoldKey.currentState.removeCurrentSnackBar();
                    _scaffoldKey.currentState.removeCurrentSnackBar();
                    getarrangments();
                  },
                )
              ],
            ));
        _scaffoldKey.currentState.showSnackBar(snackBar);
      }).then((data) async {
//      races = [];
        var dataServer = jsonDecode(data.body);
        List temp = dataServer['arrangements'];

        await DBProvider.db.database;
        if (temp != null && dataServer.length > 0) {
          await DBProvider.db.deleteAllEvent();

          await DBProvider.db.insertallEvent(temp).then((value) {
            go_to_page();
            return;
          });

//        setState(() {});
        } else if (dataServer.length == 0) {
          await DBProvider.db.deleteAllEvent().then((value) {
            go_to_page();
          });
        }
      });
    } catch (ex) {
      print('ex>>>>>>>>>> ${ex}');
      final snackBar = new SnackBar(
          action: SnackBarAction(
            label: 'Undo',
            onPressed: () {
              // Some code to undo the change.
            },
          ),
          content: Row(
            children: <Widget>[
              Text(' please check internet connection'),
              InkWell(
                child: Icon(Icons.refresh),
                onTap: () {
//              _scaffoldKey.currentState.removeCurrentSnackBar();
                  _scaffoldKey.currentState.removeCurrentSnackBar();
                  getarrangments();
                },
              )
            ],
          ));
      _scaffoldKey.currentState.showSnackBar(snackBar);
    }
  }

  Future<void> go_to_page() {
    Timer.periodic(Duration(seconds: 1), (timer) async {
      print('timer>>>> ${timer.tick} ${DBProvider.db.is_finish_load}');
      if (DBProvider.db.is_finish_load && !is_dispose) {
        await get_checked_list();
        setState(() {
          is_finish_loading = true;
        });
//        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (_)=>EventsScreen(false)), (route) => false);
        timer.cancel();
        return;
      } else if (is_dispose) {
        timer.cancel();
      }
    });
  }

  Future<void> load() async {
    await get_racer();
  }

  bool is_dispose = false;

  @override
  void dispose() {
    is_dispose = true;
    super.dispose();
  }

  Future<void> get_checked_list() async {
    await DBProvider.db.Checked_list_open_races().then((value) {
      if (value.length > 0 && !is_dispose) {
        setState(() {
          checked_list = value;
        });
      } else if (!is_dispose) {
        setState(() {
          checked_list = [];
        });
      }
    });
  }

  Future<void> get_racer() async {
    await DBProvider.db.Racer_list().then((value) {
      print('>>>>>>racerscreen>>>>>>>racer_list  ${value}');
      if (value != null && value.length > 0 && !is_dispose) {
        setState(() {
          racer_list = value;
          racer = racer_list[0];
          print(">>>>>>racerscreen>>>>>>>racer_list ${racer_list}");
        });
      }
      if (value == null) {
        racer_list = [];
      }
    });
  }

  @override
  void initState() {
    _scrollController = ScrollController();

    if (try_load) {
      getarrangments();
    }
//    racer_list.add(racer);
//    getRaces();
//    get_manager();
    load();
//    getarrangments();

    super.initState();
  }





  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,


        endDrawer: get_drawer(),
        appBar: PreferredSize(
          child: Stack(
            children: <Widget>[
              Center(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 40),
                  child: Container(
//                  width: 150,
                    child: Text(
                      "La Liga Tävling",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: 23,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 150,
                child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          InkWell(
                            child: Padding(
                              padding: EdgeInsets.only(top: 60),
                              child: Container(
                                width: 80,
                                height: 50,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image:
                                            AssetImage('Images/LaLigaLogo.png'),
                                        fit: BoxFit.fill)),
                              ),
                            ),
                            onTap: () {
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) => EventsScreenRacer(true)),
                                  (route) => false);
                            },
                          ),
                          Expanded(
                            child: Center(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                      top: 70,
                                    ),
                                    child: Center(
                                      child: Container(
                                        width: 200,
                                        child: SingleChildScrollView(
                                          scrollDirection: Axis.horizontal,
                                          child: Text(
                                            "Dagens evenemang",
                                            style: TextStyle(
                                                color: Colors.white,
                                                fontSize: 18,
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Padding(
                            padding:
                                EdgeInsets.only(right: 20, top: 0, bottom: 3.5),
                            child: Container(
                              child: is_finish_loading &&
                                      racer_list.length != 0 &&
                                      checked_list.length != 0
                                  ? InkWell(
                                      onTap: () {
                                        _scaffoldKey.currentState
                                            .openEndDrawer();
//                                        _showMyDialog();
                                      },
                                      child: Stack(
                                        alignment: Alignment.topRight,
                                        children: <Widget>[
                                          Padding(
                                            padding:  EdgeInsets.only(
                                                right:get_length_checked() > 0? 8:0, top:get_length_checked() > 0? 5:0),
                                            child: Container(
                                              width: 25,
                                              height: 25,
                                              decoration: BoxDecoration(
                                                  image: DecorationImage(
                                                      image: AssetImage(
                                                          'Images/redChecked.png'),
                                                      fit: BoxFit.fill)),
                                            ),
                                          ),
                                          is_finish_loading
                                              ? get_length_checked() > 0
                                                  ? Container(
                                                      decoration: BoxDecoration(
                                                          shape:
                                                              BoxShape.circle,
                                                          color: Colors
                                                              .orangeAccent,
                                                          border: Border.all(
                                                              color:
                                                                  Colors.white,
                                                              width: 1)),
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                .all(2.0),
                                                        child: Text(
                                                          get_length_checked()
                                                              .toString(),
                                                          style: TextStyle(
                                                              color:
                                                                  Colors.white),
                                                          textAlign:
                                                              TextAlign.center,
                                                        ),
                                                      ))
                                                  : Container()
                                              : Container(),
                                        ],
                                      ),
                                    )
                                  : Container(
                                      width: 25,
                                      height: 25,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  'Images/greyChecked.png'),
                                              fit: BoxFit.fill)),
                                    ),
                            ),
                          ),
                        ],
                        crossAxisAlignment: CrossAxisAlignment.end,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: Container(
                          width: MediaQuery.of(context).size.width,
                          color: Colors.blue,
                          height: 2,
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
          preferredSize: Size.fromHeight(100),
        ),
        backgroundColor: Colors.black,
        body: Column(
          children: <Widget>[
            !try_load
                ? Expanded(
                    child: eventsListView(),
                  )
                : try_load && is_finish_loading
                    ? Expanded(
                        child: eventsListView(),
                      )
                    : SpinKitThreeBounce(
                        size: 15,
                        color: Colors.blue,
                      ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 25,
                  child: InkWell(
                    onTap: () {
                      _scaffoldKey.currentState
                          .showBottomSheet((context) => ourBottomSheet(),
                              backgroundColor: Colors.black)
                          .closed
                          .then((val) {
                        setState(() {});
                      });
                    },
                    child: Stack(
                      children: <Widget>[
                        Text(
                          "Andra evenemang",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: font_size_body,
                              fontWeight: FontWeight.w100),
                        ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child: Padding(
                            padding: const EdgeInsets.only(right: 24.0),
                            child: Icon(
                              Icons.keyboard_arrow_up,
                              color: Colors.blue,
                              size: 30,
                            ),
                          ),
                        ),
                      ],
                    ),
                  )),
            )
          ],
        ));
  }

  Drawer get_drawer() {
    child:
    return Drawer(
      child: Container(
        color: col.col_from_argb,
        child: ListView(
          scrollDirection: Axis.vertical,
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.only(top: 40),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white24),
                child: CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Container(
                    width: 80,
                    height: 50,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('Images/LaLigaLogo.png'),
                            fit: BoxFit.fill)),
                  ),
                ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: checked_list.map((e) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    child: InkWell(
                      onTap: () async {
                        Event_l event;
                        Race race;
                        await DBProvider.db.Events_list().then((value) {
                          event = value.firstWhere(
                              (element) => element.reference == e.ref_event,
                              orElse: () => null);
//                          print('event>>>> ${event}');
                        });
                        if (event != null) {
                          await DBProvider.db
                              .Race_list_from_event(event)
                              .then((value) {
                            race = value.firstWhere(
                                (element) => element.reference == e.reference,
                                orElse: () => null);
                          });
                          print('race>>>> ${race}');
                        }
                        print('here>>>>');
                        if (event != null &&
                            race != null &&
                            racer_list.firstWhere((element) =>
                                    element.ref_event == event.reference &&
                                    element.ref_race == race.reference) !=
                                null) {
                          print('here>>>ddddd>');
                          e.view = true;

                          await DBProvider.db.update_checked(e);
                          Navigator.of(context).pop();
                          Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) =>
                                          CheckMeInScreen(event, race, false)))
                              .then((value) {
                            get_checked_list();
                          });
                        }
                      },
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: e.view ? Colors.white54 : Colors.blueGrey,
                        ),
                        child: Center(
                          child: Text(
                            '${e.name_event} - ${e.name}',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: font_size_body,
                                fontWeight: FontWeight.w100),
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              }).toList(),
            )
          ],
        ),
      ),
    );
  }

  FutureBuilder<List<Event_l>> eventsListView() {
    return FutureBuilder(
      initialData: [],
      future: DBProvider.db.Events_list(),
      builder: (BuildContext context, AsyncSnapshot<List<Event_l>> snapshot) {
        if (snapshot.hasData && snapshot.data.length > 0) {
          return ListView.builder(
              itemCount: snapshot.data.length,
              controller: _scrollController,
              itemBuilder: (BuildContext context, int index) {
                Event_l item = snapshot.data[index];
                print(item.toString());
                return Padding(
                  padding: EdgeInsets.only(
                      left: 3, right: 3, top: index == 0 ? 15 : 2),
                  child: Container(
                    decoration: BoxDecoration(
                        color: col.col_from_argb,
                        border: Border(
                            bottom: index != snapshot.data.length - 1
                                ? BorderSide(
                                    color: Colors.blueGrey[50], width: 0.3)
                                : BorderSide(),
                            top: index == 0
                                ? BorderSide(
                                    color: Colors.blueGrey[50], width: 0.3)
                                : BorderSide())),
                    height: 65,
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            new MaterialPageRoute(
                                builder: (context) =>
                                    RacesScreenRacer(item))).then((value) {get_checked_list();


                        });
                      },
                      child: Container(
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Container(
//                                    width: 275,
                                      child:
                                          Text('${item.name}, ${item.location}',
                                              //item.name
                                              style: TextStyle(
                                                color: Colors.white,
                                                fontSize: font_size_body,
                                                fontWeight: FontWeight.w400,
                                              ),
                                              overflow: TextOverflow.ellipsis,
                                              maxLines: 1),
                                    ),
                                  ),
//                                Padding(
//                                  padding: const EdgeInsets.only(
//                                      left: 8.0, top: 2.0),
//                                  child: Container(
//                                    width: 225,
//                                    child: Text(
//                                      "Location : ${item.location != null ? item.location : '....'}",
//                                      //item.location
//                                      style: TextStyle(
//                                          color: Colors.white,
//                                          fontSize: 18,
//                                          fontWeight: FontWeight.w100),
//                                    ),
//                                  ),
//                                )
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 15.0),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              });
        } else if (snapshot == null) {
          return Center(
              child: SpinKitThreeBounce(
            size: 17,
            color: Colors.blue,
          ));
        } else {
          return Center(
              child: Text(
            "No Events Here",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.w300),
          ));
        }
//        return Center(
//          child: CircularProgressIndicator(
//            backgroundColor: Colors.green,
//          ),
//        );
      },
    );
  }

  int get_length_checked() {
    return checked_list
        .where((element) => !element.view && element.is_open)
        .length;
  }

  _EventsScreenRacerState(this.try_load);
}

// we are learning pull with git ....
