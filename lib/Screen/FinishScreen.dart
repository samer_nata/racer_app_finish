import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:racer_app_finish/Appearance.dart';
import 'package:racer_app_finish/Models/Course.dart';
import 'package:racer_app_finish/Models/Coursepoints.dart';
import 'package:racer_app_finish/Models/Details_Tracking.dart';
import 'package:racer_app_finish/Models/Event.dart';
import 'package:racer_app_finish/Models/Race.dart';
import 'package:racer_app_finish/Models/Racer.dart';
import 'package:racer_app_finish/Models/Timings.dart';
import 'package:racer_app_finish/Models/TrackingWithInformation.dart';
import 'package:racer_app_finish/ColorsF.dart' as col;
import 'package:racer_app_finish/SplashScreen.dart';
//import 'package:wakelock/wakelock.dart';
import '../Database.dart';
import 'EventsScreenRacer.dart';

class FinishedScreen extends StatefulWidget {
  Event_l event_l;
  Race race;

  Racer racer;

  FinishedScreen(
    this.event_l,
    this.race,
    this.racer,
  );

  @override
  _FinishedScreenState createState() =>
      _FinishedScreenState(event_l, race, racer);
}

class _FinishedScreenState extends State<FinishedScreen> {
  Event_l event_l;
  Race race;

  Racer racer;
  DetailsTracking detailsTracking;
  List<Timing> list_finish = new List();
  int place;
  bool is_finish_star = false;
  List<TrackingInformation> lst_tracking = new List();
  bool is_finish_from_server = false;
  Course course;

  _FinishedScreenState(this.event_l, this.race, this.racer);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.only(top: 8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(top: 0),
                      child: Container(
                        child: Text(
                          "${event_l.name}",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 4),
                      child: Container(
                        child: Text(
                          "${race.name}\nFinish",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
//                    Padding(
//                      padding: const EdgeInsets.only(top: 4),
//                      child: Container(
//                        child: Text(
//                          "Finish",
//                          style: TextStyle(
//                              color: Colors.white,
//                              fontSize: 20,
//                              fontWeight: FontWeight.w400),
//                        ),
//                      ),
//                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.only(top: 15, right: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[],
                ),
              ),
            ),
            Container(
              child: InkWell(
                child: Padding(
                  padding: EdgeInsets.only(top: 60),
                  child: Container(
                    width: 80,
                    height: 50,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('Images/LaLigaLogo.png'),
                            fit: BoxFit.fill)),
                  ),
                ),
                onTap: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (_) => EventsScreenRacer(true)),
                      (route) => false);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 120),
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.blue,
                height: 2,
              ),
            ),
          ],
        ),
        preferredSize: Size.fromHeight(100),
      ),
      backgroundColor: Colors.black,
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Padding(
          padding: const EdgeInsets.only(left: 3, right: 3, top: 15),
          child: Container(
            width: MediaQuery.of(context).size.width,
//          decoration: BoxDecoration(
//            shape: BoxShape.rectangle,
//            border: Border.all(color: Colors.black, width: 7),
//            borderRadius: BorderRadius.circular(20),
//            color: Color.fromARGB(60,105,105,105),
//          ),,
            decoration: BoxDecoration(
                color: col.col_from_argb,
                border: Border(
                    top: BorderSide(color: Colors.blueGrey[50], width: 0.3))),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                      left: 8, bottom: 20, top: 10, right: 8),
                  child: Text('You have finished the race.',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: font_size_body,
                          fontWeight: FontWeight.w300)),
                ),
//              Padding(
//                padding: const EdgeInsets.all(8.0),
//                child: Text('You have finished the race.',
//                    style: TextStyle(
//                        color: Colors.white,
//                        fontSize: 15,
//                        fontWeight: FontWeight.w300)),
//              ),
                is_finish_from_server
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                            'Your time at finish ${course != null && course.coursepoints != null && course.coursepoints.length > 0 ? (course.distance.toInt() / 1000).toStringAsFixed(1) : ''}  km:  \n  ${detailsTracking != null ? detailsTracking.lst_timings.firstWhere((element) => element.courspoint == course.coursepoints.firstWhere((element) => element.reference == "finish", orElse: () => null).reference && element.bib == racer.bib, orElse: () => null) != null ? '${get_time().toString().substring(10,19)}' : "No Set" : ''}',
//                            'Your time at finish ${course != null && course.coursepoints != null && course.coursepoints.length > 0 ? (course.distance.toInt() / 1000).toStringAsFixed(1) : ''}  km:  \n  ${detailsTracking != null ? detailsTracking.lst_timings.firstWhere((element) => element.courspoint == course.coursepoints.firstWhere((element) => element.reference == "finish", orElse: () => null).reference && element.bib == racer.bib, orElse: () => null) != null ? '${(DateTime.fromMillisecondsSinceEpoch(detailsTracking.lst_timings.firstWhere((element) => element.courspoint == course.coursepoints.last.reference && element.bib == racer.bib, orElse: () => null).duration).minute)}:${(DateTime.fromMillisecondsSinceEpoch(detailsTracking.lst_timings.firstWhere((element) => element.courspoint == course.coursepoints.last.reference && element.bib == racer.bib, orElse: () => null).duration).second)}:${(DateTime.fromMillisecondsSinceEpoch(detailsTracking.lst_timings.firstWhere((element) => element.courspoint == course.coursepoints.last.reference && element.bib == racer.bib, orElse: () => null).duration).millisecond)}' : "No Set" : ''}',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: font_size_body,
                                fontWeight: FontWeight.w400)),
                      )
                    : SpinKitThreeBounce(
                        size: 17,
                        color: Colors.blue,
                      ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text('You lap times :',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: font_size_body,
                          fontWeight: FontWeight.w400)),
                ),
                is_finish_from_server && course != null
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: course.coursepoints
                            .where((element) =>
                                element.reference !=
                                    course.coursepoints.last.reference &&
                                element.reference !=
                                    course.coursepoints.first.reference)
                            .map((e) => Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    '${(e.distance.toInt() / 1000).toStringAsFixed(1)} Km ${detailsTracking.lst_timings.firstWhere((element) => element.courspoint == e.reference && element.bib == racer.bib, orElse: () => null) != null ? '${get_time_p(e).toString().substring(10,19)}' : "No Set"}',
                                   // '${(e.distance.toInt() / 1000).toStringAsFixed(1)} Km ${detailsTracking.lst_timings.firstWhere((element) => element.courspoint == e.reference && element.bib == racer.bib, orElse: () => null) != null ? '${(DateTime.fromMillisecondsSinceEpoch(detailsTracking.lst_timings.firstWhere((element) => element.courspoint == e.reference && element.bib == racer.bib, orElse: () => null).duration).minute)}:${(DateTime.fromMillisecondsSinceEpoch(detailsTracking.lst_timings.firstWhere((element) => element.courspoint == e.reference && element.bib == racer.bib, orElse: () => null).duration).second)}:${(DateTime.fromMillisecondsSinceEpoch(detailsTracking.lst_timings.firstWhere((element) => element.courspoint == e.reference && element.bib == racer.bib, orElse: () => null).duration).millisecond)}' : "No Set"}',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: font_size_body,
                                        fontWeight: FontWeight.w400),
                                  ),
                                ))
                            .toList(),
                      )
                    : SpinKitThreeBounce(
                        size: 17,
                        color: Colors.blue,
                      ),
                is_finish_from_server && place != null
                    ? Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text('Place in your race: ${place}',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: font_size_body,
                                fontWeight: FontWeight.w400)),
                      )
                    : SpinKitThreeBounce(
                        size: font_size_body,
                        color: Colors.blue,
                      ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> get_information_from_server() async {
    try {
      await DBProvider.db
          .get_information_race(race, event_l)
          .then((value) async {
        await refresh_informa();
      }).catchError((err) {
        get_information_from_server();
      });
    } catch (ex) {
      print('ex>>>>>>>>>>>>>> ${ex}');
      setState(() {
        is_finish_from_server = true;
      });
    }
  }

  Future<void> refresh_informa() async {
    await DBProvider.db
        .TrackingInformation_list_from_race(event_l,race)
        .then((value) async {
      lst_tracking = value;

      await DBProvider.db.Race_list_refresh(race, event_l).then((value) {
        race = value[0];
      });
    });

    await DBProvider.db.DetailsTracking_limit1(race, event_l).then((value) {
      if (value != null) {
        detailsTracking = value;
        list_finish = detailsTracking.lst_timings
            .where((element) => element.courspoint == 'finish')
            .toList(growable: true);
        if (list_finish.where((element) => element.bib == racer.bib).length ==
            0) {

            place = 0;

        }
        else if(list_finish.length<2){
          place =1;

        }

       else if (list_finish.length>=2) {
          list_finish.sort((a, b) => DateTime.fromMillisecondsSinceEpoch(a.duration).compareTo(DateTime.fromMillisecondsSinceEpoch(b.duration)));
          place =
              list_finish.indexWhere((element) => element.bib == racer.bib) +
                  1;
        }
        setState(() {

        });
      }
    }).catchError((err) {
      print('errrr>>>> ${err}');
      setState(() {
        place = 0;
      });
    });

    get_course();
  }

  Future<void> get_course() async {
    await DBProvider.db.Course_from_race(race).then((value) {
      setState(() {
        course = value;
        is_finish_from_server = true;
      });
    });
  }

  @override
  Future<void> initState() {

    get_information_from_server();
    super.initState();
  }
DateTime get_time(){
 return (DateTime.fromMillisecondsSinceEpoch(detailsTracking.lst_timings.firstWhere((element) => element.courspoint == course.coursepoints.last.reference && element.bib == racer.bib, orElse: () => null).duration)).subtract(Duration(hours: 1));
}
  DateTime get_time_p(Coursepoints e){


    return (DateTime.fromMillisecondsSinceEpoch(detailsTracking.lst_timings.firstWhere((element) => element.courspoint == e.reference && element.bib == racer.bib, orElse: () => null).duration).subtract(Duration(hours: 1)));
  }
  @override
  void dispose() {
//    Wakelock.disable();
    super.dispose();
  }
}
