import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';

import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:geolocator/geolocator.dart';
import 'package:imei_plugin/imei_plugin.dart';
import 'package:racer_app_finish/Checking/CheckMeInScreen.dart';
import 'package:racer_app_finish/Enum.dart';
import 'package:racer_app_finish/ManagerFunction/OurTabBarView/ScreenManager/EventsScreenManager.dart';
import 'package:racer_app_finish/ManagerFunction/OurTabBarView/ScreenManager/Login_Manager_From_Racer.dart';
import 'package:racer_app_finish/ManagerFunction/OurTabBarView/ScreenManager/RacesScreenManager.dart';
import 'package:racer_app_finish/Models/Checked.dart';
import 'package:racer_app_finish/Models/Course.dart';
import 'package:racer_app_finish/Models/Details_Tracking.dart';

import 'package:racer_app_finish/Models/Event.dart';
import 'package:racer_app_finish/Models/Manager.dart';
import 'package:racer_app_finish/Models/Racer.dart';
import 'package:racer_app_finish/Models/TrackingWithInformation.dart';
import 'package:racer_app_finish/User/Login.dart';
import 'package:toast/toast.dart';

import '../Appearance.dart';
import '../Database.dart';

import '../LiveResultsRacer.dart';
import '../Models/Race.dart';
import '../SplashScreen.dart';
import 'EventsScreenRacer.dart';
import 'package:racer_app_finish/ColorsF.dart' as col;

class RacesScreenRacer extends StatefulWidget {
  Event_l event_l;

  RacesScreenRacer(this.event_l);

  @override
  _RacesScreenRacerState createState() => _RacesScreenRacerState(event_l);
}

class _RacesScreenRacerState extends State<RacesScreenRacer> {
  Key key;
  Event_l event_l;

  _RacesScreenRacerState(this.event_l);

  ScrollController _scrollController;

  List<Racer> racer_list = new List();
  List<TrackingInformation> lst_tracking = new List();

  List<Manager> manager_list = new List();

//  Racer racer =
//  new Racer(id: 1, firstname: 'ss',club: 's',bib: 5,surname: 'dd');
//  Racer racer;
  String imei;
  DetailsTracking detailsTracking;
  Racer racer_r;
  TrackingInformation trackingInformation;
  var _key_scaffold = GlobalKey<ScaffoldState>();
  List<Checked> checked_list = new List();

  Future<void> get_manager() {
    DBProvider.db.Manager_list().then((value) {
      if (value.length > 0) {
        setState(() {
          manager_list = value;
        });
      } else {
        setState(() {
          manager_list = [];
        });
      }
    });
  }

//  Drawer get_drawer() {
//    child:
//    return Drawer(
//      child: Container(
//        color: col.col_from_argb,
//        child: ListView(
//          scrollDirection: Axis.vertical,
//          // Important: Remove any padding from the ListView.
//          padding: EdgeInsets.only(top: 40),
//          children: <Widget>[
//            Padding(
//              padding: const EdgeInsets.all(8.0),
//              child: Container(
//                height: 100,
//                decoration: BoxDecoration(
//                    borderRadius: BorderRadius.circular(10),
//                    color: Colors.white24),
//                child: CircleAvatar(
//                  backgroundColor: Colors.white,
//                  child: Container(
//                    width: 80,
//                    height: 50,
//                    decoration: BoxDecoration(
//                        image: DecorationImage(
//                            image: AssetImage('Images/LaLigaLogo.png'),
//                            fit: BoxFit.fill)),
//                  ),
//                ),
//              ),
//            ),
//            Column(
//              mainAxisAlignment: MainAxisAlignment.center,
//              children: checked_list.map((e) {
//                return Padding(
//                  padding: const EdgeInsets.all(8.0),
//                  child: Container(
//                    child: InkWell(
//                      onTap: () async {
//                        Event_l event;
//                        Race race;
//                        await DBProvider.db.Events_list().then((value) {
//                          event = value.firstWhere(
//                                  (element) => element.reference == e.ref_event,
//                              orElse: () => null);
////                          print('event>>>> ${event}');
//                        });
//                        if (event != null) {
//                          await DBProvider.db
//                              .Race_list_from_event(event)
//                              .then((value) {
//                            race = value.firstWhere(
//                                    (element) => element.reference == e.reference,
//                                orElse: () => null);
//                          });
//                          print('race>>>> ${race}');
//                        }
//                        print('here>>>>');
//                        if (event != null &&
//                            race != null &&
//                            racer_list.firstWhere((element) => element.ref_event==event.reference&&element.ref_race==race.reference)!=null) {
//                          print('here>>>ddddd>');
//                          e.view = true;
//
//                          await DBProvider.db.update_checked(e);
//                          Navigator.of(context).pop();
//                          Navigator.push(
//                              context,
//                              MaterialPageRoute(
//                                  builder: (_) =>
//                                      CheckMeInScreen(event, race,false)))
//                              .then((value) {
//                            get_checked_list();
//                          });
//                        }
//                      },
//                      child: Container(
//                        height: 60,
//                        decoration: BoxDecoration(
//                          borderRadius: BorderRadius.circular(10),
//                          color: e.view ? Colors.white54 : Colors.blueGrey,
//                        ),
//                        child: Center(
//                          child: Text(
//                            '${e.name_event} - ${e.name}',
//                            style: TextStyle(
//                                color: Colors.white,
//                                fontSize: 18,
//                                fontWeight: FontWeight.w100),
//                          ),
//                        ),
//                      ),
//                    ),
//                  ),
//                );
//              }).toList(),
//            )
//          ],
//        ),
//      ),
//    );
//  }

  Future<void> get_racer() async {
    await DBProvider.db.Racer_list().then((value) async {
      print('>>>>>>racerscreen>>>>>>>racer_list  ${value}');
      if (value != null && value.length > 0) {
        setState(() {
          racer_list = value;
//          racer = racer_list[0];
          print(">>>>>>racerscreen>>>>>>>racer_list ${racer_list}");
        });
      }
      if (value == null) {
        racer_list = [];
      }
    });
    await get_checked_list();
  }

  Future<void> get_checked_list() async {
    await DBProvider.db.Checked_list_open_races().then((value) {
      if (value.length > 0) {
        setState(() {
          checked_list = value;
        });
      } else {
        setState(() {
          checked_list = [];
        });
      }
    });
  }

  Future get_imei() async {
    try {
      imei = await ImeiPlugin.getImei();

      await get_checked_list();
      setState(() {});
    } catch (ex) {
      var snackbar = SnackBar(
        duration: Duration(seconds: 3),
        content: Container(
          height: 40,
          child: Row(
            children: <Widget>[
              Text('please grant permission',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                      fontWeight: FontWeight.w100)),
              Padding(
                padding: EdgeInsets.only(
                    left: MediaQuery.of(context).size.width / 3),
                child: Container(
                  child: IconButton(
                      onPressed: () {
                        _key_scaffold.currentState.removeCurrentSnackBar();
                        get_imei();
                      },
                      icon: Icon(Icons.refresh)),
                ),
              )
            ],
          ),
        ),
      );
      _key_scaffold.currentState.showSnackBar(snackbar);
    }
  }

  int get_length_checked() {
    return checked_list
        .where((element) => !element.view && element.is_open)
        .length;
  }

  Future refresh_information(Race race) async {
    racer_r = null;
    await DBProvider.db.Racer_list_from(event_l, race).then((value) {
      racer_r = value;
    });
    /* old>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    await DBProvider.db.get_information_race(race, event_l).then((value) async {
      await get_informa().then((value) {
        trackingInformation = lst_tracking.firstWhere(
            (element) => element.reference == race.reference,
            orElse: () => null);
      });
      await DBProvider.db.Race_list_refresh(race, event_l).then((value) {
        race = value[0];
      });

      await DBProvider.db
          .DetailsTracking_limit1(race, event_l)
          .then((de) async {
        if (de != null) {
          detailsTracking = de;

          print('detailsTracking.>>>> ${detailsTracking}');
        }
      });
      await delegate_navigator(race);
    });

     */
    await get_informa(race).then((value) async {
      trackingInformation = lst_tracking.firstWhere(
          (element) => element.reference == race.reference,
          orElse: () => null);
      await delegate_navigator(race);
    });
  }

  Future delegate_navigator(Race race) async {
    if (check_is_null_or_guned(trackingInformation)) return;
    if (check_is_finished(trackingInformation, race, racer_r)) return;

    if (racer_list.length == 0 || racer_r == null) {
      Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (context) => Login(event_l, race, null)))
          .then((value) async {
        await get_manager();
        await get_racer();
        await get_checked_list();
      });
      await toast_and_change_loading(null, false);
      return;
    } else if (racer_r != null &&
        detailsTracking != null &&
        detailsTracking.lst_trackers.length > 0 &&
        detailsTracking.lst_trackers
                .where((element) => element.bib == racer_r.bib)
                .length >
            0) {
      await toast_and_change_loading(
          ' sorry,You are already a participant.', false);
      return;
    } else {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (context) => CheckMeInScreen(event_l, race, false)),
          (Route<dynamic> route) => false).then((value) async {
        await get_racer();
      });
      await toast_and_change_loading(null, false);
      return;
    }
  }

  @override
  void initState() {
//    racer_list.add(racer);
    _scrollController = ScrollController();

    get_manager();
    get_course_all();
    get_imei();
    get_racer();

//    get_informa();
    super.initState();
  }

  Future<void> get_informa(Race race) async {
    await DBProvider.db.TrackingInformation_list().then((value) {
      lst_tracking = value;
    });
    await DBProvider.db.DetailsTracking_limit1(race, event_l).then((de) async {
      if (de != null) {
        detailsTracking = de;

        print('detailsTracking.>>>> ${detailsTracking}');
      }
    });
//    get_manager();
    await get_racer();
  }

  bool is_dispose = false;

  var doubleRE = RegExp(r"-?(?:\d*\.)?\d+(?:[eE][+-]?\d+)?");

  Drawer get_drawer() {
    child:
    return Drawer(
      child: Container(
        color: col.col_from_argb,
        child: ListView(
          scrollDirection: Axis.vertical,
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.only(top: 40),
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                height: 100,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white24),
                child: CircleAvatar(
                  backgroundColor: Colors.white,
                  child: Container(
                    width: 80,
                    height: 50,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('Images/LaLigaLogo.png'),
                            fit: BoxFit.fill)),
                  ),
                ),
              ),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: checked_list.map((e) {
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    child: InkWell(
                      onTap: () async {
                        Event_l event;
                        Race race;
                        await DBProvider.db.Events_list().then((value) {
                          event = value.firstWhere(
                              (element) => element.reference == e.ref_event,
                              orElse: () => null);
//                          print('event>>>> ${event}');
                        });
                        if (event != null) {
                          await DBProvider.db
                              .Race_list_from_event(event)
                              .then((value) {
                            race = value.firstWhere(
                                (element) => element.reference == e.reference,
                                orElse: () => null);
                          });
                          print('race>>>> ${race}');
                        }
                        print('here>>>>');
                        if (event != null &&
                            race != null &&
                            racer_list.firstWhere((element) =>
                                    element.ref_event == event.reference &&
                                    element.ref_race == race.reference) !=
                                null) {
                          print('here>>>ddddd>');
                          e.view = true;

                          await DBProvider.db.update_checked(e);
                          Navigator.of(context).pop();
                          Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_) =>
                                          CheckMeInScreen(event, race, false)))
                              .then((value) {
                            get_checked_list();
                          });
                        }
                      },
                      child: Container(
                        height: 60,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: e.view ? Colors.white54 : Colors.blueGrey,
                        ),
                        child: Center(
                          child: Text(
                            '${e.name_event} - ${e.name}',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: font_size_body,
                                fontWeight: FontWeight.w100),
                          ),
                        ),
                      ),
                    ),
                  ),
                );
              }).toList(),
            )
          ],
        ),
      ),
    );
  }

  @override
  void deactivate() {
    is_dispose = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key_scaffold,
//      floatingActionButton: FloatingActionButton(
//        onPressed: () async {
//      await  DBProvider.db.  get_trackings_races_automatic(event_l);
//        },
//      ),
      endDrawer: get_drawer(),
      appBar: PreferredSize(
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.only(top: 13),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      child: Text(
                        "${event_l.name}",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(2),
                      child: Container(
                        child: Text(
                          "Races",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.only(top: 15, right: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      child: InkWell(
                        onTap: () {
                          if (manager_list.length == 0) {
                            Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) => LoginManagerFromRacer(
                                            event_l, null)))
                                .then((value) => get_manager());
                          } else if (manager_list.length > 0) {
                            Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (_) =>
                                            RacesScreenManager(event_l)))
                                .then((value) => get_manager());
                          }
                        },
                        child: Container(
                          width: 20,
                          height: 25,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: manager_list.length == 0
                                      ? AssetImage('Images/profile.png')
                                      : AssetImage('Images/greenProfile.png'),
                                  fit: BoxFit.fill)),
                        ),
                      ),
                    ),
                    Container(
                      child: racer_list.length != 0 && checked_list.length != 0
                          ? InkWell(
                              onTap: () {
                                _key_scaffold.currentState.openEndDrawer();
//                                        _showMyDialog();
                              },
                              child: Stack(
                                alignment: Alignment.topRight,
                                children: <Widget>[
                                  Padding(
                                    padding: EdgeInsets.only(
                                        right: get_length_checked() > 0 ? 8 : 0,
                                        top: get_length_checked() > 0 ? 5 : 0),
                                    child: Container(
                                      width: 25,
                                      height: 25,
                                      decoration: BoxDecoration(
                                          image: DecorationImage(
                                              image: AssetImage(
                                                  'Images/redChecked.png'),
                                              fit: BoxFit.fill)),
                                    ),
                                  ),
                                  get_length_checked() > 0
                                      ? Container(
                                          decoration: BoxDecoration(
                                              shape: BoxShape.circle,
                                              color: Colors.orangeAccent,
                                              border: Border.all(
                                                  color: Colors.white,
                                                  width: 1)),
                                          child: Padding(
                                            padding: const EdgeInsets.all(2.0),
                                            child: Text(
                                              get_length_checked().toString(),
                                              style: TextStyle(
                                                  color: Colors.white),
                                              textAlign: TextAlign.center,
                                            ),
                                          ))
                                      : Container()
                                ],
                              ),
                            )
                          : Container(
                              width: 25,
                              height: 25,
                              decoration: BoxDecoration(
                                  image: DecorationImage(
                                      image:
                                          AssetImage('Images/greyChecked.png'),
                                      fit: BoxFit.fill)),
                            ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: InkWell(
                child: Padding(
                  padding: EdgeInsets.only(top: 60),
                  child: Container(
                    width: 80,
                    height: 50,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('Images/LaLigaLogo.png'),
                            fit: BoxFit.fill)),
                  ),
                ),
                onTap: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (_) => EventsScreenRacer(true)),
                      (route) => false);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 120),
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.blue,
                height: 2,
              ),
            ),
          ],
        ),
        preferredSize: Size.fromHeight(100),
      ),
      backgroundColor: Colors.black,
      body: Stack(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              !isloading && imei != null
                  ? Expanded(child: races_ListView())
                  : Center(
                      child: SpinKitThreeBounce(
                      size: 17,
                      color: Colors.blue,
                    )),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(left: 10, right: 10, bottom: 40),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Colors.blue,
                ),
                width: MediaQuery.of(context).size.width,
                height: 40,
                child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => LiveResultsRacer(event_l)));
                  },
                  child: Center(
                    child: Text(
                      "Liveresultat",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: font_size_body,
                          fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(2.0),
            child: Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                  },
                  child: Text(
                    "Back",
                    style: TextStyle(
                        color: Colors.blue,
                        fontSize: font_size_body,
                        fontWeight: FontWeight.w400),
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  bool check_is_null_or_guned(TrackingInformation trackingInformation) {
    if (trackingInformation == null) {
      toast_and_change_loading('sorry try again', false);
      print('herett>>> ${trackingInformation}');
      return true;
    }
    if (trackingInformation != null &&
        trackingInformation.status == Status.guned.asString()) {
      toast_and_change_loading('sorry the race is guned', false);

      return true;
    }
    return false;
  }

  bool check_is_finished(
      TrackingInformation trackingInformation, Race race, Racer racer) {
    if (trackingInformation != null &&
        trackingInformation.status == Status.finished.asString() &&
        racer == null) {
      toast_and_change_loading('sorry the race is finished', false);

      return true;
    } else if (trackingInformation != null &&
        trackingInformation.status == Status.finished.asString() &&
        racer != null) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (context) => CheckMeInScreen(event_l, race, false)),
          (Route<dynamic> route) => false).then((value) async {
        await get_racer();
      });

      return true;
    }
    return false;
  }

  Future<Race> refresh_race(Race race) async {
    await DBProvider.db.Race_list_refresh(race, event_l).then((value) {
      if (value.length > 0) {
        setState(() {
          race = value[0];
        });
        return race;
      }
      return race;
    });
  }

  bool isloading = false;
  List<Course> list_course = new List();

  Future<void> get_course_all() async {
    await DBProvider.db.Course_list_all(event_l).then((value) {
      if (value.length > 0) {
        list_course = value;
      } else {
        list_course = [];
      }
    });
  }

  FutureBuilder<List<Race>> races_ListView() {
    return FutureBuilder(
      initialData: [],
      future: DBProvider.db.Race_list_from_event(event_l),
      builder: (BuildContext context, AsyncSnapshot<List<Race>> snapshot) {
        if (snapshot.hasData && snapshot.data.length > 0) {
          return ListView.builder(
              itemCount: snapshot.data.length,
              controller: _scrollController,
              itemBuilder: (BuildContext context, int index) {
                snapshot.data
                    .sort((a, b) => a.startTimeMs.compareTo(b.startTimeMs));
                Race race = snapshot.data[index];
                Course course = list_course.firstWhere(
                    (element) => element.id == race.courseId,
                    orElse: () => null);
                print("race>>>> ${race.reference}");
                print("snapshot.data.length>>>> ${snapshot.data.length}");
//
////                print(">>>>>>>>>>>>>>>>ee>> ${race.reference}");
//                print(">>>>>>>>>>>>>>>>>>P ${race.jso['splitNames']}");

                return Padding(
                    padding: EdgeInsets.only(
                        left: 3, right: 3, top: index == 0 ? 15 : 2),
                    child: Container(
                      decoration: BoxDecoration(
                          color: col.col_from_argb,
                          border: Border(
                              bottom: index != snapshot.data.length - 1
                                  ? BorderSide(
                                      color: Colors.blueGrey[50], width: 0.3)
                                  : BorderSide(),
                              top: index == 0
                                  ? BorderSide(
                                      color: Colors.blueGrey[50], width: 0.3)
                                  : BorderSide())),
                      height: 75,
                      child: InkWell(
                        onTap: () async {
//                          await get_informa().then((value) {
//                            trackingInformation = lst_tracking.firstWhere(
//                                    (element) =>
//                                element.reference == race.reference,
//                                orElse: () => null);
//                          });

                          chang_loading(true);
                          await refresh_information(race);
                          try {} catch (ex) {
                            print('exppppppp>>> ${ex}');
                            await toast_and_change_loading(null, false);
                            return;
                          }
                        },
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Expanded(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Container(
//                                          width: 275,
                                      child: Text(
                                        race.name,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: font_size_body,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ),
//                                      Padding(
//                                        padding:
//                                            const EdgeInsets.only(left: 8.0),
//                                        child: ,
//                                      ),
                                  Padding(
                                    padding: const EdgeInsets.only(left: 8.0),
                                    child: Row(
                                      children: <Widget>[
                                        Text(
                                          "Start ${showdate(race.startTimeMs).toString().substring(10, 16)}, ",
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: font_size_body,
                                              fontWeight: FontWeight.w400),
                                        ),
                                        course != null
                                            ? Padding(
                                                padding: EdgeInsets.only(
                                                    left: index == 0 ? 0 : 2),
                                                child: Container(
                                                  child: Text(
                                                    '${removeDecimalZeroFormat(double.parse((course.distance / 1000).toStringAsFixed(1)))} Km',
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize:
                                                            font_size_body,
                                                        fontWeight:
                                                            FontWeight.w400),
                                                  ),
                                                ),
                                              )
                                            : Container()
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(right: 16),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.white,
                              ),
                            )
                          ],
                        ),
                      ),
                    ));
              });
        } else if (snapshot == null) {
          return Center(
              child: SpinKitThreeBounce(
            size: 17,
            color: Colors.blue,
          ));
        } else if (snapshot.data.length == 0) {
          return Center(
              child: Text(
            "No Races Here",
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.w300),
          ));
        }

        return Center(
          child: CircularProgressIndicator(
            backgroundColor: Colors.green,
          ),
        );
      },
    );
  }

  Future<void> toast_and_change_loading(String msg, bool state) async {
    if (msg != null) {
      Toast.show('${msg} ', context,
          backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
    }
    chang_loading(state);
    return;
  }

  void chang_loading(bool va) {
    setState(() {
      isloading = va;
    });
  }

  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('vill du logga ut?'),
          actions: <Widget>[
            FlatButton(
              child: Text('Annullera'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: () async {
                await DBProvider.db.Racer_list().then((value) {
                  if (value.length > 0) {
                    DBProvider.db.deleteAllRacer().then((value) {
                      setState(() {
                        racer_list = [];
                      });
                      Navigator.of(context).pushNamedAndRemoveUntil(
                          '/EventsScreen', (Route<dynamic> route) => false);
                      return;
                    });
                  }
                });
                Navigator.of(context).pushNamedAndRemoveUntil(
                    '/EventsScreen', (Route<dynamic> route) => false);
              },
            ),
          ],
        );
      },
    );
  }
}

String removeDecimalZeroFormat(double n) {
  return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 1);
}
