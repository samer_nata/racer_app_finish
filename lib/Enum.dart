import 'package:racer_app_finish/Models/TrackingWithInformation.dart';

enum  Status{
upcoming,
open,
waiting_for_gun,
guned,
finished

}

extension EnumX on Object {
  String asString() => toString().split('.').last;
}
  bool  check_can_check_in(TrackingInformation trackingInformation) {
  return (trackingInformation.status == Status.open.asString() ||
      trackingInformation.status ==
          Status.waiting_for_gun.asString() &&
          (showdate(trackingInformation.openingTimeMs)
              .difference(DateTime.now())
              .inMinutes <=
              30 &&
              DateTime.now()
                  .difference(
                  showdate(trackingInformation.openingTimeMs))
                  .inDays ==
                  0));
}
DateTime showdate(DateTime dateTime){
return  dateTime.subtract(Duration(hours: 1));
}

