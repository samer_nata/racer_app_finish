import 'package:flutter/material.dart';
import 'package:racer_app_finish/Screen/EventsScreenRacer.dart';
//import 'package:wakelock/wakelock.dart';


import 'Checking/CheckMeInScreen.dart';
import 'Database.dart';
import 'Screen/RacesScreenRacer.dart';
import 'SplashScreen.dart';

void main() {
  runApp(new MyApp());
  DBProvider.db.database;
  DBProvider.db.open();

}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  var key = GlobalKey();

  @override
  void initState() {

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      key: key,
      debugShowCheckedModeBanner: false,
      routes: {

        '/SplashScreen': (context) => SplashScreen(),
        //'/CheckedScreen' : (context) => CheckedScreen()
      },
      initialRoute: '/',
      home: SplashScreen(),
    );
  }

  @override
  void dispose() {

    super.dispose();
  }
}
