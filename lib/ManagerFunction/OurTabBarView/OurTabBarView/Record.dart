import 'dart:async';
import 'dart:convert';
import 'dart:core';
import 'dart:io';

import 'package:animated_widgets/widgets/opacity_animated.dart';
import 'package:animated_widgets/widgets/translation_animated.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

import 'package:http/http.dart' as http;
import 'package:racer_app_finish/Enum.dart';
import 'package:racer_app_finish/ManagerFunction/OurTabBarView/ScreenManager/ShowInformationRacer.dart';
import 'package:racer_app_finish/Models/Course.dart';
import 'package:racer_app_finish/Models/Coursepoints.dart';
import 'package:racer_app_finish/Models/Details_Tracking.dart';
import 'package:racer_app_finish/Models/Event.dart';
import 'package:racer_app_finish/Models/Manager.dart';
import 'package:racer_app_finish/Models/Race.dart';
import 'package:racer_app_finish/Models/Racer.dart';
import 'package:racer_app_finish/Models/Timings.dart';
import 'package:racer_app_finish/Models/Trackers.dart';
import 'package:racer_app_finish/Models/TrackingWithInformation.dart';

import 'package:toast/toast.dart';

import '../../../Appearance.dart';
import '../../../Database.dart';
import '../../../Square.dart';
import '../ScreenManager/EndRacer.dart';
import '../ScreenManager/Unidentified.dart';

class Record extends StatefulWidget {
  Event_l event_l;

  Race race;

  Record(this.event_l, this.race);

  @override
  _RecordState createState() => _RecordState(event_l, race);
}

class _RecordState extends State<Record> {
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isAll = false;
  Race race;

//  Timer _timer;
  Event_l event_l;
  bool is_dispose = false;
  List<Manager> manager_list = new List();

  List<Coursepoints> list_search = new List();
  List<Coursepoints> list_search_temp = new List();

  TrackingInformation trackingInformation_public;
  List<TrackingInformation> list_tracking = new List();

  // List<Race> lst_race = new List();
  List<Coursepoints> lapsList = new List();
  List<Coursepoints> lapsList_for_undifind = new List();
  List<Coursepoints> lapsList_for_manual = new List();
  List list_send_to_server = new List();
  SpinKitThreeBounce spinKitThreeBounce = SpinKitThreeBounce(
    color: Colors.blue,
    size: 17,
  );
  bool cpmplite_load = false;

  // DetailsTracking _detailsTracking_public;
  DetailsTracking detailsTracking_races;

//  List<Course>list_course=new List();
  List<Square> list_square_orange = new List<Square>();
  List<Square> list_square_blue = new List<Square>();
  List<Square> list_square_grey = new List<Square>();
  List<Square> list_square_grey_io = new List<Square>();
  Course course;

  List<int> list_bib_of_square_can_undo = new List<int>();

//  List<Square> orang_list = new List<Square>();

  // Coursepoints value_filter = new Coursepoints(lapIndex: -9, label: 'Show all');

  bool first_load = true;

  Future<void> get_update_in_five_seconds() async {
try{
  await request_get_tracking();
  if (list_bib_of_square_can_undo.length == 0) {
    await load_square_list_automatic();
  }
  print("invoked>>>>");

//    await DBProvider.db
//        .TrackingInformation_list_from_race(event_l, race)
//        .then((value) {
//      trackingInformation_public = value[0];
//    });
  if (list_tracking.length == 0) {
    await DBProvider.db
        .TrackingInformation_list_from_event(event_l)
        .then((value) {
      if (value.length > 0) {
        list_tracking = value;
      }
    });

  }
  delay_s();
}
catch(ex){
  delay_s();
}

  }
  Future<void>delay_s(){
    Future.delayed(Duration(seconds: 5), () async {
      if (!is_dispose) {
        try {
          await get_update_in_five_seconds();
        } catch (ex) {
          toast(context, ex.toString());
        }
      }
//      print('afterfive> ${course_point.toString()}');
    });
  }

  bool want_refresh = false;

  void refresh_want_refresh(bool state) {
    want_refresh = state;
  }

 Future refresh_details(DetailsTracking value) {
   if(value!=null){
     detailsTracking_races = value;
     detailsTracking_races.get_timigs();
     detailsTracking_races.get_trackers();
   }


  }

  Future<void> load_square_list_automatic() async {
    await DBProvider.db
        .get_trackings_races_automatic(event_l, trackers)
        .then((value) async {




print("ttttttt>>> ${(list_bib_of_square_can_undo.length == 0 && want_refresh)}");
      if (first_request) {
        first_request = false;
        print('first_request>>>>>> ${value}');
        try {
          refresh_details(value);
          await load_square_list();
        } catch (ex) {
          print("ex>>>>> ${ex}");
        }
        await load_square_list();
        print('valll>>>>>> ${value}');
      } else if ((value != null &&
              (value.lst_trackers.any((element) =>
                      !detailsTracking_races.lst_trackers.contains(element)) ||
                  value.lst_timings.any((element) =>
                      !detailsTracking_races.lst_timings.contains(element))) &&
              list_bib_of_square_can_undo.length == 0) ||
          (list_bib_of_square_can_undo.length == 0 && want_refresh)) {

        //    detailsTracking_races = value;
        refresh_details(value);
        await add_square_automatic();
      }
//      else
//        if (value != null &&  (value.lst_timings.length > 0||value.lst_trackers.length > 0) ) {
////        detailsTracking_races = value;
//
//      }
    });
  }

  Future add_square_automatic() async {

    refresh_want_refresh(false);
    List<Square> list_square_blue_temp = new List();
    List<Square> list_square_orange_temp = new List();
    List<Tracker> lst_trackersfinish = detailsTracking_races.lst_trackers
        .where((element) => element.status == "finished")
        .toList(growable: true);
    List<Timing> lst_timingsfinish = detailsTracking_races.lst_timings
        .where((element) =>
            element.courspoint.toLowerCase() ==
            course.coursepoints.last.reference.toLowerCase())
        .toList(growable: true);
    lst_trackersfinish.forEach((el) {
      list_square_orange.removeWhere((element) =>
          element.bib == el.bib && element.tracking == el.tracking);
      list_square_blue.removeWhere((element) =>
          element.bib == el.bib && element.tracking == el.tracking);
    });
    lst_timingsfinish.forEach((el) {
      list_square_orange.removeWhere((element) =>
          element.bib == el.bib && element.tracking == el.tracking);
      list_square_blue.removeWhere((element) =>
          element.bib == el.bib && element.tracking == el.tracking);
    });

    List<Timing> lst_timings = detailsTracking_races.lst_timings
        .where((element) =>
            lst_trackersfinish
                .where((eltr) =>
                    eltr.bib == element.bib &&
                    eltr.tracking == element.tracking)
                .length ==
            0)
        .toList(growable: true);

    lst_timings.sort((a, b) => get_coursepoint(a.courspoint, course)
        .compareTo(get_coursepoint(b.courspoint, course)));
    List<int> list_bib = new List();
    lst_timings.forEach((element) {
      if (!list_bib.contains(element.bib)) {
        list_bib.add(element.bib);
      }
    });

    List<Tracker> lst_trackers = detailsTracking_races.lst_trackers
        .where((element) =>
            !list_bib.contains(element.bib) && element.status != "finished")
        .toList(growable: true);

    if (lst_trackers.length > 0) {
      list_bib.addAll(lst_trackers.map((e) => e.bib).toList());
    }


    //   List<int> list_bib = new List();
    lst_timings.forEach((element) {
      if (!list_bib.contains(element.bib)) {
        list_bib.add(element.bib);
      }
    });

    list_bib.forEach((element) {
      var list =
          lst_timings.where((el) => el.bib == element).toList(growable: true);
      list.sort((a, b) => get_coursepoint(a.courspoint, course)
          .compareTo(get_coursepoint(b.courspoint, course)));
      String tracking;
      if (list.length > 0) {
        tracking = list
            .firstWhere((el) => el.bib == element, orElse: () => null)
            .tracking;
      }

      Square square = new Square(
        course: course,
        timerMaxSeconds: 10,fun_want_refresh: (){
        refresh_want_refresh(true);
      },
        fun_delete_from_list_undo: () {
          remove_from_list_undo(element);
        },
        fun_refresh_state: () {
          setState(() {});
        },
        tracking: tracking,
//        detailsTracking: detailsTracking_races,
        bib: element,
        lst_timings:
            list.where((el) => el.bib == element).toList(growable: true),
        is_start_from_page: false,
//        time_in_mill:
//            lst_timings.lastWhere((el) => el.bib == element).temp_time != null
//                ? lst_timings.lastWhere((el) => el.bib == element).temp_time
//                : null,
      );

      square.lst_timings.sort((a, b) => get_coursepoint(a.courspoint, course)
          .compareTo(get_coursepoint(b.courspoint, course)));

      var last_cours = square.lst_timings.lastWhere(
              (element) => element != null && element.tracking == tracking,
          orElse: () => null);

      if (last_cours!=null&& last_cours.courspoint == course.coursepoints.last.reference
//          &&
//          last_cours.source == 'manual'
          ) {
        list_square_blue.removeWhere((el) => el.bib == square.bib);
        list_square_orange.removeWhere((el) => el.bib == square.bib);
        return;
      } else if (last_cours == null) {
        String next_coursepoint;
        print('object2>>>> ${square.bib}');
        next_coursepoint = course.coursepoints
            .firstWhere((element) => element != null)
            .reference;

        Timing timing = new Timing(
            bib: element, courspoint: next_coursepoint, tracking: tracking);
        square.lst_timings.add(timing);
        list_square_blue_temp.add(square);
      }
      else {
        //check2
        var last_coursepoint =
            get_coursepoint(square.lst_timings.last.courspoint, course);
        String next_coursepoint;
        /*
        if (square.lst_timings.last.source == 'manual') {
          next_coursepoint = course.coursepoints
              .firstWhere(
                  (element) => element.lapIndex > last_coursepoint.lapIndex)
              .reference;
        } else {
          next_coursepoint = course.coursepoints
              .firstWhere(
                  (element) => element.lapIndex == last_coursepoint.lapIndex)
              .reference;
        }

         */
        next_coursepoint = course.coursepoints
            .firstWhere(
                (element) => element.lapIndex > last_coursepoint.lapIndex)
            .reference;

        Timing timing = new Timing(bib: element, courspoint: next_coursepoint);
        square.lst_timings.add(timing);
      }
      if (square.lst_timings.last.courspoint ==
          course.coursepoints.last.reference) {
        list_square_orange_temp.add(square);
      } else if (square.lst_timings.last.courspoint !=
          course.coursepoints.last.reference) {
        square.km = get_coursepoint(square.lst_timings.last.courspoint, course)
            .distance;

        list_square_blue_temp.add(square);
        //

      }
    });
/*
* list_square_blue
        .where((element) =>
    element.currentSeconds != 0 || element.is_start_from_page)
        .length ==
        0 &&
        list_square_orange
            .where((element) =>
        element.currentSeconds != 0 || element.is_start_from_page)
            .length ==
            0*/
    if (2 > 1) {
      list_square_blue_temp.forEach((element) {
        element.fun_delete = () {
          list_square_blue.removeWhere((el) => el.bib == element.bib);
        };
        var old_square = list_square_blue
            .firstWhere((el) => el.bib == element.bib, orElse: () => null);
        if (old_square == null) {
          list_square_blue.add(element);
        } else if (old_square != null &&
            old_square.lst_timings.length != element.lst_timings.length) {
          list_square_blue.removeWhere((el) => el.bib == element.bib);

          list_square_blue.add(element);
        }
      });
      list_square_orange_temp.forEach((element) {
        element.fun_delete = () {
          list_square_orange.removeWhere((el) => el.bib == element.bib);
        };
        var old_square = list_square_orange
            .firstWhere((el) => el.bib == element.bib, orElse: () => null);
        if (old_square == null) {
          list_square_orange.add(element);
        } else if (old_square != null &&
            old_square.lst_timings.length != element.lst_timings.length) {
          list_square_orange.removeWhere((el) => el.bib == element.bib);
          list_square_orange.add(element);
        }
        list_square_blue.removeWhere((el) => el.bib == element.bib);
      });
//      await DBProvider.db.update_detailstracking(detal);
    }
    if (!is_dispose) {
      setState(() {});
    }
  }

  Future<void> fill_search_list(List<Coursepoints> list) {
    setState(() {
      list_search = list;
    });
    list_search.insert(0, new Coursepoints(lapIndex: -9, label: 'Show all'));
  }

  Future<void> load_square_list() async {
    list_square_orange = [];
    list_square_blue = [];

    list_square_grey = [];
list_square_grey_io=[];
    await add_square_first_load();

    list_square_grey.add(new Square(txt: "OI"));
    setState(() {});
  }

  Future add_square_first_load() async {
    if(detailsTracking_races==null){
      first_request=true;
    await  load_square_list_automatic();
    return;
    }
    List<Tracker> lst_trackersfinish = detailsTracking_races.lst_trackers
        .where((element) => element.status == "finished")
        .toList(growable: true);
    List<Timing> lst_timings = detailsTracking_races.lst_timings
        .where((element) =>
            lst_trackersfinish
                .where((eltr) =>
                    eltr.bib == element.bib &&
                    eltr.tracking == element.tracking)
                .length ==
            0)
        .toList(growable: true);

    lst_timings.sort((a, b) => get_coursepoint(a.courspoint, course)
        .compareTo(get_coursepoint(b.courspoint, course)));
    List<int> list_bib = new List();
    lst_timings.forEach((element) {
      if (!list_bib.contains(element.bib)) {
        list_bib.add(element.bib);
      }
    });
    List<Tracker> lst_trackers = detailsTracking_races.lst_trackers
        .where((element) =>
            !list_bib.contains(element.bib) && element.status != "finished")
        .toList(growable: true);
    if (lst_trackers.length > 0) {
      list_bib.addAll(lst_trackers.map((e) => e.bib).toList());
    }
    list_bib.forEach((element) {
      var list_timings_s =
          lst_timings.where((el) => el.bib == element).toList(growable: true);
      list_timings_s.sort((a, b) => get_coursepoint(a.courspoint, course)
          .compareTo(get_coursepoint(b.courspoint, course)));
      String tracking;
      if (list_timings_s.length > 0) {
        tracking = list_timings_s
            .lastWhere((el) => el.bib == element, orElse: () => null)
            .tracking;
      } else {
        tracking = lst_trackers
            .lastWhere((el) => el.bib == element, orElse: () => null)
            .tracking;
      }
      Square square = new Square(
        course: course,
        timerMaxSeconds: 10,fun_want_refresh: (){
        refresh_want_refresh(true);
      },
        fun_delete_from_list_undo: () {
          remove_from_list_undo(element);
        },
        fun_refresh_state: () {
          setState(() {});
        },
        tracking: tracking,
        bib: element,
        lst_timings: list_timings_s
            .where((el) => el.bib == element && el.tracking == tracking)
            .toList(growable: true),
        is_start_from_page: false,
//        time_in_mill:
//            lst_timings.lastWhere((el) => el.bib == element).temp_time != null
//                ? lst_timings.lastWhere((el) => el.bib == element).temp_time
//                : null,
      );
      square.lst_timings.sort((a, b) => get_coursepoint(a.courspoint, course)
          .compareTo(get_coursepoint(b.courspoint, course)));
      var last_cours = square.lst_timings.lastWhere(
          (element) => element != null && element.tracking == tracking,
          orElse: () => null);
      if (last_cours != null &&
          last_cours.courspoint == course.coursepoints.last.reference) {
        return;
      } else if (last_cours == null) {
        String next_coursepoint;

        next_coursepoint = course.coursepoints
            .firstWhere((element) => element != null)
            .reference;

        Timing timing = new Timing(
            bib: element, courspoint: next_coursepoint, tracking: tracking);
        square.lst_timings.add(timing);
        delegate_add(square, course);
      } else {
        var last_coursepoint =
            get_coursepoint(square.lst_timings.last.courspoint, course);
        String next_coursepoint;

        next_coursepoint = course.coursepoints
            .firstWhere(
                (element) => element.lapIndex > last_coursepoint.lapIndex)
            .reference;

        Timing timing = new Timing(
            bib: element, courspoint: next_coursepoint, tracking: tracking);
        square.lst_timings.add(timing);
        delegate_add(square, course);
      }
    });
  }

  void delegate_add(Square square, Course course) {
    if (square.lst_timings.last.courspoint ==
        course.coursepoints.last.reference) {
      square.fun_delete = () {
        list_square_orange.removeWhere((el) => el.bib == square.bib);
      };
      list_square_orange.add(square);
    } else if (square.lst_timings.last.courspoint !=
        course.coursepoints.last.reference) {
      square.fun_delete = () {
        list_square_blue.removeWhere((el) => el.bib == square.bib);
      };
      square.km =
          get_coursepoint(square.lst_timings.last.courspoint, course).distance;

      list_square_blue.add(square);
    }
  }

  Coursepoints get_coursepoint(String label, Course course) =>
      course.coursepoints.firstWhere(
          (element) => element.reference.toUpperCase() == label.toUpperCase());

  Future<void> stop_all() async {
    list_square_orange.forEach((element) async {
      if (element.is_start_from_page && !element.is_finish) {
        await element.cancel();
      }
    });
    list_square_blue.forEach((element) async {
      if (element.is_start_from_page && !element.is_finish) {
        await element.cancel();
      }
    });
  }

  Future start_all() async {
    list_square_orange.forEach((element) async {
//      print("elbeforestart>>> ${element.bib}");
      if (element.is_start_from_page && !element.is_start) {
//        print("elafterstart>>> ${element.bib}");

        await element.start();
//        setState(() {});
      }
    });
    list_square_blue.forEach((element) async {
      if (element.is_start_from_page && !element.is_start) {
        await element.start();
//        setState(() {});
      }
    });
//    setState(() {});
  }

  @override
  void dispose() {
    is_dispose = true;

    stop_all();
    list_square_blue.where((element) => false).forEach((element) {
      element.is_finish_page = true;
    });
    list_square_orange.where((element) => false).forEach((element) {
      element.is_finish_page = true;
    });
    //_detailsTracking_public=null;
    print('dispose>>>M');
//    list_square_blue = [];
//    list_square_orange = [];

    super.dispose();
  } //  Race race;

  bool isLoadingSubmitted = false;

//  int indexLoop;

  TextEditingController cont_time = new TextEditingController();

  _RecordState(this.event_l, this.race);

  Future<void> getLapsTimes() async {
    lapsList = [];
    await DBProvider.db.Course_list_all(event_l).then((value) {
      if (value.length > 0) {
        setState(() {
//          print("CCCCC>>>>> ${value}");
          lapsList = value[0].coursepoints;
          list_search = value[0].coursepoints;
          list_search_temp = value[0].coursepoints;
          lapsList.insert(0, new Coursepoints(lapIndex: -9, label: 'Show all'));
//          print("lapsList>>> ${lapsList}");
//          list_course=value;
        });
      }
    });

//    lapsList=event_l.;
//    print(race.jso['splitNames']);
  }

  Future<void> get_detalstracking_public() async {
//    await DBProvider.db.DetailsTracking_limit1(race, event_l).then((value) {
//      _detailsTracking_public = value;
//    });
  }

  Future<void> get_course() async {
//    await DBProvider.db
//        .Course_list_from_id(_detailsTracking_public.id)
//        .then((value) {
//      course = value[0];
//    });
    await DBProvider.db.Course_list(race).then((value) {
      course = value[0];
    });
  }

  @override
  void initState() {
    print('initState>>>');
    list_square_grey_io = [];
    load_and_refresh_from_server();

    super.initState();
  }

  Future<void> request_get_tracking() async {
    await DBProvider.db.get_trackers_races(event_l).then((value) async {
      trackers = value;

    });


  }

  Map<String, dynamic> trackers = new Map();
  bool first_request = true;

  Future<void> load_and_refresh_from_server() async {
    try {
//      await DBProvider.db.get_trackings_race(race, event_l, is_update: true);

//      await load_race();
//      await start_all();

      print("trackers>> ${trackers}");

      await getLapsTimes();

//      await get_detalstracking_public();
      await get_course();
      await get_update_in_five_seconds();

      //await load_square_list();
      setState(() {
        cpmplite_load = true;
      });
    } catch (exxx) {
//      print("EEEEXX ${exxx}");
//      await load_race();
//      await start_all();
//      await get_detalstracking_public();
      await get_course();
      // await load_square_list();

      setState(() {
        cpmplite_load = true;
      });
    }
//    await start_all();
  }

//  Future<void> load_race() async {
//    await DBProvider.db.Race_list_from_event(event_l).then((value) {
//      if (!is_dispose) {
//        setState(() {
//          lst_race = value;
//        });
//      }
//    });
//  }

  Widget endRace() {
//    print("lst_race>>> ${lst_race}");
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  color: Color(0xff121214),
                  height: 60,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.keyboard_arrow_down,
                        color: Colors.blue,
                      ),
                      Text(
                        "End Race",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: font_size_body,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  color: Color(0xff121214),
                  height: 100,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Do you want to end the races? You cantundo this.",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: font_size_body,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 24.0),
                child: InkWell(
                  onTap: () async {
                    endRaceFunc();
                  },
                  child: isLoadingSubmitted
                      ? SpinKitThreeBounce(
                          color: Colors.red,
                          size: 17,
                        )
                      : Container(
                          decoration: BoxDecoration(
                              color: Colors.red,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                          width: 250,
                          height: 50,
                          child: Center(
                            child: Text(
                              "END RACES",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: font_size_body,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 24.0),
                child: InkWell(
                  onTap: () => Navigator.pop(context),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Colors.blue,
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                    width: 250,
                    height: 50,
                    child: Center(
                      child: Text(
                        "KEEP RACES ON ",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: font_size_body,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(right: 16.0, top: 16.0),
          child: Align(
            alignment: Alignment.topRight,
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: Icon(
                Icons.close,
                color: Color(0xff121214),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Future<void> finish_race(Race race) async {
//    print(race.toString());
    try {
      await http
          .post(
              'http://54.77.120.67:8080/rest/_App/trackings/${race.reference}/finish',  headers: <String, String>{
        'Content-Type': 'application/json;charset=UTF-8'
      })
          .timeout(Duration(seconds: 10))
          .then((data) async {
//      races = [];
        Map<String, dynamic> dataServer = jsonDecode(data.body);
//print("<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< ${dataServer}");
        if (dataServer["status"] == "finished") {
//
          race.is_start = false;
          TrackingInformation trackingInformation =
              new TrackingInformation.fromJSONSERVER(dataServer, race, event_l);
          await DBProvider.db
              .insert_tracking(trackingInformation)
              .then((value) {
//            DBProvider.db.update_race(race);
          });

          return;
        }
//        startRace(race,false);
      });
    } catch (ex) {
      toast(context, ex);
//      print("<<<<<<<WW ${ex}");
    }
  }

  endRaceFunc() async {
    List<Race> lst_race = await DBProvider.db.Race_list_from_event(event_l);

    List<TrackingInformation> list_tracking = new List();
    await DBProvider.db
        .TrackingInformation_list_from_event(event_l)
        .then((value) {
//      print("value>>TTT ${value}");
      list_tracking = value
          .where((element) => element.status == Status.guned.asString())
          .toList();
      print("list_tracking>>TTT ${list_tracking}");
      list_tracking.forEach((el) async {
//        print("EL>>>> ${el}");
        Race racep = lst_race.firstWhere(
            (element) =>
                element.reference == el.reference &&
                element.ref_event == event_l.reference,
            orElse: () => null);
        if (racep != null) {
          await finish_race(racep);
        }
      });
    });
    await DBProvider.db
        .TrackingInformation_list_from_event(event_l)
        .then((value) {
      list_tracking = value;
    });

    Navigator.pop(context);
    setState(() {});
  }

  /*************************controlle_orange_square***********************************/
//  int _counter_orange = 0;
//
//  bool _buttonPressed_orange = false;
//  bool _loopActive_orange = false;

  void _increaseCounterWhilePressed(
      int maxseconds, Function function, Square square) async {
    // make sure that only one loop is active
    if (square.loopActive) return;

    square.loopActive = true;

    while (square.buttonPressed) {
      // do your thing

      square.counter++;
      if (maxseconds == square.counter) {
        function.call();
      }

      // wait a bit
      await Future.delayed(Duration(seconds: 1));
    }

    square.loopActive = false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
//      floatingActionButton: FloatingActionButton(
//        onPressed: () {
////          _showMyDialogforAddManual();
//        },
//        child: Icon(Icons.add),
//        backgroundColor: Colors.blue,
//      ),
      key: _scaffoldKey,
      body: cpmplite_load
          ? SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                        top: 16.0, left: 8.0, right: 8.0, bottom: 8.0),
                    child: Container(
                      color: Color(0xff121214),
                      child: ExpansionTile(
                        title: Container(
                          // width: MediaQuery.of(context).size.width,
                          height: 40,
                          color: Color(0xff121214),
                          child: Stack(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(top: 10.0),
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.horizontal,
                                  child: Row(
                                    children: <Widget>[
                                      first_load ||
                                              list_search
                                                      .where((element) =>
                                                          element.lapIndex ==
                                                              -9 &&
                                                          element.select)
                                                      .toList()
                                                      .length >
                                                  0
                                          ? Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 4),
                                              child: Text(
                                                "Alla",
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontSize: font_size_body,
                                                    fontWeight:
                                                        FontWeight.bold),
                                              ),
                                            )
                                          : Row(
                                              children: list_search
                                                  .where((element) =>
                                                      element.lapIndex != -9)
                                                  .map((e) => e.reference ==
                                                          course.coursepoints
                                                              .last.reference
                                                      ? Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 4),
                                                          child: Text(
                                                            "Målgång ${removeDecimalZeroFormat(double.parse((e.distance / 1000).toStringAsFixed(1)))} km",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize:
                                                                    font_size_body,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                        )
                                                      : Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 4),
                                                          child: Text(
                                                            "${e.reference == course.coursepoints.first.reference ? "Start" : removeDecimalZeroFormat(double.parse((e.distance / 1000).toStringAsFixed(1))) + ' Km'}",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize:
                                                                    font_size_body,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                        ))
                                                  .toList(),
                                            )
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        children: <Widget>[
                          Container(
                            width: MediaQuery.of(context).size.width / 1.1,
                            decoration: BoxDecoration(
                                color: Colors.blue,
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15))),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Column(
                                    children: lapsList
//                                        .where((element) =>
//                                            element.reference != "start")
                                        .map((e) => Padding(
                                              padding:
                                                  const EdgeInsets.only(top: 4),
                                              child: InkWell(
                                                onTap: () {},
                                                child: Stack(
                                                  children: <Widget>[
                                                    Container(
                                                      height: 35,
                                                      child: ListTile(
                                                        leading: e.reference ==
                                                                course
                                                                    .coursepoints
                                                                    .last
                                                                    .reference
                                                            ? Text(
                                                                "Målgång (${removeDecimalZeroFormat(double.parse((e.distance / 1000).toStringAsFixed(1)))}km)",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                        font_size_body,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .w400),
                                                              )
                                                            : e.reference ==
                                                                    course
                                                                        .coursepoints
                                                                        .first
                                                                        .reference
                                                                ? Text(
                                                                    "Start",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontSize:
                                                                            font_size_body,
                                                                        fontWeight:
                                                                            FontWeight.w400),
                                                                  )
                                                                : Text(
                                                                    "${e.label}  ${e.distance != null ? '(' + removeDecimalZeroFormat(double.parse((e.distance / 1000).toStringAsFixed(1))) + 'Km)' : ''} ",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontSize:
                                                                            font_size_body,
                                                                        fontWeight:
                                                                            FontWeight.w400),
                                                                  ),
                                                        trailing: Checkbox(
                                                          onChanged:
                                                              (val) async {
                                                            first_load = false;
                                                            if (!val &&
                                                                lapsList
                                                                        .where((element) =>
                                                                            element.lapIndex !=
                                                                                e.lapIndex &&
                                                                            element.select)
                                                                        .length ==
                                                                    0) {
//                                                              e.select=val;
                                                            } else {
                                                              e.select = val;
                                                            }

                                                            if (lapsList[0]
                                                                    .select ==
                                                                true) {
                                                              await fill_search_list(
                                                                  list_search_temp
                                                                      .toList());
                                                            } else {
                                                              await fill_search_list(lapsList
                                                                  .where((element) =>
                                                                      element
                                                                          .select ==
                                                                      true)
                                                                  .toList());
                                                            }
                                                            if (list_bib_of_square_can_undo
                                                                    .length ==
                                                                0) {
                                                              await load_square_list();
                                                            }
                                                          },
                                                          value: e.select,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ))
                                        .toList(),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 24.0, left: 4.0),
                                    child: InkWell(
                                      onTap: () {
                                        _scaffoldKey.currentState
                                            .showBottomSheet(
                                                (context) => endRace(),
                                                backgroundColor: Colors.black);
                                      },
                                      child: Text(
                                        "End Race",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: font_size_body,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  /**comment_temp orange*/
                  Padding(
                    padding: const EdgeInsets.only(left: 16.0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Wrap(
                          crossAxisAlignment: WrapCrossAlignment.start,
                          spacing: 10,
                          runSpacing: 10,
                          children: list_square_orange
                              .where((sq) =>
                                  list_search.firstWhere(
                                      (element) =>
                                          element.reference ==
                                          get_coursepoint(
                                                  sq.lst_timings.last
                                                      .courspoint,
                                                  course)
                                              .reference,
                                      orElse: () => null) !=
                                  null)
                              .map((e) => e.currentSeconds != 0
                                  ? TranslationAnimatedWidget.tween(
                                      enabled: e.start_animation,
                                      translationDisabled: Offset(0, 0),
                                      translationEnabled: Offset(
                                          -MediaQuery.of(context).size.width,
                                          0),
                                      child: OpacityAnimatedWidget.tween(
                                          enabled: e.start_animation,
                                          opacityDisabled: 1,
                                          opacityEnabled: 0,
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Listener(
                                              onPointerDown: (details) {
                                                e.buttonPressed = true;
                                                try {
                                                  _increaseCounterWhilePressed(
                                                      3, () async {
                                                    await stop_all()
                                                        .then((value) async {
                                                      await _showMyDialogforUndo(
                                                              e.time_in_mill,
                                                              e.bib,
                                                              list_square_orange,
                                                              e)
                                                          .then((value) {
                                                        reset_square(e);
                                                        start_all();
                                                      });
                                                    });
                                                  }, e);
                                                } catch (ex) {
                                                  toast(context, ex);
                                                  reset_square(e);
                                                }
                                              },
                                              onPointerUp: (details) async {
                                                if (e.counter == 1) {
                                                  try {
//                                      reset_orange();
                                                    await send_on_or_off(
                                                        e, context, true);
                                                  } catch (ex) {
                                                    toast(
                                                        context, ex.toString());
                                                    reset_square(e);
                                                  }
                                                }

                                                reset_square(e);
                                              },
                                              child: Container(
                                                child: Center(
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 6),
                                                    child:
                                                        SingleChildScrollView(
                                                      scrollDirection:
                                                          Axis.vertical,
                                                      child: Column(
                                                        children: <Widget>[
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .center,
                                                            children: <Widget>[
                                                              Text(
                                                                get_timing(e),
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                        17,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              ),
//                                                      Padding(
//                                                        padding:
//                                                            const EdgeInsets
//                                                                    .only(
//                                                                left: 3.0),
//                                                        child: Text(
//                                                          list_square_orange[
//                                                                  index]
//                                                              .txt,
//                                                          style: TextStyle(
//                                                              color:
//                                                                  Colors.white,
//                                                              fontSize: 11,
//                                                              fontWeight:
//                                                                  FontWeight
//                                                                      .bold),
//                                                        ),
//                                                      ),
                                                            ],
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(0.0),
                                                            child: Center(
                                                              child: Text(
                                                                e.time_in_mill !=
                                                                            null &&
                                                                        e.time_in_mill !=
                                                                            0
                                                                    ? DateTime.fromMillisecondsSinceEpoch(e
                                                                            .time_in_mill)
                                                                        .toString()
                                                                        .substring(
                                                                            10,
                                                                            19)
                                                                    : '${e.time_in_mill == 0 ? 'tap' : ''}',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                        10,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                width: 70,
                                                height: 50,
                                                decoration: BoxDecoration(
                                                    gradient: LinearGradient(
                                                        colors: <Color>[
                                                          Colors.orange,
                                                          Colors.orange,
                                                          Colors.orange,
                                                          Colors.orange,
                                                          Colors.deepOrange,
                                                          Colors.deepOrange
                                                        ],
                                                        begin:
                                                            Alignment.topLeft,
                                                        end: Alignment
                                                            .bottomRight),
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                10))),
                                              ),
                                            ),
                                          )))
                                  : Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Listener(
                                        onPointerDown: (details) {
                                          print('onPointerDown>>>ss');

                                          e.buttonPressed = true;
                                          _increaseCounterWhilePressed(3,
                                              () async {
                                            await stop_all()
                                                .then((value) async {
                                              Navigator.push(
                                                  context,
                                                  new MaterialPageRoute(
                                                      builder: (context) =>
                                                          ShowInformationRacer(
                                                              e))).then(
                                                  (value) async {
                                                reset_square(e);
//                                                get_manager();

//                                                await load_square_list_refresh()
//                                                    .then((value) async {
//
//                                                });
                                                await start_all();
                                              });
                                            });
                                          }, e);
                                        },
                                        onPointerUp: (details) async {
                                          print('onPointerUp>>>>');
//                                        await stop_all();

                                          if (e.counter == 1) {
                                            try {
//                              reset_orange();
                                              await send_on_or_off(
                                                  e, context, true);
                                            } catch (ex) {
                                              toast(context, ex.toString());
                                              reset_square(e);
                                            }
                                          }

                                          reset_square(e);
                                        },
                                        child: Container(
                                          child: Center(
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.only(top: 6),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Row(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      Text(
                                                        get_timing(e),
                                                        style: TextStyle(
                                                            color: Colors.white,
                                                            fontSize: 17,
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
//                                                        Padding(
//                                                          padding:
//                                                              const EdgeInsets
//                                                                      .only(
//                                                                  left: 3.0),
//                                                          child: Text(
//                                                            get_timing(list_square_orange[
//                                                                        index]) !=
//                                                                    null
//                                                                ? list_square_orange[
//                                                                        index]
//                                                                    .txt
//                                                                : 'ss',
//                                                            style: TextStyle(
//                                                                color: Colors
//                                                                    .white,
//                                                                fontSize: 11,
//                                                                fontWeight:
//                                                                    FontWeight
//                                                                        .bold),
//                                                          ),
//                                                        ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          width: 70,
                                          height: 50,
                                          decoration: BoxDecoration(
                                              gradient: LinearGradient(
                                                  colors: <Color>[
                                                    Colors.orange,
                                                    Colors.orange,
                                                    Colors.orange,
                                                    Colors.orange,
                                                    Colors.deepOrange,
                                                    Colors.deepOrange
                                                  ],
                                                  begin: Alignment.topLeft,
                                                  end: Alignment.bottomRight),
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10))),
                                        ),
                                      ),
                                    ))
                              .toList()),
                    ),
                  ),
                  /**comment_temp blue*/
                  Padding(
                    padding: const EdgeInsets.only(left: 16.0, top: 8.0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Wrap(
                          crossAxisAlignment: WrapCrossAlignment.start,
                          spacing: 10,
                          runSpacing: 10,
                          children: list_square_blue
                              .where((sq) =>
                                  list_search.firstWhere(
                                      (element) =>
                                          element.reference ==
                                          get_coursepoint(
                                                  sq.lst_timings.last
                                                      .courspoint,
                                                  course)
                                              .reference,
                                      orElse: () => null) !=
                                  null)
                              .map((e) => e.currentSeconds != 0
                                  ? TranslationAnimatedWidget.tween(
                                      enabled: e.start_animation,
                                      translationDisabled: Offset(0, 0),
                                      translationEnabled: Offset(
                                          -MediaQuery.of(context).size.width,
                                          0),
                                      child: OpacityAnimatedWidget.tween(
                                          enabled: e.start_animation,
                                          opacityDisabled: 1,
                                          opacityEnabled: 0,
                                          child: Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Listener(
                                              onPointerUp: (de) async {
                                                if (e.counter == 1) {
                                                  try {
                                                    // reset_blue();
                                                    await send_on_or_off(
                                                        e, context, false);
                                                  } catch (ex) {
                                                    toast(
                                                        context, ex.toString());
                                                    reset_square(e);
                                                  }
                                                }
                                                reset_square(e);
                                              },
                                              onPointerDown: (details) {
                                                e.buttonPressed = true;
                                                try {
                                                  _increaseCounterWhilePressed(
                                                      3, () async {
                                                    await stop_all()
                                                        .then((value) async {
                                                      await _showMyDialogforUndo(
                                                              e.time_in_mill,
                                                              e.bib,
                                                              list_square_blue,
                                                              e)
                                                          .then((value) async {
                                                        reset_square(e);
                                                        await start_all();
                                                      });
                                                    });
                                                  }, e);
                                                } catch (ex) {
                                                  toast(context, ex);
                                                  reset_square(e);
                                                }

                                                /***************code for thread and update******************/
                                              },
                                              child: Container(
                                                child: Center(
                                                  child: Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 6),
                                                    child:
                                                        SingleChildScrollView(
                                                      scrollDirection:
                                                          Axis.horizontal,
                                                      child: Column(
                                                        children: <Widget>[
                                                          SingleChildScrollView(
                                                            child: Row(
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .center,
                                                              children: <
                                                                  Widget>[
                                                                Text(
                                                                  get_timing(e),
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                          .white,
                                                                      fontSize:
                                                                          17,
                                                                      fontWeight:
                                                                          FontWeight
                                                                              .bold),
                                                                ),
                                                                /*******comment2************/
                                                                Padding(
                                                                  padding: const EdgeInsets
                                                                          .only(
                                                                      left: 2,
                                                                      top: 4),
                                                                  child: Text(
                                                                    '${e.km != 0 ? removeDecimalZeroFormat(double.parse((e.km / 1000).toStringAsFixed(1))) : ''}',
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontSize:
                                                                            10,
                                                                        fontWeight:
                                                                            FontWeight.bold),
                                                                  ),
                                                                )
//                                                      Padding(
//                                                        padding:
//                                                       li     const EdgeInsets
//                                                                    .only(
//                                                                left: 3.0),
//                                                        child: Text(
//                                                          list_square_orange[
//                                                                  index]
//                                                              .txt,
//                                                          style: TextStyle(
//                                                              color:
//                                                                  Colors.white,
//                                                              fontSize: 11,
//                                                              fontWeight:
//                                                                  FontWeight
//                                                                      .bold),
//                                                        ),
//                                                      ),
                                                              ],
                                                            ),
                                                            scrollDirection:
                                                                Axis.horizontal,
                                                          ),
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                    .all(0.0),
                                                            child: Center(
                                                              child: Text(
                                                                e.time_in_mill !=
                                                                            null &&
                                                                        e.time_in_mill !=
                                                                            0
                                                                    ? DateTime.fromMillisecondsSinceEpoch(e
                                                                            .time_in_mill)
                                                                        .toString()
                                                                        .substring(
                                                                            10,
                                                                            19)
                                                                    : '${e.time_in_mill == 0 ? 'tap' : ''}',
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white,
                                                                    fontSize:
                                                                        10,
                                                                    fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                              ),
                                                            ),
                                                          )
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                width: 70,
                                                height: 50,
                                                decoration: BoxDecoration(
                                                    color: Colors.blue,
                                                    borderRadius:
                                                        BorderRadius.all(
                                                            Radius.circular(
                                                                10))),
                                              ),
                                            ),
                                          )),
                                    )
                                  : e.currentSeconds == 0
                                      ? Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Listener(
                                            onPointerUp: (da) async {
                                              if (e.counter == 1) {
                                                try {
                                                  //    reset_blue();
                                                  await send_on_or_off(
                                                      e, context, false);
                                                } catch (ex) {
                                                  toast(context, ex.toString());
                                                  reset_square(e);
                                                }
                                              }
                                              reset_square(e);
                                            },
                                            onPointerDown: (de) {
                                              e.buttonPressed = true;
                                              _increaseCounterWhilePressed(3,
                                                  () async {
                                                await stop_all().then((value) {
                                                  Navigator.push(
                                                      context,
                                                      new MaterialPageRoute(
                                                          builder: (context) =>
                                                              ShowInformationRacer(
                                                                  e))).then(
                                                      (value) async {
//                                                      get_manager();
                                                    reset_square(e);
//                                                  await load_square_list_refresh()
//                                                      .then((value) async {
//
//                                                  });
                                                    await start_all();
                                                  });
                                                });
                                              }, e);
                                            },
                                            child: Container(
                                              child: Center(
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    SingleChildScrollView(
                                                      scrollDirection:
                                                          Axis.horizontal,
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .center,
                                                        children: <Widget>[
                                                          Text(
                                                            get_timing(e),
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 17,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                          ),
                                                          /********comment1************/
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 2,
                                                                    top: 4),
                                                            child: Text(
                                                              '${e.km != 0 ? removeDecimalZeroFormat(double.parse((e.km / 1000).toStringAsFixed(1))) : ''}',
                                                              style: TextStyle(
                                                                  color: Colors
                                                                      .white,
                                                                  fontSize: 10,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
//                                                    Text(
//                                                      '${e.lst_timings.length}/${course.coursepoints.length}',
//                                                      style: TextStyle(
//                                                          color: Colors.white,
//                                                          fontSize: 10,
//                                                          fontWeight:
//                                                              FontWeight.bold),
//                                                    )
                                                  ],
                                                ),
                                              ),
                                              width: 70,
                                              height: 50,
                                              decoration: BoxDecoration(
                                                  color: Colors.blue,
                                                  borderRadius:
                                                      BorderRadius.all(
                                                          Radius.circular(10))),
                                            ),
                                          ),
                                        )
                                      : Container())
                              .toList()),
                    ),
                  ),
                  /**comment_temp grey*/
                  Padding(
                    padding: EdgeInsets.only(left: 27.0, top: 8.0),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Wrap(
                        crossAxisAlignment: WrapCrossAlignment.start,
                        spacing: 10,
                        runSpacing: 10,
                        children: list_square_grey_io
                            .map((e) => OpacityAnimatedWidget.tween(
                                  enabled: e.send_server,
                                  opacityEnabled: 0, //define start value
                                  opacityDisabled: 1, //
                                  child: Listener(
                                    onPointerUp: (da) {
                                      reset_square(e);
                                    },
                                    onPointerDown: (de) {
                                      e.buttonPressed = true;
                                      try {
                                        _increaseCounterWhilePressed(3,
                                            () async {
                                          await _showMyDialogforUundifind(
                                            e.time_in_mill,
                                            e,
                                          ).then((value) {
                                            reset_square(e);
                                          });
                                        }, e);
                                      } catch (ex) {
                                        toast(context, ex);
                                        reset_square(e);
                                      }
                                    },
                                    child: Container(
                                      child: SingleChildScrollView(
                                        scrollDirection: Axis.vertical,
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            SingleChildScrollView(
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    top: 2),
                                                child: Center(
                                                  child: Text(
                                                    e.txt,
                                                    style: TextStyle(
                                                        color: Colors.white,
                                                        fontSize: 14,
                                                        fontWeight:
                                                            FontWeight.bold),
                                                  ),
                                                ),
                                              ),
                                              scrollDirection: Axis.horizontal,
                                            ),
                                            e.time_in_mill != null
                                                ? Padding(
                                                    padding:
                                                        const EdgeInsets.only(
                                                            top: 0),
                                                    child: Text(
                                                      DateTime.fromMillisecondsSinceEpoch(
                                                              e.time_in_mill)
                                                          .toString()
                                                          .substring(10, 19),
                                                      style: TextStyle(
                                                          color: Colors.white,
                                                          fontSize: 10,
                                                          fontWeight:
                                                              FontWeight.bold),
                                                    ),
                                                  )
                                                : Container(),
                                            Text(
                                              "Hold 3 sec",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 13,
                                                  fontWeight: FontWeight.w400),
                                            )
                                          ],
                                        ),
                                      ),
                                      width: 70,
                                      height: 50,
                                      decoration: BoxDecoration(
                                          color: Colors.grey,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10))),
                                    ),
                                  ),
                                ))
                            .toList(),
                      ),
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        left: 27.0,
                        top: MediaQuery.of(context).size.height / 9),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Wrap(
                          crossAxisAlignment: WrapCrossAlignment.start,
                          children: list_square_grey
                              .map((e) => InkWell(
                                    onTap: () async {
                                      int count = 0;
                                      for (int i = 1;
                                          i < list_square_grey_io.length;
                                          i++) {
                                        print('i>>>>>${i}');
                                        if (list_square_grey_io.firstWhere(
                                                (element) =>
                                                    element.count_gray == i,
                                                orElse: () => null) ==
                                            null) {
                                          count = i;
                                          list_square_grey_io.add(new Square(
                                              txt: "OI ${count}",
                                              count_gray: count,
                                              time_in_mill: DateTime.now()
                                                  .millisecondsSinceEpoch));
                                          list_square_grey_io.sort((b, a) => b
                                              .count_gray
                                              .compareTo(a.count_gray));
                                          setState(() {});
                                          return;
                                        }
                                      }

                                      list_square_grey_io.add(new Square(
                                          txt:
                                              "OI ${list_square_grey_io.length + 1}",
                                          count_gray:
                                              list_square_grey_io.length + 1,
                                          time_in_mill: DateTime.now()
                                              .millisecondsSinceEpoch));
                                      list_square_grey_io.sort((b, a) =>
                                          b.count_gray.compareTo(a.count_gray));
                                      setState(() {});
//                                  var coursed;
//                                  var details;
//                                  await DBProvider.db
//                                      .DetailsTracking_limit1(race)
//                                      .then((value) {
//                                    details = value;
//                                  });
//                                  await DBProvider.db
//                                      .Course_list_from_id(details.id)
//                                      .then((value) {
//                                    coursed = value[0];
//                                  });
//                                  Navigator.push(
//                                      context,
//                                      new MaterialPageRoute(
//                                          builder: (context) =>
//                                              UnidentifiedWithoutNote(
//                                                  event_l,
//                                                  details,
//                                                  coursed,
//                                                  race))).then((value) {
//                                    load_square_list_refresh();
//                                  });
                                    },
                                    child: Container(
                                      child: Center(
                                        child: Text(
                                          e.txt,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: 17,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      width: 70,
                                      height: 50,
                                      decoration: BoxDecoration(
                                          color: Colors.grey,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10))),
                                    ),
                                  ))
                              .toList()),
                    ),
                  ),
                ],
              ),
            )
          : spinKitThreeBounce,
    );
  }

  void reset_square(Square square) {
    square.counter = 0;
    square.buttonPressed = false;
  }

  Future send_on_or_off(Square e, BuildContext context, bool is_orange) async {
    if (e.is_start_from_page || e.is_start) return;
    try {
      if (!e.want_to_send) {
        await add_course_point_to_send(
          e,
        );
      }
    } catch (ex) {
      toast(context, ex);
      reset_square(e);
    }
  }

  void toast(BuildContext context, String msg) {
    Toast.show(msg, context, duration: 3);
  }

  String get_timing(Square square) {
    square.lst_timings.sort((a, b) =>
        get_coursepoint(a.courspoint, square.course)
            .compareTo(get_coursepoint(b.courspoint, square.course)));
    return square.lst_timings.last.bib.toString();
  }

  Future<void> refresh_square_after_add_split(
      Square square, Map<String, dynamic> response) async {
    if (!square.is_finish) {
      square.is_start_from_page = true;
      square.currentSeconds = 0;
      square.interval = Duration(seconds: 1);
      square.increase = Duration(seconds: 0);
      square.is_stop = false;
      //square.time_in_mill = int.parse(response['duration'].toString()).abs();
      square.time_in_mill =
          DateTime.fromMillisecondsSinceEpoch(response['duration'])
              .subtract(Duration(hours: 1))
              .millisecondsSinceEpoch;

      await start_all();
      square.want_to_send = false;
      print("here>>>>>>2start");

//            return;
    }
  }

  void determin_squre_want_send(
    Square squareg,
    bool state,
  ) {
    squareg.want_to_send = state;
  }

  Future<bool> enable_timer_and_send(Square square_send,
      {Map<String, dynamic> jso_sent}) async {
    print("jso_sent>>> ${jso_sent}");
    bool is_change = false;

    List lst_timings = await detailsTracking_races.timings['timings'];

    list_bib_of_square_can_undo.add(square_send.bib);
    var eltim = list_send_to_server.lastWhere(
        (element) => element['bib'].toString() == square_send.bib.toString());
    Map<String, dynamic> bodys = eltim;

    Map<String, dynamic> bodys_new = {
      "timeMs": bodys['details']['timeMs'],
      "bib": bodys['details']['bib'],
      "coursepoint": bodys['details']['courspoint'],
      "source": bodys['details']['source'],
      "participantRef": bodys['details']['participantRef']
    };

    try {
      await http
          .put(
              'http://54.77.120.67:8080/rest/_App/trackings/${square_send.tracking}/timings',
              body: jsonEncode(bodys_new),
              headers: <String, String>{
                'Content-Type': 'application/json;charset=UTF-8'
              })
          .timeout(Duration(seconds: 10))
          .then((data) async {
            if (data.statusCode != 200) {
              throw Exception('Failed to load data');
            }
//          print("loadingsend>>>>>>>>");
            //                print("send>>>>>>>>>>>>>>>>>");
            Map<String, dynamic> js = jsonDecode(data.body);
            if (js.containsKey("errors")) {
              print('here_erroe>>>>>');

              throw Exception('${js['errors']}');
            }
            if (js != null) {
              print('js>>>> ${js} lst_timings>>> ${lst_timings}  ');

              list_send_to_server.removeWhere(
                  (element) => element['bib'] == square_send.bib.toString());
              Timing timing = Timing.fromJSON(js);
              //remove any duplicated timing
              square_send.lst_timings.removeWhere((element) =>
                  element.courspoint == timing.courspoint &&
                  element.bib == square_send.bib);
//              print(
//                  'delete_duplicate>> ${lst_timings.where((element) => element['courspoint'] == js['courspoint'] && element['bib'] == square_send.bib)}');
              lst_timings.removeWhere((element) =>
                  element['courspoint'] == js['courspoint'] &&
                  element['bib'] == square_send.bib);
              lst_timings.add(js);
              square_send.lst_timings.add(timing);
              await detailsTracking_races.timings
                  .update('timings', (value) => lst_timings);
//              await DBProvider.db
//                  .update_detailstracking(_detailsTracking_public);

              print("jsCHEC>>>>> ${js}");
              print("jslist>>>>> ${lst_timings.where((element) => element['bib']==square_send.bib)}");
              refresh_details(detailsTracking_races);

              refresh_square_after_add_split(
                square_send,
                js,
              );
              reset_square(square_send);
            }
          });
    } catch (ex) {
      square_send.want_to_send = false;
//      determin_squre_want_send(square_send.bib, false, list_sq);
      remove_from_list_undo(square_send.bib);
      reset_square(square_send);

      toast(context, ex.toString());
    }

    return is_change;
  }

  void remove_from_list_undo(int square) {
    list_bib_of_square_can_undo.removeWhere((ele) => ele == square);
  }

  void run_animation_and_delete(Square squareg) {
    setState(() {
      squareg.send_server = true;
      squareg.start_animation = true;
    });
    refresh_want_refresh(true);
    Future.delayed(Duration(seconds: 1), () {
      refresh_want_refresh(true);
      list_square_grey_io.removeWhere((element) => element.bib == squareg.bib);
      setState(() {});
    });
  }

  Future<void> delete_split(Square squareg, List<Square> list_sq) async {
    Coursepoints coursepoints = squareg.course.coursepoints.lastWhere(
        (element) => element.reference == squareg.lst_timings.last.courspoint);

    List lst_timings = detailsTracking_races.timings['timings'];
    String participantRef;

    participantRef=  get_participantRef(squareg);
    Map<String, dynamic> bodys = {
      "bib": "${squareg.bib}",
      "source": "manual",
      "coursepoint": "${coursepoints.reference}",
      "participantRef": "$participantRef"
    };
    print('jsodelete>>> ${bodys}');
    try {
      await http
          .put(
              'http://54.77.120.67:8080/rest/_App/trackings/${squareg.tracking}/timings/${squareg.bib}/undo',
              body: jsonEncode(bodys),
              headers: <String, String>{
                'Content-Type': 'application/json;charset=UTF-8'
              })
          .timeout(Duration(seconds: 10))
          .then((data) async {
            if (data.statusCode != 200) {
              throw Exception('Failed to load data');
            }

            //                print("send>>>>>>>>>>>>>>>>>");
            Map<String, dynamic> js = jsonDecode(data.body);
print("js>>>>>T${js}");
            if (js.containsKey("errors")) {
              print('here_erroe>>>>>');

              throw Exception('${js['errors']}');
            }
            if (js.toString() == '{}') {
              lst_timings.removeWhere((element) =>
                  element['bib'] == squareg.bib &&
                  element['courspoint'] == bodys['coursepoint'] &&
                  element['tracking'] == squareg.tracking);

              remove_from_list_undo(squareg.bib);
              list_sq
                  .where((element) => element.bib == squareg.bib)
                  .forEach((element) {
                print('elementbefore ${element.lst_timings}');
                element.lst_timings.removeWhere((element) =>
                    element.courspoint.toLowerCase() ==
                        coursepoints.reference.toLowerCase() &&
                    element.tracking == squareg.tracking);
                print('elementafter ${element.lst_timings}');
              });
              print('js>>>>>>>>>>>>>>>>>>>>>>>>>>>> ${js}');
              print('lst_timings<<<< ${lst_timings}');
              await detailsTracking_races.timings
                  .update('timings', (value) => lst_timings);
              if(squareg.lst_timings.length==0){
                String next_coursepoint;

                next_coursepoint = course.coursepoints
                    .firstWhere((element) => element != null)
                    .reference;

                Timing timing = new Timing(
                    bib: squareg.bib, courspoint: next_coursepoint, tracking: squareg.tracking);
                squareg.lst_timings.add(timing);
              }
              refresh_details(detailsTracking_races);
//              await DBProvider.db
//                  .update_detailstracking(_detailsTracking_public);
            }
          });
    } catch (ex) {
      print('Exxxxxxxxxxxxxxxxxxxxx>>> ${ex}');
      Toast.show(ex.toString(), context,
          backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
    }
  }

  Future<void> send_undifind(Map<String, dynamic> bodys, Square squareg) async {
    List lst_timings = detailsTracking_races.timings['timings'];

    try {
      squareg.want_to_send = true;
      determin_squre_want_send(
        squareg,
        true,
      );
      await http
          .put(
              'http://54.77.120.67:8080/rest/_App/trackings/${race.reference}/timings',
              body: jsonEncode(bodys),
              headers: <String, String>{
                'Content-Type': 'application/json;charset=UTF-8'
              })
          .timeout(Duration(seconds: 10))
          .then((data) async {
            if (data.statusCode != 200) {
              throw Exception('Failed to load data');
            }

            //                print("send>>>>>>>>>>>>>>>>>");
            Map<String, dynamic> js = jsonDecode(data.body);
            if (js.containsKey("errors")) {
              print('here_erroe>>>>>');

              throw Exception('${js['errors']}');
            }
            if (js.isNotEmpty) {
              print('js>>>>>>>>>>>>>>>>>>>>>>>>>>>> ${js}');

              lst_timings.add(js);

              await detailsTracking_races.timings
                  .update('timings', (value) => lst_timings);
              refresh_details(detailsTracking_races);
//              await DBProvider.db
//                  .update_detailstracking(_detailsTracking_public);
//              refresh_square_after_add_split(squareg, js, list_sq);

              run_animation_and_delete(squareg);

            }
          });
    } catch (ex) {
      squareg.want_to_send = false;
      determin_squre_want_send(
        squareg,
        false,
      );
      print('Exxxxxxxxxxxxxxxxxxxxx>>> ${ex}');
      Toast.show(ex.toString(), context,
          backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
    }
  }

//  Future<void> send_manual(Map<String, dynamic> bodys) async {
//    List lst_timings = detailsTracking_races.timings['timings'];
//
//    try {
//      await http
//          .put(
//              'http://54.77.120.67:8080/rest/_App/trackings/${race.reference}/timings',
//              body: jsonEncode(bodys),
//              headers: <String, String>{
//                'Content-Type': 'application/json;charset=UTF-8'
//              })
//          .timeout(Duration(seconds: 3))
//          .then((data) async {
//            if (data.statusCode != 200) {
//              throw Exception('Failed to load data');
//            }
//
//            //                print("send>>>>>>>>>>>>>>>>>");
//            Map<String, dynamic> js = jsonDecode(data.body);
//            if (js.containsKey("errors")) {
//              print('here_erroe>>>>>');
//
//              throw Exception('${js['errors']}');
//            }
//            if (js.isNotEmpty) {
//              print('js>>>>>>>>>>>>>>>>>>>>>>>>>>>> ${js}');
//
//              lst_timings.add(js);
//
//              await detailsTracking_races.timings
//                  .update('timings', (value) => lst_timings);
//              refresh_details(detailsTracking_races);
//              refresh_want_refresh(true);
////              await DBProvider.db
////                  .update_detailstracking(_detailsTracking_public);
////              refresh_square_after_add_split(squareg, js, list_sq);
//
//            }
//          });
//    } catch (ex) {
//      print('Exxxxxxxxxxxxxxxxxxxxx>>> ${ex}');
//      Toast.show(ex.toString(), context,
//          backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
//    }
//  }

  void refresh_when_not_gun(Square square) {
    square.time_in_mill = 0;
    square.currentSeconds = 1;
    setState(() {});
  }

  void manage_list_want_to_send(
      Square square, Map<String, dynamic> dataServer) {
    list_send_to_server
        .removeWhere((element) => element['bib'] == '${square.bib}');
    list_send_to_server.add(dataServer);
  }
String  get_participantRef(Square square){
    String participantRef;
  if(square.lst_timings.length>0&&square.lst_timings.first.participantRef!=null){
    participantRef=square.lst_timings.first.participantRef;
  }
  else if(detailsTracking_races.lst_trackers.firstWhere((element) =>element.bib==square.bib&&element.tracking==square.tracking ,orElse: ()=>null)!=null) {
    participantRef=detailsTracking_races.lst_trackers.firstWhere((element) =>element.bib==square.bib&&element.tracking==square.tracking ,orElse: ()=>null).participantRef;
  }
  else {
    participantRef="unknow";
  }
  return participantRef;
}
  Future<void> add_course_point_to_send(Square square) async {
    square.currentSeconds = 0;

    try {
      print(
          'square.lst_timings.last.courspoint ${square.lst_timings.last.courspoint}');
      print('square.course.coursepoints ${square.course.coursepoints}');
      Coursepoints coursepoints ;
      if(square.lst_timings.length>0){
        coursepoints= square.course.coursepoints.firstWhere(
    (element) =>
    element.reference.toLowerCase().toString() ==
    square.lst_timings.last.courspoint.toLowerCase().toString());
      }
      else {
        coursepoints= square.course.coursepoints.first;

      }
//      print('coursepoints>> ${coursepoints}');
//      bool is_found = false;
//      list_send_to_server.forEach((element) {
//        if (element['bib'] == square.bib.toString()) {
//          is_found = true;
//        }
//      });
//      print('tracking>> ${trackingInformation_public}');
//      print('2>>>');
//      if (!is_found) {
//        print('3>>>');
//
//        print('4>>>${dataServer}');
//
//        print('5>>>${list_send_to_server}');
//      }
//      print("chNNN>> ${detailsTracking_races.lst_trackers.firstWhere((element) =>element.bib==square.bib&&element.tracking==square.tracking ,orElse: ()=>null).participantRef}");
      ///**check1**/

      String participantRef;

      participantRef=  get_participantRef(square);

      Map<String, dynamic> dataServer = {
        'bib': '${square.bib}',
        'details': {
          "timeMs": DateTime.now().millisecondsSinceEpoch,
          "bib": square.lst_timings.last.bib,
          "courspoint": coursepoints.reference,
          "source": "manual",
          "participantRef": "$participantRef",
        }
      };
      print("square.lst_timings.length>> ${square.lst_timings}");
      print("dataServer>>> ${dataServer['participantRef']}");

      manage_list_want_to_send(square, dataServer);
      trackingInformation_public = list_tracking.firstWhere(
          (element) => element.reference == square.tracking,
          orElse: () => null);
      if (trackingInformation_public != null &&
          trackingInformation_public.status != Status.guned.asString()) {
        print('trackingInformation_public>>>>dd ${trackingInformation_public}');
        refresh_when_not_gun(square);
      } else {
        if (trackingInformation_public != null &&
            trackingInformation_public.status == Status.guned.asString() &&
            !square.want_to_send) {
          square.want_to_send = true;
          await enable_timer_and_send(square);
          print('good>>>>>');
        }
//        else if (trackingInformation_public != null &&
//            trackingInformation_public.status == Status.guned.asString() &&
//            !square.want_to_send) {
//          await enable_timer_and_send(square, list_sq);
//        }
      }
      reset_square(square);
    } catch (ex) {
      reset_square(square);
      print('ex>>>>>>>>> ${ex}');
    }

//        await   load_square_list_refresh();
    Toast.show("Complete", context,
        backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
  }

  Future<void> _showMyDialogforUndo(
      int mili, int bib, List<Square> list_sq, Square square) async {
    bool is_loading = false;
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (context, set) {
            if (mili == null) {
              Navigator.pop(context);
            }
            return AlertDialog(
              backgroundColor: Colors.black,
              title: Text('Do You want Undo?',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.w100)),
              content: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Row(
                        children: <Widget>[
                          Text(
                            "Bib: ",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.w100),
                          ),
                          Text(
                            bib.toString(),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 20,
                                fontWeight: FontWeight.w100),
                          )
                        ],
                      ),
                    ),
                    Container(
                      child: SingleChildScrollView(
                        scrollDirection: Axis.horizontal,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Row(
                            children: <Widget>[
                              Text("Time: ",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w100)),
                              Text(
                                  '${DateTime.fromMillisecondsSinceEpoch(mili).hour}:${DateTime.fromMillisecondsSinceEpoch(mili).minute}:${DateTime.fromMillisecondsSinceEpoch(mili).second}',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 20,
                                      fontWeight: FontWeight.w100))
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('Cancel'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                !is_loading
                    ? FlatButton(
                        child: Text('Ok'),
                        onPressed: () async {
                          if (list_sq.firstWhere(
                                  (element) => element.bib == bib,
                                  orElse: () => null) ==
                              null) {
                            Toast.show(' Sorry I cant hold back ', context);
                            Navigator.pop(context);
                            return;
                          }
                          if (is_loading) return;

                          try {
                            set(() {
                              is_loading = true;
                            });
                            if (square.time_in_mill != 0) {
                              await delete_split(square, list_sq)
                                  .then((value) async {
                                await undo(bib, list_sq);
                              });
                            } else if (square.time_in_mill == 0) {
                              await undo(bib, list_sq);
                            }
                          } catch (ex) {
                            set(() {
                              is_loading = false;
                            });
                          }

                          Navigator.pop(context);
                        },
                      )
                    : SpinKitThreeBounce(
                        color: Colors.blueGrey,
                        size: 12,
                      ),
              ],
            );
          },
        );
      },
    );
  }

  Future<void> undo(int bibp, List<Square> list_sq) async {
    list_send_to_server
        .removeWhere((element) => element['bib'] == bibp.toString());

    list_sq.where((element) => element.bib == bibp).forEach((element) async {
      element.is_start_from_page = false;
      await element.cancel();

      element.currentSeconds = 0;
      element.interval = Duration(seconds: 1);
      element.increase = Duration(seconds: 0);
      element.is_stop = false;
      element.time_in_mill = null;

//            element.is_delete = true;
//            element.increase =  Duration(seconds: 1);
      setState(() {});
    });
    Toast.show("Complete", context,
        backgroundColor: Colors.black, duration: Toast.LENGTH_LONG);
  }

  Future<void> _showMyDialogforUundifind(
      int time_in_mili, Square squareg) async {
    print("gray>>>>> ${squareg.toString()}");
    lapsList_for_undifind = [];
    lapsList_for_undifind = list_search
        .where((element) => element.label != 'Show all')
        .map((e) => new Coursepoints(
            distance: e.distance,
            label: e.label,
            lapIndex: e.lapIndex,
            type: e.type,
            reference: e.reference))
        .toList();

    TextEditingController _controller_bib = new TextEditingController();
    if (squareg.map_gray != null && squareg.map_gray.containsKey('bib')) {
      _controller_bib.text = squareg.map_gray['bib'].toString();
      setState(() {});
    }
    bool loading_send = false;
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return StatefulBuilder(
          builder: (cont, StateSetter setter) {
            return AlertDialog(
              backgroundColor: Colors.black,
              content: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(children: <Widget>[
                      Text("Bib:",
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: font_size_body,
                              fontWeight: FontWeight.w400)),
                      Flexible(
                        child: TextField(
                            keyboardType: TextInputType.number,
                            inputFormatters: [
                              LengthLimitingTextInputFormatter(7),
                              WhitelistingTextInputFormatter.digitsOnly,
                              BlacklistingTextInputFormatter
                                  .singleLineFormatter,
                            ],
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,
                              contentPadding: EdgeInsets.only(
                                  left: 15, bottom: 11, top: 11, right: 15),
                            ),
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: font_size_body,
                                fontWeight: FontWeight.w400),
                            controller: _controller_bib),
                      )
                    ]),
                    Text(
                        "Recorded time : ${DateTime.fromMillisecondsSinceEpoch(time_in_mili).hour}:${DateTime.fromMillisecondsSinceEpoch(time_in_mili).minute}:${DateTime.fromMillisecondsSinceEpoch(time_in_mili).second}",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: font_size_body,
                            fontWeight: FontWeight.w400)),
                    Padding(
                      padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
                      child: Container(
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Row(
                            children: <Widget>[
                              Text("Timing position choose one:",
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: font_size_body,
                                      fontWeight: FontWeight.w400)),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Column(
//                        mainAxisAlignment: MainAxisAlignment.start,
//                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: lapsList_for_undifind
                            .where((element) =>
                                element.reference !=
                                course.coursepoints.first.reference)
                            .map((e) => Padding(
                                  padding: const EdgeInsets.only(left: 4),
                                  child: Container(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(right: 0.0),
                                          child: e.reference ==
                                                  course.coursepoints.last
                                                      .reference
                                              ? Text(
                                                  "Målgång ${removeDecimalZeroFormat(double.parse((e.distance / 1000).toStringAsFixed(1)))} km",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: font_size_body,
                                                      fontWeight:
                                                          FontWeight.w400),
                                                )
                                              : Text(
                                                  "${e.distance != null ? removeDecimalZeroFormat(double.parse((e.distance / 1000).toStringAsFixed(1))) : ''} Km",
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: font_size_body,
                                                      fontWeight:
                                                          FontWeight.w400),
                                                ),
                                        ),
                                        Theme(
                                          data: Theme.of(context).copyWith(
                                              unselectedWidgetColor:
                                                  Colors.white),
                                          child: Checkbox(
//                                        checkColor: Colors.white,
                                            onChanged: (val) async {
                                              lapsList_for_undifind
                                                  .forEach((element) {
                                                element.select = false;
                                              });
                                              setter(() => e.select = val);
                                            },
                                            value: e.select,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ))
                            .toList()),
                  ],
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('Dont Save',
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: font_size_body,
                          fontWeight: FontWeight.w300)),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                !loading_send
                    ? FlatButton(
                        child: Text('Save',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: font_size_body,
                                fontWeight: FontWeight.w300)),
                        onPressed: () async {
                          if (lapsList_for_undifind
                                  .where((element) => element.select == true)
                                  .length ==
                              0) {
                            if (_controller_bib.text.isNotEmpty) {
                              list_square_grey_io
                                  .where((element) =>
                                      element.count_gray == squareg.count_gray)
                                  .forEach((element) {
                                element.map_gray = {
                                  'bib': int.parse(_controller_bib.text.trim())
                                };
                                element.bib =
                                    int.parse(_controller_bib.text.trim());
                                squareg = element;
                              });
                            }
                          } else if (_controller_bib.text.isNotEmpty) {
                            print("here1>>>");
                            Map<String, dynamic> jso = {
                              "timeMs": time_in_mili,
                              "bib": int.parse(_controller_bib.text.trim()),
                              "coursepoint": lapsList_for_undifind
                                  .where((element) => element.select == true)
                                  .toList()[0]
                                  .reference,
                              "source": "manual",
                              "participantRef": 'unknow'
                            };
                            squareg.bib =
                                int.parse(_controller_bib.text.trim());
                            print("here2>>>");
                            TrackingInformation trackingInformation_public =
                                list_tracking.firstWhere(
                                    (element) => element != null,
                                    orElse: () => null);
                            print("here3>>>");
                            if (!squareg.want_to_send &&
                                trackingInformation_public != null &&
                                trackingInformation_public.status ==
                                    Status.guned.asString() &&
                                !loading_send) {
                              try {
                                print("here4>>>");
                                setter(() => loading_send = true);
                                await send_undifind(jso, squareg);
                              } catch (ex) {
                                setter(() => loading_send = false);
                              }
                            }
                          }
                          Navigator.pop(context);
                        },
                      )
                    : SpinKitThreeBounce(
                        color: Colors.blue,
                        size: 12,
                      ),
                FlatButton(
                  child: Text('Delete',
                      style: TextStyle(
                          color: Colors.red,
                          fontSize: font_size_body,
                          fontWeight: FontWeight.w300)),
                  onPressed: () async {
                    list_square_grey_io
                        .removeWhere((element) => element == squareg);
                    list_square_grey_io
                        .sort((b, a) => b.count_gray.compareTo(a.count_gray));
                    setState(() {});
                    Navigator.of(context).pop();
//                setState(() {});
                  },
                )
              ],
            );
          },
        );
      },
    );
  }

//  Future<void> _showMyDialogforAddManual() async {
//    bool loading_send = false;
//
//    lapsList_for_manual = [];
//    lapsList_for_manual = list_search
//        .where((element) => element.label != 'Show all')
//        .map((e) => new Coursepoints(
//            distance: e.distance,
//            label: e.label,
//            lapIndex: e.lapIndex,
//            type: e.type,
//            reference: e.reference))
//        .toList();
//    int _chosenValuebib;
//
//    return showDialog<void>(
//      context: context,
//      barrierDismissible: false, // user must tap button!
//      builder: (BuildContext context) {
//        return StatefulBuilder(
//          builder: (cont, StateSetter setter) {
//            return AlertDialog(
//              backgroundColor: Colors.blueGrey,
//              content: SingleChildScrollView(
//                scrollDirection: Axis.vertical,
//                child: Column(
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  children: <Widget>[
//                    Row(children: <Widget>[
//                      Text("Bib:",
//                          style: TextStyle(
//                              color: Colors.white,
//                              fontSize: font_size_body,
//                              fontWeight: FontWeight.w400)),
//                    ]),
//                    DropdownButton<String>(
//                      focusColor: Colors.white,
//                      value: _chosenValuebib != null
//                          ? _chosenValuebib.toString()
//                          : null,
//                      dropdownColor: Colors.blueGrey,
//                      //elevation: 5,
//                      style: TextStyle(color: Colors.white),
//                      iconEnabledColor: Colors.white,
//                      items: race.list_racers
//                          .map<DropdownMenuItem<String>>((Racer value) {
//                        return DropdownMenuItem<String>(
//                          value: value.bib.toString(),
//                          child: Text(
//                            value.bib.toString(),
//                            style: TextStyle(color: Colors.white),
//                          ),
//                        );
//                      }).toList(),
//                      hint: Text(
//                        "Please choose a racer",
//                        style: TextStyle(
//                            color: Colors.white,
//                            fontSize: 14,
//                            fontWeight: FontWeight.w500),
//                      ),
//                      onChanged: (String value) {
//                        setter(() => _chosenValuebib = int.parse(value));
//                      },
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.only(top: 2.0, bottom: 2.0),
//                      child: Container(
//                        child: SingleChildScrollView(
//                          scrollDirection: Axis.horizontal,
//                          child: Row(
//                            children: <Widget>[
//                              Text("Timing position choose one:",
//                                  style: TextStyle(
//                                      color: Colors.white,
//                                      fontSize: font_size_body,
//                                      fontWeight: FontWeight.w400)),
//                            ],
//                          ),
//                        ),
//                      ),
//                    ),
//                    Column(
////                        mainAxisAlignment: MainAxisAlignment.start,
////                        crossAxisAlignment: CrossAxisAlignment.center,
//                        children: lapsList_for_manual
//                            .where((element) =>
//                                element.reference !=
//                                course.coursepoints.first.reference)
//                            .map((e) => Padding(
//                                  padding: const EdgeInsets.only(left: 4),
//                                  child: Container(
//                                    child: Row(
//                                      mainAxisAlignment:
//                                          MainAxisAlignment.spaceBetween,
//                                      children: <Widget>[
//                                        Padding(
//                                          padding:
//                                              const EdgeInsets.only(right: 0.0),
//                                          child: e.reference ==
//                                                  course.coursepoints.last
//                                                      .reference
//                                              ? Text(
//                                                  "Målgång ${removeDecimalZeroFormat(double.parse((e.distance / 1000).toStringAsFixed(1)))} km",
//                                                  style: TextStyle(
//                                                      color: Colors.white,
//                                                      fontSize: font_size_body,
//                                                      fontWeight:
//                                                          FontWeight.w400),
//                                                )
//                                              : Text(
//                                                  "${e.distance != null ? removeDecimalZeroFormat(double.parse((e.distance / 1000).toStringAsFixed(1))) : ''} Km",
//                                                  style: TextStyle(
//                                                      color: Colors.white,
//                                                      fontSize: font_size_body,
//                                                      fontWeight:
//                                                          FontWeight.w400),
//                                                ),
//                                        ),
//                                        Theme(
//                                          data: Theme.of(context).copyWith(
//                                              unselectedWidgetColor:
//                                                  Colors.white),
//                                          child: Checkbox(
////                                        checkColor: Colors.white,
//                                            onChanged: (val) async {
//                                              lapsList_for_manual
//                                                  .forEach((element) {
//                                                element.select = false;
//                                              });
//                                              setter(() => e.select = val);
//                                            },
//                                            value: e.select,
//                                          ),
//                                        ),
//                                      ],
//                                    ),
//                                  ),
//                                ))
//                            .toList()),
//                  ],
//                ),
//              ),
//              actions: <Widget>[
//                FlatButton(
//                  child: Text('Dont Save',
//                      style: TextStyle(
//                          color: Colors.white,
//                          fontSize: font_size_body,
//                          fontWeight: FontWeight.w300)),
//                  onPressed: () {
//                    Navigator.of(context).pop();
//                  },
//                ),
//                !loading_send
//                    ? FlatButton(
//                        child: Text('Save',
//                            style: TextStyle(
//                                color: Colors.white,
//                                fontSize: font_size_body,
//                                fontWeight: FontWeight.w300)),
//                        onPressed: () async {
//                          if (lapsList_for_manual
//                                      .where(
//                                          (element) => element.select == true)
//                                      .length >
//                                  0 &&
//                              _chosenValuebib != null) {
//                            String participantRef = 'unknow';
//                            if (_detailsTracking_public.lst_trackers.firstWhere(
//                                    (element) => element.bib == _chosenValuebib,
//                                    orElse: () => null) !=
//                                null) {
//                              participantRef = _detailsTracking_public
//                                  .lst_trackers
//                                  .firstWhere(
//                                      (element) =>
//                                          element.bib == _chosenValuebib,
//                                      orElse: () => null)
//                                  .participantRef;
//                            }
//
//                            Map<String, dynamic> jso = {
//                              "timeMs": DateTime.now().millisecondsSinceEpoch,
//                              "bib": _chosenValuebib,
//                              "coursepoint": lapsList_for_manual
//                                  .where((element) => element.select == true)
//                                  .toList()[0]
//                                  .reference,
//                              "source": "manual",
//                              "participantRef": participantRef
//                            };
//
//                            if (trackingInformation_public != null &&
//                                trackingInformation_public.status ==
//                                    Status.guned.asString() &&
//                                !loading_send) {
//                              setter(() => loading_send = true);
//                              try {
//                                await send_manual(jso);
//                                Navigator.pop(context);
//                              } catch (ex) {
//                                setter(() => loading_send = false);
//                              }
//                            }
//                          }
//                        },
//                      )
//                    : SpinKitThreeBounce(
//                        size: 12,
//                        color: Colors.blue,
//                      ),
//              ],
//            );
//          },
//        );
//      },
//    );
//  }

  String removeDecimalZeroFormat(double n) {
    return n.toStringAsFixed(n.truncateToDouble() == n ? 0 : 1);
  }
}
