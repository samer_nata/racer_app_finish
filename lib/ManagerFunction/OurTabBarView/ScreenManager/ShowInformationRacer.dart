import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:racer_app_finish/Models/Details_Tracking.dart';
import 'package:racer_app_finish/Models/Event.dart';
import 'package:racer_app_finish/Models/Manager.dart';
import 'package:racer_app_finish/Models/Race.dart';
import 'package:racer_app_finish/Models/Timings.dart';
import 'package:racer_app_finish/Models/Trackers.dart';
import 'package:racer_app_finish/Models/TrackingWithInformation.dart';
import 'package:racer_app_finish/Screen/EventsScreenRacer.dart';

import 'package:toast/toast.dart';

import '../../../Appearance.dart';
import '../../../Database.dart';
import '../../../Square.dart';
import 'EventsScreenManager.dart';
import 'LoginManager.dart';
import 'Unidentified.dart';

import 'package:http/http.dart' as http;

class ShowInformationRacer extends StatefulWidget {
  Square square;

  ShowInformationRacer(this.square);

  @override
  _ShowInformationRacerState createState() =>
      _ShowInformationRacerState(square);
}

class _ShowInformationRacerState extends State<ShowInformationRacer> {
  Square square;
  DetailsTracking detailsTracking = new DetailsTracking();
  bool is_finish_load = false;

  bool send_to_server = false;
  Race race = new Race();
  Event_l event_l = new Event_l();

  _ShowInformationRacerState(
    this.square,
  );

  bool is_dnf = false;
  bool is_dns = false;
  bool is_dsq = false;
  Map<String, bool> list_resource = {'dns': false, 'dnf': false, 'dsq': false};
  Map<String, bool> list_refresh = {'dns': false, 'dnf': false, 'dsq': false};

  List<Manager> manager_list = new List();
  List<Timing> list_timings = new List();
  List<Tracker> list_trackers = new List();

  @override
  void dispose() {
    super.dispose();
  }

//  Future<void> _showMyDialog() async {
//
//    DateTime temp;
//    return showDialog<void>(
//      context: context,
//      barrierDismissible: false, // user must tap button!
//      builder: (BuildContext context) {
//        return AlertDialog(
//          backgroundColor: Colors.black,
//          title: Text('Set Time'),
//          content: SingleChildScrollView(
//            scrollDirection: Axis.vertical,
//            child: TimePickerSpinner(
//              normalTextStyle: TextStyle(fontSize: 24, color: Colors.grey),
//              highlightedTextStyle:
//                  TextStyle(fontSize: 24, color: Colors.white),
//              isShowSeconds: true,
//              is24HourMode: false,
//              spacing: 10,
//              itemHeight: 60,
//              isForce2Digits: true,
//              onTimeChange: (time) {
//                setState(() {
//                  temp = time;
//                });
//              },
//            ),
//          ),
//          actions: <Widget>[
//            FlatButton(
//              child: Text('Cancel'),
//              onPressed: () {
//                Navigator.of(context).pop();
//              },
//            ),
//            FlatButton(
//              child: Text('Ok'),
//              onPressed: () {
//                if (temp != null) {
//                  setState(() {
//                    _dateTime = temp;
//                  });
//                }
//                Navigator.of(context).pop();
//              },
//            ),
//          ],
//        );
//      },
//    );
//  }
  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('vill du logga ut?'),
          actions: <Widget>[
            FlatButton(
              child: Text('Annullera'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: () async {
                await DBProvider.db.deleteAllManager().then((value) {
                  manager_list = [];
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();
                });

                setState(() {});
              },
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.only(top: 15),
                child: is_finish_load
                    ? Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            child: Text(
                              "${event_l.name}",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Container(
                            child: Text(
                              "${race.name}",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                          Container(
                            child: Text(
                              "${square.bib}.${race.list_racers.firstWhere((element) => element.bib == square.bib, orElse: () => null) != null ? race.list_racers.firstWhere((element) => element.bib == square.bib, orElse: () => null).firstname + ' ' + race.list_racers.firstWhere((element) => element.bib == square.bib, orElse: () => null).surname : ''}",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w400),
                            ),
                          ),
                        ],
                      )
                    : Container(),
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: const EdgeInsets.only(top: 15, right: 20),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(
                      child: InkWell(
                        onTap: () {
                          if (manager_list.length == 0) {
                            Navigator.push(
                                    context,
                                    new MaterialPageRoute(
                                        builder: (context) =>
                                            LoginManager(event_l, null)))
                                .then((value) {
                              get_manager();
                            });
                          } else {
                            _showMyDialog();
                          }
                        },
                        child: Container(
                          width: 20,
                          height: 25,
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: manager_list.length == 0
                                      ? AssetImage('Images/profile.png')
                                      : AssetImage('Images/greenProfile.png'),
                                  fit: BoxFit.fill)),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: InkWell(
                child: Padding(
                  padding: EdgeInsets.only(top: 60),
                  child: Container(
                    width: 80,
                    height: 50,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('Images/LaLigaLogo.png'),
                            fit: BoxFit.fill)),
                  ),
                ),
                onTap: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (_) => EventsScreenRacer(true)),
                      (route) => false);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 120),
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.blue,
                height: 2,
              ),
            ),
          ],
        ),
        preferredSize: Size.fromHeight(100),
      ),
      backgroundColor: Colors.black,
      body: is_finish_load
          ? Stack(
              children: <Widget>[
                SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Container(
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 8.0, right: 8.0, top: 15),
                          child: Container(
                            decoration: BoxDecoration(
                                color: Color(0xff121214),
                                border: Border(
                                    top: BorderSide(
                                        color: Colors.blueGrey[50],
                                        width: 0.3))),
                            width: MediaQuery.of(context).size.width,
                            child: SingleChildScrollView(
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Bib: ${square.bib}",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: font_size_body,
                                          fontWeight: FontWeight.w400),
                                    ),
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: square.course.coursepoints
                                          .map((e) => Text(
                                              "${e.label}: ${list_timings.firstWhere((element) => element.courspoint == e.reference, orElse: () => null) != null ? list_timings.firstWhere((element) => element.courspoint == e.reference).time : 'No Set'}",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: font_size_body,
                                                  fontWeight: FontWeight.w400)))
                                          .toList(),
                                    ),
                                    get_checkbox()
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              top: 24.0, left: 20, right: 20),
                          child: !send_to_server
                              ? InkWell(
                                  onTap: () async {
                                    var element1 = list_resource.entries
                                        .firstWhere((element) => element.value,
                                            orElse: () => null);
                                    var element2 = list_refresh.entries
                                        .firstWhere((element) => element.value,
                                            orElse: () => null);
                                    if ((element1 == null &&
                                            element2 != null) ||
                                        (element1 != null &&
                                            element2 != null &&
                                            (element1.key != element2.key))) {
                                      print('exce>>>>');
                                      await change_status_racer();
                                    } else {
                                      Navigator.pop(context);
                                    }
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                        color: Colors.blue,
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10))),
                                    height: 50,
                                    child: Center(
                                      child: Text(
                                        "Confirm",
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: font_size_body,
                                            fontWeight: FontWeight.w400),
                                      ),
                                    ),
                                  ),
                                )
                              : SpinKitThreeBounce(
                                  color: Colors.blue,
                                  size: font_size_body,
                                ),
                        )
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 16.0, left: 8.0),
                  child: Align(
                    alignment: Alignment.bottomLeft,
                    child: InkWell(
                      onTap: () => Navigator.pop(context),
                      child: Text(
                        "Back",
                        style: TextStyle(
                            color: Colors.blue,
                            fontSize: font_size_body,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ),
                )
              ],
            )
          : Container(),
    );
  }

  String extract_value() {
    if (is_dnf) {
      return "dnf";
    } else if (is_dns) {
      return "dns";
    } else if (is_dsq) {
      return "dsq";
    }
  }

  Future<void> change_status_racer() async {
    setState(() {
      send_to_server = true;
    });

    var bodys = jsonEncode({
      "status": list_refresh.entries.firstWhere((element) => element.value).key
    });
    print('bodys>>> ${bodys}');
    try {
//      print('>>>>>>>>>>>>>> ${trackingInformation.reference} ${square.bib}');
      await http.post(
          'http://54.77.120.67:8080/rest/_App/trackings/${square.tracking}/trackers/${square.bib}/result',
          body: bodys,
          headers: <String, String>{
            'Content-Type': 'application/json;charset=UTF-8'
          }).then((data) async {
//      races = [];
        Map<String, dynamic> dataServer = jsonDecode(data.body);
        print("<<<<<<<<<<<<<<<<<<<<<ddd<<<<<<<<<<< ${dataServer}");
        if (dataServer.isNotEmpty) {
          List trackers = detailsTracking.trackers['trackers'];
          trackers.forEach((element) {
            if (element['bib'] == square.bib) {
              element = dataServer;
            }
          });
          await detailsTracking.trackers
              .update('trackers', (value) => trackers);
          await DBProvider.db.update_detailstracking(detailsTracking);
          detailsTracking.get_trackers();
          setState(() {
            send_to_server = false;
          });
          Toast.show("succeeded!!!!", context);
          Navigator.pop(context);
        } else {
          Toast.show("an error occurred ):", context);
          setState(() {
            send_to_server = false;
          });
        }
      });
    } catch (ex) {
      setState(() {
        send_to_server = false;
      });
    }
  }

  get_manager() {
    DBProvider.db.Manager_list().then((value) {
      if (value != null && value.length > 0) {
        setState(() {
          manager_list = value;
        });
      } else if (value.length == 0) {
        setState(() {
          manager_list = [];
        });
      }
    });
  }

  List d = List();

  Widget get_checkbox() {
    return Column(
      children: list_refresh.entries
          .map((e) => Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                      "The racer has ${e.key == 'dns' ? 'start' : e.key} the race (${e.key.toUpperCase()})",
                      style: TextStyle(
                          color: Colors.white,
                          fontSize: font_size_body,
                          fontWeight: FontWeight.w400)),
                  alig_checbox(Theme(
                    data: Theme.of(context).copyWith(
                      unselectedWidgetColor: Colors.white,
                    ),
                    child: Checkbox(
                      onChanged: (val) {
                        if (val == true) {
                          list_refresh.updateAll((key, value) => false);
                          list_refresh.update(e.key, (value) => val);
                        }
                        setState(() {});
                      },
                      value: e.value,
                    ),
                  ))
                ],
              ))
          .toList(),
    );
  }

  Future<void> get_detail() async {
    await DBProvider.db
        .get_trackings_reference(square.tracking, is_update: true)
        .then((value) {
      if (value.length > 0) {
        event_l=value[0];
        print("event_l>>>> ${event_l}  ${value[0]}");
        race=value[1];
        print("race>>>> ${race}  ");
        detailsTracking=value[2];
        print("detailsTracking>>>>tt ${detailsTracking}  ");
        detailsTracking.get_timigs();
        detailsTracking.get_trackers();
        referseh();
      }
    });



  }

  Future<void> referseh() async {
//    List<Race> lst_race = await DBProvider.db.Race_list_all_e();
//    race = value.firstWhere((element) => element.reference == square.tracking,
//        orElse: () => null);
//    print("race>>>TTT ${detailsTracking}");
//
//    await DBProvider.db.Events_list().then((value) {
//      event_l = value.firstWhere(
//          (element) => element.reference == race.ref_event,
//          orElse: () => null);
//    });
    is_finish_load = true;

    list_timings = detailsTracking.lst_timings
        .where((element) =>
            element.bib == square.bib && element.tracking == square.tracking)
        .toList(growable: true);

    Tracker tracker = detailsTracking.lst_trackers
        .firstWhere((element) => element.bib == square.bib, orElse: () => null);
    print('checKKKKK>> ${tracker}');
    if (tracker != null && tracker.result != null) {
      String state = tracker.result['status'];

      list_resource.forEach((key, value) {
        if (state == key) {
          value = true;
          list_resource.update(key, (value) => true);
        }
      });
      list_refresh.forEach((key, value) {
        if (state == key) {
          list_refresh.update(key, (value) => true);
          value = true;
        }
      });
    }
    setState(() {});
  }

  void make_checkbox_fasle() {
    is_dsq = false;
    is_dnf = false;
    is_dns = false;
  }

  Widget alig_checbox(Widget widget) {
    return Align(
      alignment: Alignment.bottomRight,
      child: widget,
    );
  }

  @override
  void initState() {
    get_detail();
    get_manager();

    super.initState();
  }
}
