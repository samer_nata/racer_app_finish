import 'package:flutter/material.dart';
import 'package:racer_app_finish/ManagerFunction/OurTabBarView/ScreenManager/LoginManager.dart';
import 'package:racer_app_finish/Models/Event.dart';
import 'package:racer_app_finish/Models/Manager.dart';
import 'package:racer_app_finish/Models/Race.dart';
import 'package:racer_app_finish/Screen/EventsScreenRacer.dart';
import 'package:racer_app_finish/Screen/RacesScreenRacer.dart';
import 'package:racer_app_finish/User/Login.dart';




import '../../../Database.dart';
import '../OurTabBarView/Record.dart';

import '../OurTabBarView/Start.dart';

import 'package:http/http.dart' as http;

import 'EventsScreenManager.dart';
import 'LiveResultsManager.dart';

class ControlPage extends StatefulWidget {
  Race race;

  Event_l event_l;

  ControlPage(this.event_l, this.race);

  @override
  _ControlPageState createState() => _ControlPageState(event_l, race);
}

class _ControlPageState extends State<ControlPage>
    with SingleTickerProviderStateMixin {
  Race race;
  TabController tabController;
  List<Manager> manager_list = new List();
  Event_l event_l;

  get_manager() {
    DBProvider.db.Manager_list().then((value) {
      if (value != null && value.length > 0) {
        setState(() {
          manager_list = value;
        });
      }
      else if (value.length==0) {
        setState(() {
          manager_list=[];
        });

      }
    });
  }

  _ControlPageState(this.event_l, this.race);

  @override
  void initState() {
    tabController = new TabController(length: 3, vsync: this);
    tabController.addListener(() {
      setState(() {});
    });

    get_manager();
    DBProvider.db.get_information_race(race, event_l).then((value) {
//  DBProvider.db.get_trackings_race(race,is_update: true);
      print(">>>>>>>>>>>>>## ");
    });

    super.initState();
  }
  Future<void> _showMyDialog() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('vill du logga ut?'),
          actions: <Widget>[
            FlatButton(
              child: Text('Annullera'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            FlatButton(
              child: Text('Ok'),
              onPressed: () async {
                await DBProvider.db.deleteAllManager().then((value) {
                  manager_list=[];
                  Navigator.of(context).pop();
                  Navigator.of(context).pop();

                });


              },
            ),
          ],
        );
      },
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: PreferredSize(
        child: Stack(
          children: <Widget>[
            Align(
              alignment: Alignment.topCenter,
              child: Padding(
                padding: const EdgeInsets.only(top: 30, left: 0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        "${event_l.name}",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 20,
                            fontWeight: FontWeight.w400),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 60, right: 20),
              child: Align(
                alignment: Alignment.topRight,
                child: Container(
                  child: InkWell(
                    onTap: () {
                      if (manager_list.length == 0) {
                        Navigator.push(
                            context,
                            new MaterialPageRoute(
                                builder: (context) =>
                                    LoginManager(event_l, race))).then((value) {
                          get_manager();
                        });
                      }
                      else{
                        _showMyDialog();
                      }
                    },
                    child: Container(
                      width: 20,
                      height: 25,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: manager_list.length == 0
                                  ? AssetImage('Images/profile.png')
                                  : AssetImage('Images/greenProfile.png'),
                              fit: BoxFit.fill)),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              child: InkWell(
                child: Padding(
                  padding: EdgeInsets.only(top: 60),
                  child: Container(
                    width: 80,
                    height: 50,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image: AssetImage('Images/LaLigaLogo.png'),
                            fit: BoxFit.fill)),
                  ),
                ),
                onTap: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (_) => EventsScreenRacer(true)),
                          (route) => false);
                },
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 5),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    InkWell(
                      child: Container(
                        child: Text(
                          "${"Start"}",
                          style: TextStyle(
                              color: tabController.index == 0
                                  ? Colors.blue
                                  : Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                      onTap: () {
                        if (tabController.index != 0) {
                          tabController.animateTo(0);
                        }
                      },
                    ),
                    InkWell(
                      child: Container(
                        child: Text(
                          "Record",
                          style: TextStyle(
                              color: tabController.index == 1
                                  ? Colors.blue
                                  : Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                      onTap: () {
                        if (tabController.index != 1) {
                          tabController.animateTo(1);
                        }
                      },
                    ),       InkWell(
                      child: Container(
                        child: Text(
                          "${"Results"}",
                          style: TextStyle(
                              color: tabController.index == 2
                                  ? Colors.blue
                                  : Colors.white,
                              fontSize: 20,
                              fontWeight: FontWeight.w400),
                        ),
                      ),
                      onTap: () {
                        if (tabController.index != 2) {
                          tabController.animateTo(2);
                        }
                      },
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 120),
              child: Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.blue,
                height: 2,
              ),
            ),
          ],
        ),
        preferredSize: Size.fromHeight(100),
      ),
      body: TabBarView(controller: tabController, children: <Widget>[
        new Container(
          child: Start(event_l, race),
        ),
        new Container(
          child: Record(event_l, race),
        ),
        new Container(
          child:
          LiveResultsManager(event_l,false),
        ),
      ]),
    );
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }
}
